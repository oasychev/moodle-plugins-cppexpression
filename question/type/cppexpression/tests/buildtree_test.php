<?php
use qtype_cppexpression\core\comparator;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\core\declaration;

class qtype_cppexpression_buildtree_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
    public static function setUpBeforeClass() {
        qtype_cppexpression_buildtree_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k,x,h;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_buildtree_testcase::$decl->accept_declaration();
    }


    public function test_oneOperand() {
		$expression = "a";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];

		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\operand'), "type of root not correct");
		$this->assertEquals("a",$result->name, "name of operand not correct");
	}

	public function test_oneOperator() {
		$expression = "*";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getErrors();
    }

	public function test_notEnoughOperand() {
		$expression = "a +";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getErrors();
	}

	public function test_notEnoughOperator() {
		$expression = "a + b c";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getErrors();
	}
	
	public function test_onlyUnary() {
		$expression = "!a";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];
        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\onedimnodes\not_logic_operator'), "type of root error");
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of children error");
		$this->assertEquals("a", $result->children[0]->name, "name of operand error");
	}
	
	public function test_onlyBinary() {
		$expression = "c = a / b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("c", $result->children[0]->name, "name of left operand error");
		
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\binarynodes\div_operator'), "type of right children error");
		
		$this->assertTrue(is_a($result->children[1]->children[0], 'qtype_cppexpression\core\operand'), "type of left children div operator error");
		$this->assertEquals("a", $result->children[1]->children[0]->name, "name of left operand of div opertor error");
		
		$this->assertTrue(is_a($result->children[1]->children[1], 'qtype_cppexpression\core\operand'), "type of right children div operator error");
		$this->assertEquals("b", $result->children[1]->children[1]->name, "name of right operand of div opertor error");
	}
	
	public function test_onlyKdim() {
		$expression = "a * b * c";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];


        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\kdimnodes\multi_operator'), "type of root error");
		$this->assertEquals(3, count($result->children), "number of children error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of 1-st children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of 2-sd children error");
		$this->assertTrue(is_a($result->children[2], 'qtype_cppexpression\core\operand'), "type of 3-rd children error");
		
		$this->assertEquals("a", $result->children[0]->name, "name of 1-st children error");
		$this->assertEquals("b", $result->children[1]->name, "name of 2-sd children error");
		$this->assertEquals("c", $result->children[2]->name, "name of 3-rd children error");
		
	}
	
	public function test_mixedType() {
		$expression = "a = !a && b && c || a && !b && !c";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];


        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left operand error");
		
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\kdimnodes\or_logic_operator'), "type of right children error");
		$this->assertEquals(2, count($result->children[1]->children), "number of or logic operator error");
		
		$this->assertTrue(is_a($result->children[1]->children[0], 'qtype_cppexpression\core\kdimnodes\and_logic_operator'), "type of 1-st children of or logic error");
		$this->assertTrue(is_a($result->children[1]->children[1], 'qtype_cppexpression\core\kdimnodes\and_logic_operator'), "type of 2-sd children of or logic error");
		$this->assertEquals(3, count($result->children[1]->children[0]->children), "number of 1-st children or logic operator error");
		$this->assertEquals(3, count($result->children[1]->children[1]->children), "number of 2-sd children or logic operator error");
		
		$this->assertTrue(is_a($result->children[1]->children[0]->children[0], 'qtype_cppexpression\core\onedimnodes\not_logic_operator'), "type of 1-st children of 1-st and logic error");
		$this->assertTrue(is_a($result->children[1]->children[0]->children[0]->children[0], 'qtype_cppexpression\core\operand'), "type of 1-st children of 1-st and logic error");
		$this->assertEquals("a", $result->children[1]->children[0]->children[0]->children[0]->name, "name of 1-st children of 1-st and logic error");
		
		$this->assertTrue(is_a($result->children[1]->children[0]->children[1], 'qtype_cppexpression\core\operand'), "type of 2-sd children of 1-st and logic error");
		$this->assertEquals("b", $result->children[1]->children[0]->children[1]->name, "name of 2-sd children of 1-st and logic error");
		$this->assertTrue(is_a($result->children[1]->children[0]->children[2], 'qtype_cppexpression\core\operand'), "type of 3-rd children of 1-st and logic error");
		$this->assertEquals("c", $result->children[1]->children[0]->children[2]->name, "name of 3-rd children of 1-st and logic error");
		
		$this->assertTrue(is_a($result->children[1]->children[1]->children[0], 'qtype_cppexpression\core\operand'), "type of 1-st children of 2-sd and logic error");
		$this->assertEquals("a", $result->children[1]->children[1]->children[0]->name, "name of 1-st children of 2-sd and logic error");
		
		$this->assertTrue(is_a($result->children[1]->children[1]->children[1], 'qtype_cppexpression\core\onedimnodes\not_logic_operator'), "type of 2-sd children of 2-sd and logic error");
		$this->assertTrue(is_a($result->children[1]->children[1]->children[1]->children[0], 'qtype_cppexpression\core\operand'), "type of 2-sd children of 2-sd and logic error");
		$this->assertEquals("b", $result->children[1]->children[1]->children[1]->children[0]->name, "name of 2-sd children of 2-sd and logic error");
		
		$this->assertTrue(is_a($result->children[1]->children[1]->children[2], 'qtype_cppexpression\core\onedimnodes\not_logic_operator'), "type of 3-rd children of 3-rd and logic error");
		$this->assertTrue(is_a($result->children[1]->children[1]->children[2]->children[0], 'qtype_cppexpression\core\operand'), "type of 2-sd children of 3-rd and logic error");
		$this->assertEquals("c", $result->children[1]->children[1]->children[2]->children[0]->name, "name of 2-sd children of 3-rd and logic error");
		
	}
	
	public function test_defOperator() {
		$expression = "*(a+i)";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\onedimnodes\dereference_operator'), "type of root error");
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\kdimnodes\plus_operator'), "type of children error");
		
		$this->assertEquals(2, count($result->children[0]->children), "number of plus operator error");
		
		$this->assertTrue(is_a($result->children[0]->children[0], 'qtype_cppexpression\core\operand'), "type of 1-st children error");
		$this->assertTrue(is_a($result->children[0]->children[1], 'qtype_cppexpression\core\operand'), "type of 2-nd children error");
		
		$this->assertEquals("a", $result->children[0]->children[0]->name, "name of 1-st children error");
		$this->assertEquals("i", $result->children[0]->children[1]->name, "name of 2-nd children error");
		
	}
	
	public function test_referOper() {
		$expression = "&a";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\onedimnodes\reference_operator'), "type of root error");
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of children error");
		$this->assertEquals("a", $result->children[0]->name, "name of children error");
	}
	
	public function test_unaryMinus() {
		$expression = "a + -b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\kdimnodes\plus_operator'), "type of root error");
		$this->assertEquals(2, count($result->children), "number of plus operator error");
		$this->assertEquals("a", $result->children[0]->name, "name of 1-st children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\onedimnodes\unary_minus_operator'), "type of 2-rd error");
		$this->assertEquals("b", $result->children[1]->children[0]->name, "name of 1-st children error");

	}
	
	public function test_minus() {
		$expression = "a - b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\minus_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
		
	}
	
	public function test_modoperator() {
		$expression = "a % b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\mod_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
		
	}
	
	public function test_equanOper() {
		$expression = "a == b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\equal_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
		
	}
	
	public function test_notEqualOper() {
		$expression = "a != b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\not_equal_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_greaterOper() {
		$expression = "a > b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\greater_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_lessOper() {
		$expression = "a < b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\less_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_greaterEqualOp() {
		$expression = "a >= b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\greater_equal_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_lessEqualOp() {
		$expression = "a <= b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\less_equal_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_plusAssOp() {
		$expression = "a += b";
        $tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
        $result = $tree->getTree()->children[0];

        $this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\plus_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_minusAssOp() {
		$expression = "a -= b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\minus_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_multiAssOp() {
		$expression = "a *= b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\multi_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_divAssOp() {
		$expression = "a /= b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\div_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_shlAssOp() {
		$expression = "a <<= b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\shl_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_shrAssOp() {
		$expression = "a >>= b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\shr_assign_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_shlOp() {
		$expression = "a << b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\shift_left_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_shrOp() {
		$expression = "a >> b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\shift_right_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_subscOp() {
		$expression = "a[b]";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\subscript_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_ptMemOp() {
		$expression = "a.b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\mem_acc_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
		
	}
	
	public function test_memOp() {
		$expression = "a->b";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];
		
		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\binarynodes\pt_mem_acc_operator'), "type of root error");
		
		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\operand'), "type of left children error");
		$this->assertEquals("a", $result->children[0]->name, "name of left children error");
		$this->assertTrue(is_a($result->children[1], 'qtype_cppexpression\core\operand'), "type of right children error");
		$this->assertEquals("b", $result->children[1]->name, "name of right children error");
	}
	
	public function test_powFunc() {
		$expression = "pow(a,b)";
		$tree = new tree_converter($expression, 0, qtype_cppexpression_buildtree_testcase::$decl);
		$result = $tree->getTree()->children[0];

		$this->assertTrue(is_a($result, 'qtype_cppexpression\core\kdimnodes\function_call'), "type of root error");
        $this->assertEquals('pow', $result->name->string(), "name of function error");

		$this->assertTrue(is_a($result->children[0], 'qtype_cppexpression\core\kdimnodes\sequencing_operator'), "type of list argument error");

		$this->assertTrue(is_a($result->children[0]->children[0], 'qtype_cppexpression\core\operand'), "type of first argument error");
		$this->assertEquals("a", $result->children[0]->children[0]->name, "name of first argument error");
		$this->assertTrue(is_a($result->children[0]->children[1], 'qtype_cppexpression\core\operand'), "type of second argument error");
		$this->assertEquals("b", $result->children[0]->children[1]->name, "name of second argument error");
	}
	
	public static function tearDownAfterClass() {
		
	}
}