<?php

defined('MOODLE_INTERNAL') || define('MOODLE_INTERNAL', 1);
use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


class qtype_cppexpression_sortchildren_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
    public static function setUpBeforeClass() {
        qtype_cppexpression_sortchildren_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k,arr;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_sortchildren_testcase::$decl->accept_declaration();
    }
	
	public function mydataProvider() {
		return array(
			array('c * a * 4 * b', 'c * b * a * 4'),
			array('!a && (x>y) && (c > d)', 'x > y && c > d && !a'),
			array('!c && a && d && !b', 'd &&  a && !c && !b'),
			array('e + d + c + b + a', 'e + d + c + b + a'),
			array('2 * a * b * e * k', 'k * e * b * a * 2'),
			array(' a + 5 + arr[pow(a,b) * d << c] + arr[a/2]', '5 + arr[a/2] + arr[pow(a,b) * d << c] + a')
		);
	}
	
	/**
	 * @dataProvider mydataProvider
	 */
	public function test_sortchild($expr1, $expr2) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_quinemc_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_quinemc_testcase::$decl);
		
		$tree1->getTree()->children[0]->sort_children();
		$tree2->getTree()->children[0]->sort_children();
		
		$this->assertTrue($tree1->is_tree_equal($tree1->getTree(), $tree2->getTree(), TRUE));
	}
}
