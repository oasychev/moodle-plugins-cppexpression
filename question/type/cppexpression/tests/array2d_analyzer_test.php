<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

use qtype_cppexpression\array2d\array2d_grader;

class qtype_cppexpression_array2d_analyzer_testcase extends PHPUnit_Framework_TestCase {
    public function test_identifier() {
        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10');
        $this->assertEquals(10, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i');
        $tree->set_value('i', 27);
        $this->assertEquals(27, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i == height - 1 || j == width - 3');
        $tree->set_value('i', 10);
        $tree->set_value('height', 11);
        $tree->set_value('j', 1);
        $tree->set_value('width', 4);
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i == height - 1 && j == width - 3');
        $tree->set_value('i', 10);
        $tree->set_value('height', 11);
        $tree->set_value('j', 0);
        $tree->set_value('width', 4);
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i == j || j == i && i > 0');
        $tree->set_value('i', 27);
        $tree->set_value('j', 27);
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i == 27');
        $tree->set_value('i', 0);
        $tree->set_value('j', 27);
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i != 27 || i == 27 && (i == 27 && i == 27 || j == 27)');
        $tree->set_value('i', 27);
        $this->assertEquals(true, $tree->compute());
    }

    public function test_logical() {
        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('true');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('false');
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('!false');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('i && j');
        $tree->set_value('i', false);
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('true || i');
        $tree->set_value('i', false);
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('false && i');
        $tree->set_value('i', true);
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('!i || j');
        $tree->set_value('i', 0);
        $tree->set_value('j', 0);
        $this->assertEquals(true, $tree->compute());
        $tree->set_value('i', 0);
        $tree->set_value('j', 1);
        $this->assertEquals(true, $tree->compute());
        $tree->set_value('i', 1);
        $tree->set_value('j', 0);
        $this->assertEquals(false, $tree->compute());
        $tree->set_value('i', 1);
        $tree->set_value('j', 1);
        $this->assertEquals(true, $tree->compute());
    }

    public function test_ariphmetic() {
        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 + 27 - 11 + 100 - 3.2 + i');
        $tree->set_value('i', 27);
        $this->assertEquals(149.8, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('1 * 2 * i + i');
        $tree->set_value('i', 3);
        $this->assertEquals(9, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('-i');
        $tree->set_value('i', 3);
        $this->assertEquals(-3, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 / 3');
        $this->assertEquals(3, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 / 3.0');
        $this->assertEquals(10 / 3, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 % 3');
        $this->assertEquals(1, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 >> 2');
        $this->assertEquals(2, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 << 2');
        $this->assertEquals(40, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 < 11');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 < 10');
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 <= 11');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 <= 10');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('12 > 11');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('9 > 10');
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('12 >= 11');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 >= 10');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 == 10');
        $this->assertEquals(true, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 == 9');
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 != 10');
        $this->assertEquals(false, $tree->compute());

        $tree = new \qtype_cppexpression\array2d\array2d_analyzer('10 != 9');
        $this->assertEquals(true, $tree->compute());
    }
}

