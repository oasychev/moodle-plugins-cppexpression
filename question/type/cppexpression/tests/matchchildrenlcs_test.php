<?php

/**
 * Unittest for function
 * match_children_lcs($children1, $children2, &$pairs)
 * located in file inout.php
 * Created by godric.
 * User: godric
 */
use qtype_cppexpression\core\comparator;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\core\declaration;

class qtype_cppexpression_matchchildrenlcs_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
    public static function setUpBeforeClass() {
        qtype_cppexpression_matchchildrenlcs_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k,x,h;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_matchchildrenlcs_testcase::$decl->accept_declaration();
    }

    public function mydataProvider() {
        return array(
            // the same completely
            array('a + b + c', 'a + b + c', 3, array(1=>1, 2=>2, 3=>3)),
            // first children completely in second children
            array('a + c', 'a + b + c + d', 2, array(1=>1, 2=>3)),
            // second children completely in first children
            array('a + b + c + d', 'a + c', 2, array(1=>1, 3=>2)),
            // partly matching
            array('a * b * e * f * h', 'b * f * x', 2, array(2=>1, 4=>2)),
            // not have common child
            array('a + b + c', '1+2+3+4', 0, array())
        );
    }

    /**
     * @dataProvider mydataProvider
     */
    public function test_matchchildren($expr1, $expr2, $resvalue, $respairs) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_matchchildrenlcs_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_matchchildrenlcs_testcase::$decl);

        $tree1->normalize();
        $tree2->normalize();

        $comparator = new comparator($tree1->getTree(), $tree2->getTree());
        $pairs = array();

        $tree1->getTree()->children[0]->set_all_number();
        $tree2->getTree()->children[0]->set_all_number();
        foreach ($tree1->getTree()->children as $child) {
            $comparator->compare($child, $tree2->getTree());
        }

        $resp = $comparator->match_children_lcs($tree1->getTree()->children[0]->children, $tree2->getTree()->children[0]->children, $pairs);

        $this->assertEquals($resvalue, $resp);
        $this->assertEquals($respairs, $pairs);
    }}
