<?php

defined('MOODLE_INTERNAL') || define('MOODLE_INTERNAL', 1);
ini_set('memory_limit', '-1');


use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


class qtype_cppexpression_convert_testcase extends PHPUnit_Framework_TestCase {

    static $decl;

	public static function setUpBeforeClass() {
        qtype_cppexpression_convert_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_convert_testcase::$decl->accept_declaration();
	}

	public function mydataProvider() {
		return array(
			array('a - 3 * 4 + 4','a + -8'),
			array('b + a + d + e','e + d + b + a'),
			array('pow(a, 3)','a * a * a'),
			array(' pow(a, 2.4)',' pow(a, 2.4)'),
			array(' pow(a,c)','pow(a,c)'),
			array('(a+b)*2','a + a + b + b'),
			array('(a+b) * -2','- a - a - b - b'),
			array('(a+b) * 2.4','a*2.4 + b*2.4'),
			array('(a + b - c)/d','a/d + b/d - c/d'),
			array('(a * b * c)/d',' (a*b*c)*(1/d)'),
            array('a/(b/c)','a*c/b'),
			array('a[i]','*(a+i)'),
			array('a[0]','*a'),
			array('(*a).b','a->b'),
			array('a[i].c','(a+i)->c'),
			array('a[0].b','a->b'),
			array(' (*(&(a[2*k]))).b[i+2-3]','*((a+k+k)->b+i-1)'),
			array('a += b','a = a + b'),
			array('a >>= b','a = a >> b'),
			array('a - 5 > 3 + 2*(b-3)','a-b-b-2>0'),
			array('(b << 1) - 4 >= b / c','!(b/c - b - b + 4 > 0)'),
			array('b * c + 5 - d < c + a + 3','a + c + d - 2 - b * c > 0'),
			array('a + 3 <= b - c','!(3 + a + c - b > 0)'),
			array('a != b','!(a - b == 0)'),
			array('!!!a','!a'),
			array('!!!!a','a'),
			array('a && a','a'),
			array('a << 2','a + a + a + a'),
			array('a >> 4','a / 16'),
			array('*(&a)','a'),
			array('(a + b - c)*(-2)', 'c + c - b - b - a - a'),
			array('!(a>b) && a != b', 'b-a > 0'),
			array('(a/b)*(c/d)*e*f', 'a*c*e*f/(b*d)'),

            array('i++', 'i = i + 1'),
            array('++i', 'i = i + 1'),
            array('a = b++', 'a = b, b = b + 1'),
            array('a = ++b', 'b = b + 1, a = b'),
            array('a = ++b', 'a = (b = b + 1)'),
            array('i--', 'i = i - 1'),
            array('--i', 'i = i - 1'),
            array('a = b--', 'a = b, b = b - 1'),
            array('a = --b', 'b = b - 1, a = b'),
		);
	}
	
	/**
	 * @dataProvider mydataProvider
	 */
	public function test_convert($expr1, $expr2) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_convert_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_convert_testcase::$decl);

        $tree1->normalize();
        $tree2->normalize();

		$this->assertTrue($tree1->is_tree_equal($tree1->getTree(), $tree2->getTree(), TRUE));

		unset($tree1);
		unset($tree2);
	}	

}
