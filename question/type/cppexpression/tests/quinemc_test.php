<?php

defined('MOODLE_INTERNAL') || define('MOODLE_INTERNAL', 1);
use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


class qtype_cppexpression_quinemc_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
	public static function setUpBeforeClass() {
        qtype_cppexpression_quinemc_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k,x,y;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_quinemc_testcase::$decl->accept_declaration();
	}
		
	public function mydataProvider() {
		return array(
			array('a || b','a || b'),
			array('a && b && c || c','c'),
			array('a && b || !a && b','b'),
			array('a && b || b && c || c && d','a && b || b && c || c && d'),
			array('!a&&!b&&!c&&d || !a&&!b&&c&&d || !a&&b&&!c&&d || !a&&b&&c&&d || a&&b&&c&&!d || a&&b&&c&&d','!a&&d || a&&b&&c'),
			array('a > b && b > c && a > d || b > c','b > c'),
			array('!(a>b) && (c<x-y) && !(b==2) || (a>b) && !(c<x-y) && !(b==2) || (a>b) && !(c<x-y) && (b==2)','!(c<x-y)&&(a>b) || !(b==2)&&!(a>b)&&(c<x-y)')

		);
	}
	
	/**
	 * @dataProvider mydataProvider
	 */
	public function test_convertQuineMcCluskey ($expr1, $expr2) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_quinemc_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_quinemc_testcase::$decl);

        $tree1->getTree()->children[0]->convert_quine_mc_cluskey();
		
		$tmp = null;
		if (get_class($tree2->getTree()->children[0]) != 'qtype_cppexpression\core\kdimnodes\or_logic_operator') {
			$tmp = new qtype_cppexpression\core\kdimnodes\or_logic_operator();
			array_push($tmp->children, $tree2->getTree()->children[0]);
            $tree2->getTree()->children[0] = $tmp;
		}

		$this->assertTrue($tree1->is_tree_equal($tree1->getTree(), $tree2->getTree(), TRUE));
	}
	
}