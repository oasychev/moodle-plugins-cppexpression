<?php

/**
 * Unittest for function
 * compare($tree1, $tree2)
 * located in file inout.php
 * Created by godric.
 * User: godric
 */
use qtype_cppexpression\core\comparator;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\core\declaration;


class qtype_cppexpression_compare_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
    public static function setUpBeforeClass() {
        qtype_cppexpression_compare_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_compare_testcase::$decl->accept_declaration();
    }

    public function mydataProvider() {
        return array(
            // one operand
            array('a', 'b', 0, array()),
            array('c', 'c', 1, array(0=>0)),
            // completely equals
            array('a+b+c', 'a+b+c', 1, array(0=>0, 1=>1, 2=>2, 3=>3)),
            // partly with binary operator
            array('b+c', 'c%b', 0, array(2=>1)),
            // with unary operator
            array('!(a>b && c==d)', 'a>c || c==d', 0.7, array(7=>8, 12=>13, 8=>9, 11=>12, 9=>10, 1=>2,
                                                            6=>7, 2=>3, 5=>6, 3=>4, 4=>5)),
            // one operand incorrect
            array('a > b && c && d', 'a > e && c && d', 0.9, array(0=>0, 7=>7, 8=>8, 1=>1, 6=>6, 2=>2, 5=>5, 3=>3)),
            // one operator in the root incorrect
            array('i < 10 || a[i] == 3', 'i < 10 && a[i] == 3', 0.9, array(1=>1, 9=>9, 10=>10, 11=>11, 14=>14,
                                                            12=>12, 13=>13, 8 => 8, 2=>2, 7=>7, 3=>3, 4=>4, 5=>5, 6=>6))
        );
    }

    public function compare_pairs($result, $expectation) {
        if (count($result) != count($expectation)) {
            return false;
        }
        foreach ($result as $one=>$two) {
            if (!(isset($one) && $expectation[$one] != $two)) {
                return false;
            }
        }
        return true;
    }

    public function toIds($pairs) {
        $res = array();
        foreach ($pairs as $each) {
            $res[$each[0]->id()] = $each[1]->id();
        }
        return $res;
    }

    /**
     * @dataProvider mydataProvider
     */
    public function test_compare($expr1, $expr2, $fitness, $pairs) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_compare_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_compare_testcase::$decl);

        $tree1->normalize();
        $tree2->normalize();

        $tree1->getTree()->set_all_number();
        $tree2->getTree()->set_all_number();

        $comparator = new comparator($tree1->getTree(), $tree2->getTree());

        $this->assertEquals($fitness, $comparator->get_fitness());
//        $this->assertEquals($this->toIds($comparator->get_matching_pairs()), $pairs);
    }
}
