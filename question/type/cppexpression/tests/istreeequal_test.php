<?php
use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


class qtype_cppexpression_istreeequal_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
	public static function setUpBeforeClass() {
        qtype_cppexpression_istreeequal_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_istreeequal_testcase::$decl->accept_declaration();
	}
	
	public function mydataProvider() {
		return array(
			array('a * b * c', 'b * c * a', FALSE),
			array('a + b + c + e << f', 'a + b + c + e << f', TRUE),
			array('d = a[i] >> c', 'd = a[i] >> c', TRUE),
			array('& a', '& a', TRUE),
			array('a > b && c && d ', 'a > e && c && d ', FALSE),
			array('a + b + *c', ' x *  y', FALSE),
			array('a + 2.00', 'a + 2', FALSE)
		);
	}
	
	/**
	 * @dataProvider mydataProvider
	 */
	public function test_compareTree($expr1, $expr2, $result) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_convert_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_convert_testcase::$decl);

		$this->assertEquals($result, $tree1->is_tree_equal($tree1->getTree(), $tree2->getTree(), FALSE));
	}

}
