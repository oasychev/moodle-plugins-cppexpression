<?php
use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


class qtype_cppexpression_openbracket_testcase extends PHPUnit_Framework_TestCase {

    static $decl;
    public static function setUpBeforeClass() {
        qtype_cppexpression_openbracket_testcase::$decl = new declaration('int a,b,c,d,e,f,i,k,x,y;', \qtype_cppexpression\decl_type::DECL_NO_COMMENT);
        qtype_cppexpression_openbracket_testcase::$decl->accept_declaration();
    }
	
	public function mydataProvider() {
		
		return array(
			array('c * (a + b + d)', 'c*a + c*b + c*d'),
			array('(a + c) * (b + d)', 'a*b + a*d + c*b + c*d'),
            array('(c + d) * (a + c) * b', 'c*a*b + c*c*b + d*a*b + d*c*b'),
            array('(a + b) * pow(x,y) * (c%d)', 'a*pow(x,y)*(c%d) + b*pow(x,y)*(c%d)')
		);
		  
	}
	
	/**
	 * @dataProvider mydataProvider
	 */
	public function test_openbracket($expr1, $expr2) {
        $tree1 = new tree_converter($expr1, 0, qtype_cppexpression_openbracket_testcase::$decl);
        $tree2 = new tree_converter($expr2, 0, qtype_cppexpression_openbracket_testcase::$decl);
		
		$opened = $tree1->getTree()->children[0]->open_bracket();

		$this->assertTrue($tree1->is_tree_equal($opened, $tree2->getTree()->children[0], TRUE));

		}
}
