<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

use qtype_cppexpression\array2d\array2d_grader;

class qtype_cppexpression_array2d_grader_testcase extends PHPUnit_Framework_TestCase {

    private static $grader;

    public static function setUpBeforeClass() {
        self::$grader = new array2d_grader();
    }

    public function test_method() {
        $this->assertEquals(1, self::$grader->fitness('i == j', 'j == i'));
        $this->assertEquals(1, self::$grader->fitness('1', '1'));
        $this->assertEquals(1, self::$grader->fitness('0', '0'));
        $this->assertEquals(1, self::$grader->fitness('i + j == 0', 'i == 0 && j == 0'));

        $this->assertEquals(0, self::$grader->fitness('i == 0', 'i != 0'));
        $this->assertEquals(0, self::$grader->fitness('i <= height/2', 'i > height/2'));

        $this->assertEquals(0.81163434903047, self::$grader->fitness('i == height - 1', 'j == width - 1'));
        $this->assertEquals(0.10526315789474, self::$grader->fitness('i <= height/2', 'i >= height/2'));

    }
}
