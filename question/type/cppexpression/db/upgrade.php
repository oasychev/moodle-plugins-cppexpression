<?php

/**
 * Cppexpression question type upgrade code.
 */
function xmldb_qtype_cppexpression_upgrade($oldversion = 0)
{
    global $CFG, $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2014011700) {
        $table = new xmldb_table('qtype_cppexpression_options');
        $oldfield = new xmldb_field('graderanalyzerpenalty');

        if ($dbman->field_exists($table, $oldfield)) {
            $dbman->drop_field($table, $oldfield);
        }

        $newfield = new xmldb_field('hintgradeborder',  XMLDB_TYPE_FLOAT, '12', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $dbman->add_field($table, $newfield);

        upgrade_plugin_savepoint(true, 2014011800, 'qtype', 'cppexpression');
    }

    if ($oldversion < 2014011800) {

        // Rename field codedescription on table qtype_cppexpression_options to declarations.
        $table = new xmldb_table('qtype_cppexpression_options');
        $field = new xmldb_field('codedescription', XMLDB_TYPE_CHAR, '500', null, XMLDB_NOTNULL, null, 'native', 'questionid');

        // Launch rename field codedescription.
        $dbman->rename_field($table, $field, 'declarations');

        // Rename field graderanalyzertype on table qtype_cppexpression_options to gradinganalyzer.
        $field = new xmldb_field('graderanalyzertype', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null, 'declarations');

        // Launch rename field graderanalyzertype.
        $dbman->rename_field($table, $field, 'gradinganalyzer');

        // Define field graderanalyzerpenalty to be dropped from qtype_cppexpression_options.
        $field = new xmldb_field('graderanalyzerpenalty');

        // Conditionally launch drop field graderanalyzerpenalty.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Rename field stackhintanalyzertype on table qtype_cppexpression_options to array2dmode.
        $field = new xmldb_field('stackhintanalyzertype', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null, 'gradinganalyzer');

        // Launch rename field stackhintanalyzertype.
        $dbman->rename_field($table, $field, 'array2dmode');

        // Rename field stackhintanalyzerpenalty on table qtype_cppexpression_options to array2dpenalty.
        $table = new xmldb_table('qtype_cppexpression_options');
        $field = new xmldb_field('stackhintanalyzerpenalty', XMLDB_TYPE_FLOAT, '12, 7', null, XMLDB_NOTNULL, null, null, 'array2dmode');

        // Launch rename field stackhintanalyzerpenalty.
        $dbman->rename_field($table, $field, 'array2dpenalty');

        // Define field algebrahintanalyzertype to be dropped from qtype_cppexpression_options.
        $field = new xmldb_field('algebrahintanalyzertype');

        // Conditionally launch drop field algebrahintanalyzertype.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

// Rename field algebrahintanalyzerpenalty on table qtype_cppexpression_options to hintgradeborder.
        $field = new xmldb_field('algebrahintanalyzerpenalty', XMLDB_TYPE_FLOAT, '12, 7', null, XMLDB_NOTNULL, null, null, 'array2dpenalty');

        // Launch rename field algebrahintanalyzerpenalty.
        $dbman->rename_field($table, $field, 'hintgradeborder');

        // Cppexpression savepoint reached.
        upgrade_plugin_savepoint(true, 2014111800, 'qtype', 'cppexpression');
    }

    if ($oldversion < 2014112501) {

        // Changing type of field gradinganalyzer on table qtype_cppexpression_options to char.
        $table = new xmldb_table('qtype_cppexpression_options');
        $field = new xmldb_field('gradinganalyzer', XMLDB_TYPE_CHAR, '30', null, XMLDB_NOTNULL, null, null, 'declarations');

        // Launch change of type for field gradinganalyzer.
        $dbman->change_field_type($table, $field);

        $field = new xmldb_field('array2dmode');

        // Conditionally launch drop field id.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        $field = new xmldb_field('array2dpenalty');

        // Conditionally launch drop field id.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Cppexpression savepoint reached.
        upgrade_plugin_savepoint(true, 2014112501, 'qtype', 'cppexpression');
    }

    if ($oldversion < 2014112505) {

        // Define field prohibitoperators to be added to qtype_cppexpression_options.
        $table = new xmldb_table('qtype_cppexpression_options');
        $field = new xmldb_field('prohibitoperators', XMLDB_TYPE_NUMBER, '17', null, XMLDB_NOTNULL, null, '0', 'hintgradeborder');

        // Conditionally launch add field prohibitoperators.
        if (!$dbman->field_exists($table, $field)) {
            // Launch changes for field prohibitoperators.
            $dbman->add_field($table, $field);
        }

        // Cppexpression savepoint reached.
        upgrade_plugin_savepoint(true, 2014112505, 'qtype', 'cppexpression');
    }


    return true;
}