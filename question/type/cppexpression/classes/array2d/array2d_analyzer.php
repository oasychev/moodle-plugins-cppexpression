<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes for array2d expression analyzer.
 *
 * Parse and analyze logical expression that work with variables "i", "j", "height", "width".
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qtype_cppexpression\array2d;

defined('MOODLE_INTERNAL') || die();

use qtype_cppexpression\core\tree_converter;

/**
 * Class array2d_analyzer
 *
 * @package qtype_cppexpression\array2d
 */
class array2d_analyzer extends tree_converter {

    /**
     * array2d_analyzer constructor.
     *
     * @param $expr
     */
    public function __construct($expr) {
        parent::__construct($expr, 0, null);
        $this->find_variables();
    }

    /** Node fabric
     *
     * @param $classname
     * @return node|null
     */
    protected function getinstance($classname) {
        if (array_key_exists($classname, self::$class_names)) {
            return new self::$class_names[$classname][0]();
        } else {
            return null;
        }
    }

    /** Compute value of expression
     *
     * @return mixed
     */
    public function compute() {
        return $this->getTree()->children[0]->compute();
    }

    /**
     * @var array Fabric convertation map from AST
     */
    static public $class_names = array(
            'identifier' => array('qtype_cppexpression\array2d\array2d_nodes\computable_operand', -1),

            'expr_logical_or' => array('qtype_cppexpression\array2d\array2d_nodes\kdimnodes\computable_or_logic_operator', 0),
            'expr_logical_and' => array('qtype_cppexpression\array2d\array2d_nodes\kdimnodes\computable_and_logic_operator', 1),
            'expr_plus' => array('qtype_cppexpression\array2d\array2d_nodes\kdimnodes\computable_plus_operator', 2),
            'expr_multiply' => array('qtype_cppexpression\array2d\array2d_nodes\kdimnodes\computable_multi_operator', 3),
            'expr_list' => array('qtype_cppexpression\array2d\array2d_nodes\kdimnodes\computable_sequencing_operator', 4),

            'expr_logical_not' => array('qtype_cppexpression\array2d\array2d_nodes\onedimnodes\computable_not_logic_operator', 6),
            'expr_unary_minus' => array('qtype_cppexpression\array2d\array2d_nodes\onedimnodes\computable_unary_minus_operator', 7),

            'expr_minus' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_minus_operator', 15),
            'expr_modulosign' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_mod_operator', 16),
            'expr_division' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_div_operator', 17),
            'expr_equal' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_equal_operator', 18),
            'expr_notequal' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_not_equal_operator', 19),
            'expr_rightshift' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_shift_right_operator', 23),
            'expr_leftshift' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_shift_left_operator', 24),
            'expr_lesser_or_equal' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_less_equal_operator',
                    25),
            'expr_greater_or_equal' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_greater_equal_operator',
                    26),
            'expr_lesser' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_less_operator', 27),
            'expr_greater' => array('qtype_cppexpression\array2d\array2d_nodes\binarynodes\computable_greater_operator', 28)
    );

    /**
     * @var array Links to nodes with variables : array('varname' => array(nodes))
     */
    protected $varmap = [];

    /**
     * Find all variables in tree
     */
    protected function find_variables() {
        $stack = [$this->getTree()->children[0]];
        while (!empty($stack)) {
            $node = array_pop($stack);

            if (is_a($node, 'qtype_cppexpression\array2d\array2d_nodes\computable_operand')
                    && preg_match('/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/', $node->name->string())) {
                $this->varmap[$node->name->string()] [] = $node;
            }

            $stack = array_merge($stack, $node->children);
        }
    }

    /** Contains variable in tree
     *
     * @param $varname
     * @return bool
     */
    public function contains_variable($varname) {
        return array_key_exists($varname, $this->varmap);
    }

    /** Set value to variable
     *
     * @param $varname
     * @param int $value
     * @return bool Success
     */
    public function set_value($varname, $value = 0) {
        if (!$this->contains_variable($varname)) {
            return false;
        }

        foreach ($this->varmap[$varname] as $val) {
            $val->value = $value;
        }

        return true;
    }

}