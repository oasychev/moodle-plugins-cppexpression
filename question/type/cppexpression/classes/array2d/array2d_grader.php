<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes for grader analyzer.
 *
 * Analyzing logical expression that work with variables "i", "j", "height", "width".
 * The grade determine on how much cells in this array expressions yield equal result.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qtype_cppexpression\array2d;

defined('MOODLE_INTERNAL') || die();

namespace qtype_cppexpression\array2d;

use qtype_cppexpression\accepting_error;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\array2d\array2d_analyzer;
use qtype_cppexpression\decl_type;
use qtype_cppexpression\grading_analyzer;

/**
 * Array2grader class
 *
 * @package qtype_cppexpression\array2d
 */
class array2d_grader extends grading_analyzer {
    /**
     * @return string
     */
    public function name() {
        return 'array2d';
    }

    /**
     * @var int Even matrix size
     */
    public static $evenarraysize = 10;

    /**
     * @var int Odd matrix size
     */
    public static $oddarraysize = 9;

    /**
     * @param string $answer
     * @param string $response
     * @param \qtype_cppexpression\declarations $declarations
     * @return array|\qtype_cppexpression\fitness
     */
    public function fitness($answer, $response, $declarations = null) {
        $answerarr = self::execute_expression($answer);
        $responsearr = self::execute_expression($response);

        if ($answerarr === false || $responsearr === false) {
            return 0;
        }

        $successcount = 0.0;
        $allcount = self::$evenarraysize * self::$evenarraysize + self::$oddarraysize * self::$oddarraysize +
                self::$oddarraysize * self::$evenarraysize * 2;

        for ($i = 0; $i < count($answerarr); $i++) {
            $height = count($answerarr[$i]);
            $width = count($answerarr[$i][0]);

            for ($j = 0; $j < $height; $j++) {
                for ($k = 0; $k < $width; $k++) {
                    if ($answerarr[$i][$j][$k] == $responsearr[$i][$j][$k]) {
                        $successcount++;
                    }
                }
            }
        }

        return $successcount / $allcount;
    }

    /**
     * @param $expression
     * @return array|bool
     */
    public static function execute_expression($expression) {
        if (count(self::accept_expression($expression)) > 0) {
            return false;
        }

        $tree = new array2d_analyzer($expression);

        $matrixsizes = [
                [self::$evenarraysize, self::$evenarraysize],
                [self::$oddarraysize, self::$oddarraysize],
                [self::$oddarraysize, self::$evenarraysize],
                [self::$evenarraysize, self::$oddarraysize]
        ];

        $result = [];
        foreach ($matrixsizes as $currentsize) {
            $tree->set_value("height", $currentsize[0]);
            $tree->set_value("width", $currentsize[1]);

            $currentmatrix = [];
            for ($i = 0; $i < $currentsize[0]; ++$i) {
                $tree->set_value("i", $i);
                for ($j = 0; $j < $currentsize[1]; ++$j) {
                    $tree->set_value("j", $j);

                    $value = $tree->compute();
                    $currentmatrix[$i][] = $value ? 1 : 0;
                }
            }
            $result[] = $currentmatrix;
        }

        return $result;
    }

    /**
     * @param \qtype_cppexpression\expression $expression
     * @param int $prohibits
     * @param null $declaration
     * @return array
     */
    public static function accept_expression($expression, $prohibits = 0, $declaration = null) {
        // Replace.
        $expression = preg_replace('/width|height|i|j/', '0', $expression);

        $tree = new array2d_analyzer($expression);
        $errors = $tree->getErrors();

        // If contains other ids (except i,j,width,height).
        if (preg_match('/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/', $expression, $matches)) {
            $errors[] = new \qtype_cppexpression\errors\syntax_error(null, "unacceptable variable {$matches[0]}", $expression,
                    'array2d');
        }

        return $errors;
    }

    /** Override parent method
     *
     * @return int
     */
    public static function is_need_declaration() {
        return decl_type::NO_DECLARATION;
    }
}
