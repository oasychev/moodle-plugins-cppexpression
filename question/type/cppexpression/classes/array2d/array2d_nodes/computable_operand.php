<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace qtype_cppexpression\array2d\array2d_nodes;

use qtype_cppexpression\core\computable_node;
use qtype_cppexpression\core\operand;

defined('MOODLE_INTERNAL') || die();

class computable_operand extends operand implements computable_node {

    /**
     * Compute value of corresponding node
     *
     * @return mixed value
     */
    public function compute() {
        if ($this->name->string() == "true") {
            return true;
        }

        if ($this->name->string() == "false") {
            return false;
        }

        switch ($this->typeofvar) {
            case "int":
                return (int) $this->value;
            case "float":
            case "double":
                return (double) $this->value;
        }

        return $this->value;
    }
}