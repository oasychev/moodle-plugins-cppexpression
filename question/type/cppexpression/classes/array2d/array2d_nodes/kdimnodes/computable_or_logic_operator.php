<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace qtype_cppexpression\array2d\array2d_nodes\kdimnodes;

use qtype_cppexpression\core\computable_node;
use qtype_cppexpression\core\kdimnodes\or_logic_operator;

defined('MOODLE_INTERNAL') || die();

class computable_or_logic_operator extends or_logic_operator implements computable_node {

    /**
     * Compute value of corresponding node
     *
     * @return mixed value
     */
    public function compute() {
        $result = false;
        for ($i = 0, $childs = count($this->children); $i < $childs && !$result; $i++) {
            $result = $result || $this->children[$i]->compute();
        }
        return $result;
    }
}