<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines class for analyser based hint.
 *
 * Analyzing logical expression that work with 2d array expression and two indexes "i" and "j".
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qtype_cppexpression\array2d;

use html_writer;
use qtype_poasquestion\renderer;
use qtype_cppexpression\decl_type;

defined('MOODLE_INTERNAL') || die();

class array2d_hint extends \qtype_cppexpression\hint_from_analyzer {

    /**
     * Check: does analyzer need declaration to work correctly?
     *
     * @return int NO_DECLARATION if analyzer DO NOT need declaration to work
     *             DECL_NO_COMMENT if analyzer need declaration without comment
     *             DECL_WITH_COMMENT if analyzer need declaration with comment
     */
    public static function is_need_declaration() {
        return decl_type::NO_DECLARATION;
    }

    /**
     * Renders hint information for given response using question renderer.
     *
     * Response may be omitted for non-response based hints.
     *
     * @param renderer question renderer which could be used to render things
     */
    public function render_hint($renderer, \question_attempt $qa, \question_display_options $options, $response = null) {
        if ($response === null || $this->mode == 2) {
            return "";
        }

        $tableshtml = "";
        $tablesheader = ["EVEN x EVEN", "ODD x ODD", "ODD x EVEN", "EVEN x ODD"];
        switch ($this->mode) {
            case 1:
                $responsestr = $response['answer'];
                if ($responsestr === null || strlen($responsestr) == 0) {
                    return "";
                }

                $responsearrays = array2d_grader::execute_expression($responsestr);
                $htmltables = [];
                $i = 0;
                foreach ($responsearrays as $arr) {
                    $table = html_writer::tag('div', "{$tablesheader[$i]} :\n", ['class' => 'array2d_hint_text']);
                    foreach ($arr as $row) {
                        $tr = "";
                        foreach ($row as $element) {
                            $tr .= html_writer::tag("td", $element);
                        }
                        $table .= html_writer::tag('tr', $tr);
                    }
                    $table = html_writer::tag('table', $table, ['class' => 'array2d_hint_table']);

                    $htmltables[$i++] = html_writer::tag('td', $table);
                }

                $tableshtml = html_writer::tag('tr', $htmltables[0] . $htmltables[1]);
                $tableshtml .= html_writer::tag('tr', $htmltables[2] . $htmltables[3]);

                $tableshtml = html_writer::tag('table', $tableshtml, ['id' => 'hint_tables']);
                break;
            case 3:
                $responsestr = $response['answer'];
                if ($responsestr === null || strlen($responsestr) == 0) {
                    return "";
                }
                $answerstr = $this->question->get_best_fit_answer($response)['answer']->answer;

                $responsearrays = array2d_grader::execute_expression($responsestr);
                $answerarrays = array2d_grader::execute_expression($answerstr);

                $htmltables = [];

                for ($i = 0; $i < count($responsearrays); ++$i) {
                    $table = html_writer::tag('div', "{$tablesheader[$i]} :\n", ['class' => 'array2d_hint_text']);
                    for ($j = 0; $j < count($responsearrays[$i]); ++$j) {
                        $tr = "";
                        for ($k = 0; $k < count($responsearrays[$i][$j]); ++$k) {
                            if ($responsearrays[$i][$j][$k] == $answerarrays[$i][$j][$k]) {
                                $tr .= html_writer::tag("td", $responsearrays[$i][$j][$k], ['class' => 'correct']);
                            } else {
                                $tr .= html_writer::tag("td", "<del>{$responsearrays[$i][$j][$k]}</del>/{$answerarrays[$i][$j][$k]}",
                                        ['class' => 'incorrect']);
                            }
                        }
                        $table .= html_writer::tag('tr', $tr);
                    }
                    $table = html_writer::tag('table', $table, ['class' => 'array2d_hint_table']);

                    $htmltables[$i] = html_writer::tag('td', $table);
                }

                $tableshtml = html_writer::tag('tr', $htmltables[0] . $htmltables[1]);
                $tableshtml .= html_writer::tag('tr', $htmltables[2] . $htmltables[3]);

                $tableshtml = html_writer::tag('table', $tableshtml, ['id' => 'hint_tables']);
                break;
        }

        // Add hint description.
        $decr = html_writer::tag('div', get_string('array2d_hint_description_for_mode' . $this->mode, 'qtype_cppexpression'),
                ['class' => 'array2d_hint_text']);
        $tableshtml = $decr . $tableshtml;

        return html_writer::tag('div', $tableshtml, ['id' => 'array2d_hint']);
    }

    public static function accept_expression($answer, $prohibits = 0, $declarations = null) {
        return array2d_grader::accept_expression($answer, $prohibits, $declarations);
    }

    public function hint_available($response = null) {
        // TODO Check response value.
        return $this->hint_response_based();
    }

    /**
     * Hint analyzer class names should be qtype_cppexpression\name()_hint .
     */
    public function name() {
        return 'array2d';
    }
}
