<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes for grader analyzer.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression;
defined('MOODLE_INTERNAL') || die();
use qtype_cppexpression\core\comparator;
use qtype_cppexpression\core\declaration;
use qtype_cppexpression\core\tree_converter;


/**
 * Class of grader analyzer.
 */
abstract class grading_analyzer implements expression_analyzer {

    /** @var number mode (option) of analyser */
    // public $mode; - TODO - decide whether mode is needed for grading.

    public function __construct(/*$mode*/) {
        // $this->mode = $mode;
    }

    /**
     * Return grading modes of analyser.
     *
     * TODO - decide whether it is needed at all.
     * @return array of options: array('<mode>' => '<name>', ...)
     */
   /* public static function get_modes() {
        return array(
            '0' => get_string('cppexpr_gradeonly', 'qtype_cppexpression')
        );
    }*/

    /**
     * Grader analyzer class names should be qtype_cppexpression\name()_grader .
     */
    abstract public function name();

    /**
     * Notice for student when enter response
     * @return string notice for student
     */
    public static function get_supporting_notice() {
        return "";
    }

    /**
     * Calculate fitness of the response to the answer. Fitness is a value in range [0,1]
     *
     * @param $answer string answer (of teacher).
     * @param $response string response (of student).
     * @param $declarations declarations field from the question.
     * @return fitness value
     */
    public function fitness($answer, $response, $declarations) {
        $treedeclarations = null;
        if (!empty($declarations)) {
            $treedeclarations = new declaration($declarations);
            $treedeclarations->accept_declaration();
        }
        $ans = new tree_converter($answer, 0, $treedeclarations);
        $resp = new tree_converter($response, 0, $treedeclarations);
        if ($ans->emptyErrors() && $resp->emptyErrors()) {
            $ans->normalize();
            $resp->normalize();
            $comparator = new comparator($ans->getTree(), $resp->getTree());
            return array('match' => $comparator->get_fitness(), 'answer' => $answer);
        } else {
            return array('match' => 0, 'answer' => $answer);
        }
    }

    /**
     * Checking if grader can work with expression
     * @param expression $expression expression in string
     * @param int $prohibits int flags of prohibit operators.
     * @param null $declaration declarations field from the question.
     * @return array list of errors, empty if expression is accepted.
     */
    public static function accept_expression($expression, $prohibits=0, $declaration = null) {
        $result = array();
        return $result;
    }
}
