<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines types of declaration
 *
 * @package     cppexpression
 * @author      Phan Tuan Anh
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * Created by PhpStorm.
 * User: godric
 * Date: 05.04.17
 * Time: 15:04
 */

namespace qtype_cppexpression;


abstract class decl_type {
    const NO_DECLARATION = 0;
    const DECL_NO_COMMENT = 1;
    const DECL_WITH_COMMENT = 2;
}