<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define('MOODLE_INTERNAL', 1);
global $CFG;
$CFG = new stdClass();
$CFG->dirroot = '/data/russia/diploma/moodle';
$CFG->libdir = $CFG->dirroot . '/lib';


require_once('inout.php');

function process_each_expression($filename, $filegv, $file2='t1.gv') {
    try {
        $exp = readexp($filename);
        /** @var qtype_cppexpression_base_node $tree */
        $tree = build_tree($exp)['root'];

        $tree->set_all_number();

        // Before.
        tree2dot($file2, $tree);

        // ... преобразовать.
        normalize($tree);

        $tree->set_all_number();

        // ... Печать DOT файл.
        tree2dot($filegv, $tree);

        return $tree;
    } catch (Exception $e) {
        echo "In file $filename: ".$e->getMessage()." \n";
        return null;
    }
}

if ($argc > 3) {
    echo "Too much argument in command line\n";
} else if ($argc < 3) {
    echo "Too few argument in command line\n";
} else {
    // ... Преобразовать каждое выражение.
    $tree1 = process_each_expression($argv[1], "tree1.gv");
    $tree2 = process_each_expression($argv[2], "tree2.gv", 't2.gv');

    // ... Сравнение.
    if ($tree1 !== null && $tree2 !== null) {
        $file = fopen("result.txt", "w");
        if (is_tree_equal($tree1, $tree2)) {
            fwrite($file, 'Expression equals');
            echo 'Expression equals' . PHP_EOL;
        } else {
            fwrite($file, 'Epression NOT equals');
            echo 'Epression NOT equals' . PHP_EOL;
        }
        fclose($file);

        $tmp = new qtype_cppexpression_comparator($tree1, $tree2);
        $tmp->get_fitness();
        $tmp->print_max_common_tree_to_dot('common.gv');
    }
}