<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines classes for grader analyzer.
 *
 * Analyzing logical expression that work with 2d array "arr" and two indexes "i" and "j".
 * The grade determine on how much cells in this array expressions yield equal result.
 *
 * @package     cppexpression
 * @author      Phan Tuan Anh
 * @copyright   &copy; 2016 anhptvolga, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression\compexpr;
use qtype_cppexpression\accepting_error;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\decl_type;
use qtype_cppexpression\grading_analyzer;

class compexpr_grader extends grading_analyzer {

    /** @var number mode (option) of analyser */
    // public $mode; - TODO - decide whether mode is needed for grading.

    public function __construct(/*$mode*/) {
        // $this->mode = $mode;
    }

    /**
     * Return grading modes of analyser.
     *
     * TODO - decide whether it is needed at all.
     * @return array of options: array('<mode>' => '<name>', ...)
     */
    /* public static function get_modes() {
         return array(
             '0' => get_string('cppexpr_gradeonly', 'qtype_cppexpression')
         );
     }*/

    /**
     * Grader analyzer class names should be qtype_cppexpression\name()_grader .
     */
    public function name() {
        return 'compexpr';
    }

    public static function accept_expression($expression, $prohibits=0, $declaration = null) {
        $tmp = new tree_converter($expression, $prohibits, $declaration);
        return $tmp->getErrors();
    }

    public static function get_not_supported() {
        return array();
    }

    public static function get_supporting_notice() {
        $tmp = compexpr_grader::get_not_supported();
        if (empty($tmp)) {
            return '';
        }
        $notice = get_string('notice_not_support', 'qtype_cppexpression');
        foreach ($tmp as $oprt) {
            $notice .= '<br>' . get_string($oprt, 'qtype_cppexpression');
        }
        return $notice;
    }

    /**
     * Check: does analyzer need declaration to work correctly?
     * @return boolean true if analyzer need declaration to work
     */
    public static function is_need_declaration() {
        return decl_type::DECL_NO_COMMENT;
    }
}
