<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines abstract class for analyser based hint.
 *
 * Analyzing logical expression that work with 2d array "arr" and two indexes "i" and "j".
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression\compexpr;
use qtype_cppexpression\DECL_TYPE;
use qtype_poasquestion\question;

defined('MOODLE_INTERNAL') || die();

class compexpr_hint extends  \qtype_cppexpression\hint_from_analyzer {

    /**
     * Renders hint information for given response using question renderer.
     *
     * Response may be omitted for non-response based hints.
     * @param renderer question renderer which could be used to render things
     * @return string hint
     */
    public function render_hint($renderer, \question_attempt $qa, \question_display_options $options, $response = null) {
        // TODO: Implement render_hint() method.
        return 'Not implemented yet';
    }

    public static function accept_expression($answer, $prohibits=0, $declaration = null) {
        // TODO: Implement accept_expression() method.
        $result = array();
        return $result;
    }

    /**
     * Hint analyzer class names should be qtype_cppexpression\name()_hint .
     */
    public function name() {
        return 'compexpr';
    }

    /**
     * Check: does analyzer need declaration to work correctly?
     * @return boolean true if analyzer need declaration to work
     */
    public static function is_need_declaration() {
        return DECL_TYPE::DECL_NO_COMMENT;
    }
}
