<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines interface for analyser.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression;
use qtype_cppexpression\core\declaration;

defined('MOODLE_INTERNAL') || die();


/**
 * Interface for analyser classes.
 */
interface expression_analyzer {

    /**
     * Check: can analyser process $expression.
     *
     * @param $expression expression that is processed by analyser.
     * @param $declaration declarations of variables in expression, equals null if not need.
     * @param $prohibits int flags of prohibit operators.
     * @return array of accepting_error objects, empty array for accepting.
     */
    public static function accept_expression($expression, $prohibits=0, $declaration = null);


    /**
     * Check: does analyzer need declaration to work correctly?
     * @return int NO_DECLARATION if analyzer DO NOT need declaration to work
     *             DECL_NO_COMMENT if analyzer need declaration without comment
     *             DECL_WITH_COMMENT if analyzer need declaration with comment
     */
    public static function is_need_declaration();
}
