<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace qtype_cppexpression\core;

class operand extends base_node {

    /** @var null|string name of operand */
    public $name = null;
    /** @var null|double value of operand*/
    public $value = null;
    /** @var null|double init value of operand in declaration */
    public $initvalue = null;
    /** @var null|string type of operand */
    public $typeofvar = null;
    /** @var  null|double starting range for showing wrong result */
    public $startrange;
    /** @var  null|double ending range for showing wrong result */
    public $endrange;
    /** @var  null|double step for double type*/
    public $step;

    public function __construct($name = "", $value = null, $ivl = null, $type = null, $sr = null, $er = null, $step = null) {
        $this->name = $name;
        $this->value = $value;
        $this->initvalue = $ivl;
        $this->treeinstring = $name;
        $this->ptonewchild = null;
        $this->typeofvar = $type;
        $this->startrange = $sr;
        $this->endrange = $er;
        $this->step = $step;
    }

    public function convert($parent) {

    }

    public function calculate_tree_in_string() {
        $this->treeinstring = $this->name;
    }

    public function delete_children() {
        unset ($this->name);
        unset ($this->value);
        unset ($this->typeofvar);
        $this->name = null;
        $this->value = null;
        $this->typeofvar = null;
    }

    public function __clone() {

    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return $this->name;
    }
}