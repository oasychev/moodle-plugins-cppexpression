<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
defined('MOODLE_INTERNAL') || die();
//global $CFG;
//require_once $CFG->dirroot.'/lib/classes/text.php';
require_once($CFG->dirroot .'/blocks/formal_langs/language_cpp_parseable_language.php');
//require_once($CFG->dirroot .'/question/type/cppexpression/core/compexpr/core/operand.php');
//require_once($CFG->dirroot .'/question/type/cppexpression/core/compexpr/core/comparator.php');
//require_once($CFG->dirroot .'/question/type/cppexpression/core/accepting_error.php');
use qtype_cppexpression\core;
use qtype_cppexpression\core\declaration;

/**
 * Печать дерево в формат DOT
 *
 * \param [in] file указатель на выходный файл
 * \param [in] curnode указатель на текущий узел
 *
 */
function print_tree_to_dot($file, $curnode) {
    static $globalid = 0; // ... следующий идентификатор для узлов.
    $id = $globalid; // ... идентификатор для данный узла.
    // ... инкремент общий индетификатор.
    ++$globalid;
    // ... печать определение для данного узла?
    if  ($curnode == null) return;
    if (is_a($curnode, 'qtype_cppexpression\core\operand')) {
        fwrite($file, $id.' [label = "'.$curnode->name . "\"]\n");
    } else {
        fwrite($file, $id . ' [label = "' . $curnode->get_label() . "\"]\n");
        foreach ($curnode->children as $value) {
            $next = $globalid;
            print_tree_to_dot($file, $value);
            fwrite($file, $id . ' -> ' . $next . "\n");
        }
    }
}

function tree2dot($filename, $tree) {
    $file = fopen($filename, "w");
    fwrite($file, "digraph {\n");
    print_tree_to_dot($file, $tree);
    fwrite($file, '}');
    fclose($file);
}

/**
 * \brief Функция сравнения двух деревьев
 *
 * \param [in] tree1 указатель на узел первого дерева
 * \param [in] tree2 указатель на узел второго дерева
 * \param [in] isprintdiff флаг: печать ли разницы двух деревьев
 * \return true если два дерева равны, в противном случае false
 *
 */
function is_tree_equal($tree1, $tree2, $isprintdiff = false) {

    if (get_class($tree1) != get_class($tree2)) {
        // ... печать разницы при тестировать?
        if ($isprintdiff) {
            echo "____ type of node diff\n";
            echo "__________ result = ".get_class($tree1)."\n";
            echo "__________ expected = ".get_class($tree2)."\n";
        }
        return false;
    }
    if (is_a($tree1, 'qtype_cppexpression\core\operand')) { // ... операнд?
        if ($tree1->name != $tree2->name) {
            // ... печать разницы при тестировать?
            if ($isprintdiff) {
                echo "____ name of operand diff\n";
                echo "__________ result = ".$tree1->name."\n";
                echo "__________ expected = ".$tree2->name."\n";
            }
            return false;
        }
        if ($tree1->value != $tree2->value) {
            // ... печать разницы при тестировать?
            if ($isprintdiff) {
                echo "____ number of operand diff\n";
                echo "__________ result = ".$tree1->value."\n";
                echo "__________ expected = ".$tree2->value."\n";
            }
            return false;
        }
        if ($tree1->typeofvar!= $tree2->typeofvar) {
            // ... печать разницы при тестировать?
            if ($isprintdiff) {
                echo "____ number of operand diff\n";
                echo "__________ result = ".$tree1->typeofvar."\n";
                echo "__________ expected = ".$tree2->typeofvar."\n";
            }
            return false;
        }
    } else { // if (is_subclass_of($tree1, 'qtype_cppexpression\core\k_dim_node')) { // ... k-dim.
        $res = count($tree1->children) == count($tree2->children);
        $i = 0;
        while ($res && $i < count($tree1->children)) {
            $res = $res && is_tree_equal($tree1->children[$i], $tree2->children[$i], $isprintdiff);
            $i ++;
        }
        return $res;
    }

    return true;
}

function readexp($filename) {
    $file = fopen($filename, "r");
    if ($file !== false) {
        $res = fgets($file);
        fclose($file);
        $res = rtrim($res);
    } else {
        throw new Exception("file not found");
    }
    if (strlen($res) == 0) {
        throw new Exception("no expession in file");
    }
    return $res;
}

/**
 * Функция для создания экземплю узла по именю класса
 */
function getinstane($classname) {
    static $fullnames = array(
        'identifier'=> 'qtype_cppexpression\core\operand',
        'expr_logical_or'=> 'qtype_cppexpression\core\kdimnodes\or_logic_operator',
        'expr_logical_and'=> 'qtype_cppexpression\core\kdimnodes\and_logic_operator',
        'expr_plus'=> 'qtype_cppexpression\core\kdimnodes\plus_operator',
        'expr_multiply'=>'qtype_cppexpression\core\kdimnodes\multi_operator',
        'expr_list'=> 'qtype_cppexpression\core\kdimnodes\sequencing_operator',
        'expr_function_call'=> 'qtype_cppexpression\core\kdimnodes\function_call',

        'expr_logical_not'=> 'qtype_cppexpression\core\onedimnodes\not_logic_operator',
        'expr_unary_minus'=> 'qtype_cppexpression\core\onedimnodes\unary_minus_operator',
        'expr_dereference'=> 'qtype_cppexpression\core\onedimnodes\dereference_operator',
        'expr_take_adress'=> 'qtype_cppexpression\core\onedimnodes\reference_operator',

        'expr_postfix_increment'=> 'qtype_cppexpression\core\onedimnodes\postfix_increment_operator',
        'expr_postfix_decrement'=> 'qtype_cppexpression\core\onedimnodes\postfix_decrement_operator',
        'expr_prefix_increment'=> 'qtype_cppexpression\core\onedimnodes\prefix_increment_operator',
        'expr_prefix_decrement'=> 'qtype_cppexpression\core\onedimnodes\prefix_decrement_operator',

        'expr_assign'=> 'qtype_cppexpression\core\binarynodes\assign_operator',
        'expr_minus'=> 'qtype_cppexpression\core\binarynodes\minus_operator',
        'expr_modulosign'=> 'qtype_cppexpression\core\binarynodes\mod_operator',
        'expr_division'=> 'qtype_cppexpression\core\binarynodes\div_operator',
        'expr_equal'=> 'qtype_cppexpression\core\binarynodes\equal_operator',
        'expr_notequal'=> 'qtype_cppexpression\core\binarynodes\not_equal_operator',
        'try_value_access'=> 'qtype_cppexpression\core\binarynodes\mem_acc_operator',
        'try_pointer_access'=> 'qtype_cppexpression\core\binarynodes\pt_mem_acc_operator',
        'expr_array_access'=> 'qtype_cppexpression\core\binarynodes\subscript_operator',
        'expr_rightshift'=> 'qtype_cppexpression\core\binarynodes\shift_right_operator',
        'expr_leftshift'=> 'qtype_cppexpression\core\binarynodes\shift_left_operator',
        'expr_lesser_or_equal'=> 'qtype_cppexpression\core\binarynodes\less_equal_operator',
        'expr_greater_or_equal'=> 'qtype_cppexpression\core\binarynodes\greater_equal_operator',
        'expr_lesser'=> 'qtype_cppexpression\core\binarynodes\less_operator',
        'expr_greater'=> 'qtype_cppexpression\core\binarynodes\greater_operator',
        'expr_plus_assign'=> 'qtype_cppexpression\core\binarynodes\plus_assign_operator',
        'expr_minus_assign'=> 'qtype_cppexpression\core\binarynodes\minus_assign_operator',
        'expr_multiply_assign'=> 'qtype_cppexpression\core\binarynodes\multi_assign_operator',
        'expr_division_assign'=> 'qtype_cppexpression\core\binarynodes\div_assign_operator',
        'expr_leftshift_assign'=> 'qtype_cppexpression\core\binarynodes\shl_assign_operator',
        'expr_rightshift_assign'=> 'qtype_cppexpression\core\binarynodes\shr_assign_operator'
    );
    if (array_key_exists($classname, $fullnames)) {
        return new $fullnames[$classname];
    } else {
        return null;
    }
}

function filter_operand($node, $expression, $declarations, &$res, &$curnode) {
    $nodetype = $node->type();   // ... тип узла.
    if ($declarations !== null && $nodetype === 'identifier') {
        $curnode = $declarations->get_operand($node->value());
        if ($curnode == null) {
            array_push($res['errors'],
                new \qtype_cppexpression\errors\missing_declaration($node, $expression, $node->value()));
        }
    } else {
        // If declaration not exist, create operand.
        $curnode = new \qtype_cppexpression\core\operand();
        $curnode->name = $node->value();
        $curnode->treeinstring = $curnode->name;
        if ($nodetype === 'numeric') {
            $tmp = $curnode->name->string();
            if ($tmp === (string) intval($tmp)) {
                $curnode->typeofvar = 'int';
                $curnode->value = intval($tmp);
            } else {
                $curnode->typeofvar = 'double';
                $curnode->value = doubleval($tmp);
            }
        }
    }
}

function filter_function_call($node, $expression, $declarations, &$res, &$curnode) {
    // Function has arguments?
    if (count($node->children) > 3) {
        $args = filter_node($node->children[2], $expression, $declarations);
    }

    $res['errors'] = array_merge($res['errors'], $args['errors']);

    $curnode = getinstane($node->type());
    $curnode->name = $node->children[0]->value();
    $curnode->children[] = $args['root'];

    $curnode->treeinstring = "pow(";
    $curnode->treeinstring .= ($args['root'] !== null) ? $args['root']->treeinstring : " " . ') ';
}

function filter_property_access($node, $expression, $declarations, &$res, &$curnode) {
    $leftchild = filter_node($node->children[0]->children[0], $expression, $declarations);
    $rightchild = filter_node($node->children[1], $expression, $declarations);
    $res['errors'] = array_merge($res['errors'], $leftchild['errors'], $rightchild['errors']);
    $curnode = getinstane($node->children[0]->type());
    $curnode->children[0] = $leftchild['root'];
    $curnode->children[1] = $rightchild['root'];
    $curnode->treeinstring = $node->children[0]->children[1]->value()." ";
    $curnode->treeinstring .= ($leftchild['root'] !== null) ? $leftchild['root']->treeinstring : " "." ";
    $curnode->treeinstring .= ($rightchild['root'] !== null) ? $rightchild['root']->treeinstring : " ";
}

function filter_k_dim($node, $expression, $declarations, &$res, &$curnode) {
    $leftchild = filter_node($node->children[0], $expression, $declarations);
    $rightchild = filter_node($node->children[2], $expression, $declarations);
    $res['errors'] = array_merge($res['errors'], $leftchild['errors'], $rightchild['errors']);
    $curnode = getinstane($node->type());
    if ($leftchild !== null) {
        array_push($curnode->children, $leftchild['root']);
    }
    if ($rightchild !== null) {
        array_push($curnode->children, $rightchild['root']);
    }
    $curnode->go_up_children();
    $curnode->treeinstring = $node->children[1]->value();
    foreach ($curnode->children as $value) {
        if ($value !== null) {
            $curnode->treeinstring .= " " . $value->treeinstring;
        }
    }
}

function filter_unary($node, $expression, $declarations, &$res, &$curnode) {
    $nodetype = $node->type();
    $indexoperator = 0;
    $indexoperation = 1;
    if (strpos($nodetype, 'expr_postfix_') === 0) {
        $indexoperator = 1;
        $indexoperation = 0;
    }
    $curnode = getinstane($nodetype);
    $tmp = filter_node($node->children[$indexoperation], $expression, $declarations);
    $curnode->children[0] = $tmp['root'];
    $res['errors'] = array_merge($res['errors'], $tmp['errors']);
    $curnode->treeinstring = $node->children[$indexoperator]->value() . " ";
    $curnode->treeinstring .= ($curnode->children[0] !== null) ? $curnode->children[0]->treeinstring : " ";
}

function filter_binary($node, $expression, $declarations, &$res, &$curnode) {
    $leftchild = filter_node($node->children[0], $expression, $declarations);
    $rightchild = filter_node($node->children[2], $expression, $declarations);
    $res['errors'] = array_merge($res['errors'], $leftchild['errors'], $rightchild['errors']);
    $curnode = getinstane($node->type());
    $curnode->children[0] = $leftchild['root'];
    $curnode->children[1] = $rightchild['root'];
    $curnode->treeinstring = $node->children[1]->value()." ";
    $curnode->treeinstring .= ($leftchild['root'] !== null) ? $leftchild['root']->treeinstring : " "." ";
    $curnode->treeinstring .= ($rightchild['root'] !== null) ? $rightchild['root']->treeinstring : " ";
}

/**
 * Функция для создания дерева от лексического дерева
 */
function filter_node($node, $expression, $declarations=null) {
    $res = array('root' => null, 'errors' => array());
    $curnode = null;             // ... указатель на текущий узел.
    $nodetype = $node->type();   // ... тип узла.

    if ($nodetype === 'identifier' || $nodetype === 'numeric') {  // ... операнд?
        filter_operand($node, $expression, $declarations, $res, $curnode);
    } else if ($nodetype === 'expr_function_call') {               // ... pow функция?
        filter_function_call($node, $expression, $declarations, $res, $curnode);
    } else if ($nodetype === 'expr_property_access') {             // ... операции . и -> ???
        filter_property_access($node, $expression, $declarations, $res, $curnode);
    } else if ($nodetype === 'expr_brackets') {                    // ... скобки??
        return filter_node($node->children[1], $expression, $declarations);
    } else if (in_array($nodetype, array('expr_logical_or', 'expr_logical_and',
        'expr_plus', 'expr_multiply', 'expr_list'))) {
        // ... k-dim узел.
        filter_k_dim($node, $expression, $declarations, $res, $curnode);
    } else if (in_array($nodetype, array('expr_logical_not', 'expr_unary_minus',
        'expr_dereference', 'expr_take_adress',
        'expr_postfix_increment', 'expr_postfix_decrement',
        'expr_prefix_increment', 'expr_prefix_decrement'))) {
        // ... one-dim узел.
        filter_unary($node, $expression, $declarations, $res, $curnode);
    } else if (in_array($nodetype, array('expr_assign', 'expr_minus', 'expr_modulosign', 'expr_division',
        'expr_notequal', 'expr_equal', 'expr_array_access', 'expr_rightshift',
        'expr_leftshift', 'expr_lesser_or_equal', 'expr_greater_or_equal',
        'expr_lesser', 'expr_greater', 'expr_plus_assign', 'expr_minus_assign',
        'expr_multiply_assign', 'expr_division_assign', 'expr_leftshift_assign',
        'expr_rightshift_assign'))) {
        // ... двойчный узел.
        filter_binary($node, $expression, $declarations, $res, $curnode);
    } else {
        // ... ошибка ...
        array_push($res['errors'],
            new \qtype_cppexpression\errors\accepting_error($node,
                get_string($node->type(), 'qtype_cppexpression'), $expression, 'compexpr'));
    }
    // Set parent for children.
    if ($curnode != null && empty($res['errors'])) {
        if (!empty($curnode->children)) {
            foreach ($curnode->children as &$child) {
                $child->parent = $curnode;
            }
        }
    }

    $res['root'] = $curnode;
    return $res;
}

function parse_code($code, $stripcomments=true) {
    if (empty($code)) {
        return null;
    }
    // ... создать лексическое дерево.
    $lang = new block_formal_langs_language_cpp_parseable_language();
    $lang->parser()->set_strip_comments($stripcomments);
    return $lang->create_from_string($code);
}

function build_tree($expression, $declarations=null) {

    if (empty($expression)) {
        return null;
    }

    $result = parse_code($expression, true);
    $res = array();
    $syntaxtree = $result->syntaxtree;
    if ($syntaxtree == null) {
        $res['root'] = null;
        $res['errors'] = array(new \qtype_cppexpression\errors\syntax_error(null,
            'unknown type of expression', $expression, 'compexpr'));
    } else if (count($syntaxtree) > 1 || $syntaxtree[0]->type() === 'operator_token') {
        $res['root'] = null;
        $res['errors'] = array(new \qtype_cppexpression\errors\syntax_error($syntaxtree[0],
               'unknown type of expression', $expression, 'compexpr'));
    } else {
        // ... создать дерево свое.
        $res = filter_node($syntaxtree[0], $expression, $declarations);
        // ... root of tree will be sequencing operator
        if ($res['root'] != null &&
                get_class($res['root']) != 'qtype_cppexpression\core\kdimnodes\sequencing_operator') {
            $tmp = new \qtype_cppexpression\core\kdimnodes\sequencing_operator();
            $tmp->children[] = $res['root'];
            $res['root']->parent = $tmp;
            $res['root'] = $tmp;
        }
    }
    return $res;

}

function normalize(&$tree) {
    if ($tree !== null) {
        $tree->ptonewchild = null;
        $tree->convert($tree);
        while ($tree->ptonewchild !== null) {
            $tree = $tree->ptonewchild;
            $tree->ptonewchild = null;
            $tree->convert($tree);
        }
    }
}

function accept_declaration($declarations) {
    // echo 'error: move to class declaration' . PHP_EOL;
    $errors = array();
    $tmp = parse_code($declarations, true);
    if (count($tmp->syntaxtree) == 1 &&
        $tmp->syntaxtree[0]->type() === 'program' ) {

        foreach ($tmp->syntaxtree[0]->children as $child) {
            if ($child->type() !== 'variable_declaration') {
                // Add error.
                array_push($errors,
                    new \qtype_cppexpression\errors\accepting_error($child, "Allow only variable's declaration", $declarations, 'compexpr'));
            }
        }
    } else {
        array_push($errors,
            new \qtype_cppexpression\errors\accepting_error(null, "not c/c++ declaration", $declarations, 'compexpr'));
    }
    return $errors;
}