<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \class BaseNode
 *
 * \brief Базовый класс для дерева
 *
 */
namespace qtype_cppexpression\core;

abstract class base_node {
    /** @var base_node|null указатель на новый сын если текущий сын заменился */
    public $ptonewchild = null;
    /** @var base_node|null дерево в вид строки для сортировки */
    public $treeinstring = null;

    /** @var base_node|null указатель на родители */
    public $parent = null;

    /** @var base_node[] список сыновей */
    public $children = array();

    /** @var  int identificator of node */
    private $id = -1;

    /**
     * Функция преобразования узла
     * \param [in] parent - указатель на родитель
     */
    abstract public function convert($parent);

    /**
     * Функция преобразования каждого сына текущего узла
     */
    public function convert_each_children() {

        foreach ($this->children as &$child) {
            // ... convert each children.
            $child->ptonewchild = null;
            $child->convert($this);
            $child->calculate_tree_in_string();
            while ($child->ptonewchild !== null) {
                $tmp = $child->ptonewchild;
                $child = clone $child->ptonewchild;
                $child->parent = $this;
                $tmp->delete_children();
                $child->ptonewchild = null;
                $child->convert($this);
                $child->calculate_tree_in_string();
            }
        }
    }

    /**
     * Функция вычисления дерево в вид строки
     */
    public function calculate_tree_in_string() {
        $this->treeinstring = $this->get_label().' ';

        foreach ($this->children as $value) {
            $value->calculate_tree_in_string();
            $this->treeinstring .= $value->treeinstring.' ';
        }
    }

    /**
     * Функция удаления всех сыновей текущего узла
     */
    abstract public function delete_children();

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    abstract public function get_label();

    /**
     * getter for id
     * @return int id of node
     */
    public function id() {
        return $this->id;
    }

    /**
     * Assign identificator for all nodes in tree
     * @param int $id next free identificator
     * @return int last assigned identificator
     */
    public function set_all_number($id=0) {
        $this->id = $id;
        foreach ($this->children as $child) {
            ++$id;
            $id = $child->set_all_number($id);
        }
        return $id;
    }

    /**
     * Get root of tree, i.e. global sequencing operator
     * @return null|base_node root of current tree
     */
    public function get_root() {
        $res = $this;
        while ($res->parent != null) {
            $res = $res->parent;
        }
        return $res;
    }

}