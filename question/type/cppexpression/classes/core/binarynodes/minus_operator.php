<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:49
 *
 * \class minus_operator
 *
 * \brief Класс для операции вычитания
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;
use qtype_cppexpression\core\kdimnodes\plus_operator;
use qtype_cppexpression\core\onedimnodes\unary_minus_operator;

class minus_operator extends binary_node {

    public function convert($parent) {
        // ... преобразовать каждый сын.
        $this->convert_each_children();
        // ... преобразовать в вид сложения.
        $tmp = new plus_operator();
        array_push($tmp->children, $this->children[0]);
        $t = new unary_minus_operator();
        $t->children[0] = $this->children[1];
        $t->calculate_tree_in_string();
        array_push($tmp->children, $t);
        $tmp->calculate_tree_in_string();
        $this->ptonewchild = $tmp;
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '-';
    }
}
