<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:53
 *
 * \class shift_right_operator
 *
 * \brief Класс для операции побитового сдвига право
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;

class shift_right_operator extends binary_node {

    public function convert($parent) {
        // ... преобразовать каждый сын.
        $this->convert_each_children();
        // ... если целая константа то преобразовать в деление.
        if (get_class($this->children[1]) == 'qtype_cppexpression\core\operand' &&
            $this->children[1]->value !== null) {
            $tmp = new div_operator();
            $tmp->children[0] = $this->children[0];
            $this->children[1]->value = pow(2, $this->children[1]->value);
            $this->children[1]->name = strval($this->children[1]->value);
            $this->children[1]->typeofvar = 'int';
            $this->children[1]->treeinstring = $this->children[1]->name;
            $tmp->children[1] = $this->children[1];
            $tmp->calculate_tree_in_string();
            $this->ptonewchild = $tmp;
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '>>';
    }
}
