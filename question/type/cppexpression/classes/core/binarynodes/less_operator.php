<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:56
 *
 * \class less_operator
 *
 * \brief Класс для операции сравнения меньше
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;

class less_operator extends binary_node {

    public function convert($parent) {
        $this->convert_each_children();

        // ... преобразовать в вид >.
        $gt = new greater_operator();

        $gt->children[1] = $this->children[0];
        $gt->children[0] = $this->children[1];
        $gt->calculate_tree_in_string();
        $this->ptonewchild = $gt;
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '<';
    }
}
