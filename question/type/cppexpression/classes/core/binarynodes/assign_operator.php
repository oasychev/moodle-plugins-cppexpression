<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:46
 *
 * \class assign_operator
 *
 * \brief Класс для операции присваивания
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;

class assign_operator extends binary_node {

    public function convert($parent) {
        $this->convert_each_children();
        // if expression's type a = b = c to convert to sequencing
        if (get_class($this->children[1]) === 'qtype_cppexpression\core\binarynodes\assign_operator') {
            $this->ptonewchild = new \qtype_cppexpression\core\kdimnodes\sequencing_operator();
            $this->ptonewchild->children[0] = $this->children[1];
            $this->ptonewchild->children[1] = $this;
            $this->children[1] = $this->children[1]->children[0];
        } else if (get_class($this->children[1]) === 'qtype_cppexpression\core\kdimnodes\sequencing_operator') {
            $this->ptonewchild = $this->children[1];
            $tmplast = $this->children[1]->children[count($this->children[1]->children)-1];
            $this->children[1] = get_class($tmplast) === 'qtype_cppexpression\core\binarynodes\assign_operator' ?
                $tmplast->children[0] : $tmplast;
            array_push($this->ptonewchild->children, $this);
        }
    }

    /**
     * Определить марка для узел
     */
    public function get_label() {
        return '=';
    }
}