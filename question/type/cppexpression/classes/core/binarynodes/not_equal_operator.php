<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:50
 *
 * \class not_equal_operator
 *
 * \brief Класс для операции сравнения неравенства
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;
use qtype_cppexpression\core\onedimnodes\not_logic_operator;

class not_equal_operator extends binary_node {

    public function convert($parent) {
        $this->convert_each_children();
        // ... сортировать правый и левый.
        if ($this->children[0]->treeinstring > $this->children[1]->treeinstring) {
            $tmp = $this->children[0];
            $this->children[0] = $this->children[1];
            $this->children[1] = $tmp;
        }
        // ... преобразовать в вид не равно.
        $tmp = new not_logic_operator();
        $t = new equal_operator();
        $t->children[1] = $this->children[1];
        $t->children[0] = $this->children[0];
        $t->calculate_tree_in_string();
        $tmp->children[0] = $t;
        $tmp->calculate_tree_in_string();
        $this->ptonewchild = $tmp;
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '!=';
    }
}
