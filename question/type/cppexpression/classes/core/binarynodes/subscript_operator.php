<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:53
 *
 * \class subscript_operator
 *
 * \brief Класс для операции обращения к элементу массива
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;
use qtype_cppexpression\core\kdimnodes\plus_operator;
use qtype_cppexpression\core\onedimnodes\dereference_operator;

class subscript_operator extends binary_node {

    public function convert($parent) {
        // ... преобразовать каждый сын.
        $this->convert_each_children();
        // ... преобразовать в вид через указатель.
        $tmp = new dereference_operator();
        $t = new plus_operator();
        array_push($t->children, $this->children[1]);
        array_push($t->children, $this->children[0]);
        $t->calculate_tree_in_string();
        $tmp->children[0] = $t;
        $tmp->calculate_tree_in_string();
        $this->ptonewchild = $tmp;
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '[]';
    }
}
