<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 9:47
 *
 * \class div_operator
 *
 * \brief Класс для операции деления
 *
 */
namespace qtype_cppexpression\core\binarynodes;
use qtype_cppexpression\core\binary_node;
use qtype_cppexpression\core\kdimnodes\multi_operator;
use qtype_cppexpression\core\operand;

class div_operator extends binary_node {

    public function convert($parent) {
        // ... преобразовать каждый сын
        // ... если делить на дробь: a / (b/c) => a*c/b.
        while (get_class($this->children[1]) == 'qtype_cppexpression\core\binarynodes\div_operator') {
            $tmp = new multi_operator();
            $tmp->children[] = $this->children[0];
            $tmp->children[] = $this->children[1]->children[1];
            $tmp->calculate_tree_in_string();
            $this->children[0] = $tmp;
            $this->children[1] = $this->children[1]->children[0];
        }
        // ... Преобразовать каждый сын.
        $this->convert_each_children();

        // ... Вычисление констант.
        if  (is_a($this->children[0], 'qtype_cppexpression\core\operand') &&
        is_a($this->children[1], 'qtype_cppexpression\core\operand') &&
        $this->children[0]->value !== null && $this->children[1]->value !== null &&
        gettype($this->children[0]) != 'string' && gettype($this->children[1]) != 'string'){
            $tmp = new operand();
            if ($this->children[0]->typeofvar === 'int' && $this->children[1]->typeofvar === 'int') {
                $tmp->value = floor($this->children[0]->value / $this->children[1]->value);
                $tmp->typeofvar = 'int';
            } else {
                $tmp->value = $this->children[0]->value / $this->children[1]->value;
                $tmp->typeofvar = 'float';
            }
            $this->ptonewchild = $tmp;
            return;
        }

        // ... Преобразовать в вид a * (1/b).
        if (!(get_class($this->children[0]) == 'qtype_cppexpression\core\operand' &&
                $this->children[0]->value == 1)) {

            $tmp = new multi_operator();
            $tmp->children[] = $this->children[0];

            $t2 = new div_operator();
            // ... константа 1.
            $t2->children[] = new operand("1", 1, 1, 'int');
            $t2->children[] = $this->children[1];

            $t2->calculate_tree_in_string();

            $tmp->children[] = $t2;
            $tmp->calculate_tree_in_string();
            $this->ptonewchild = $tmp;
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '/';
    }
}
