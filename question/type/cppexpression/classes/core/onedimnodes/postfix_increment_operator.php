<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 *
 * class prefix_increment_operator
 *
 * \brief Класс для операции постфикс-инкремента
 */

namespace qtype_cppexpression\core\onedimnodes;


use qtype_cppexpression\core\one_dim_node;


class postfix_increment_operator extends one_dim_node {

    /**
     * Функция преобразования узла
     * \param [in] parent - указатель на родитель
     */
    public function convert($parent) {
        // TODO: Implement convert() method.
        // y=x++ tobe converted to y = x, x=x+1
        $this->convert_each_children();
        $gseq = $this->get_root();
        // Create child = child + 1
        $right = new \qtype_cppexpression\core\kdimnodes\plus_operator();
        $right->children[] = clone $this->children[0];
        $right->children[] = new \qtype_cppexpression\core\operand('1', 1, 1, 'int');
        $assign = new \qtype_cppexpression\core\binarynodes\assign_operator();
        $assign->children[] = $right->children[0];
        $assign->children[] = $right;
        // ... assign parent for each child
        foreach ($assign->children as &$child) {
            $child->parent = $assign;
        }

        // Check only postfix
        if ($this->parent == $gseq) {
            $this->ptonewchild = $assign;
        } else {
            $gseq->children[] = $assign;
            $this->ptonewchild = $this->children[0];
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '++';
    }
}