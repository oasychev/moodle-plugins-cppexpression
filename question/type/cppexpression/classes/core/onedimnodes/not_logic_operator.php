<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:09
 *
 * \class not_logic_operator
 *
 * \brief Класс для операции логического отрицания НЕ
 *
 */
namespace qtype_cppexpression\core\onedimnodes;
use qtype_cppexpression\core\one_dim_node;

class not_logic_operator extends one_dim_node {

    public function convert($parent) {
        $this->convert_each_children();

        if (get_class($this->children[0]) == 'qtype_cppexpression\core\onedimnodes\not_logic_operator') {
            $this->ptonewchild = $this->children[0]->children[0];
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '!';
    }
}
