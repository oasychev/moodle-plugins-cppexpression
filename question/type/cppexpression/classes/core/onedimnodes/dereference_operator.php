<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:10
 *
 * \class dereference_operator
 *
 * \brief Класс для операции непрямого обращения (через указатель)
 *
 */
namespace qtype_cppexpression\core\onedimnodes;
use qtype_cppexpression\core\one_dim_node;

class dereference_operator extends one_dim_node {

    public function convert($parent) {
        $this->convert_each_children();
        // ... проверка сына.
        if (get_class($this->children[0]) == 'qtype_cppexpression\core\onedimnodes\reference_operator') {
            $this->ptonewchild = $this->children[0]->children[0];
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '*';
    }
}
