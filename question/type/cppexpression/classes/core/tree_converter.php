<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \class tree converter для создание и филтования дерево
 *
 * \brief Базовый класс для дерева
 * User: godric
 * Date: 22.04.17
 * Time: 10:43
 */

namespace qtype_cppexpression\core;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->dirroot .'/blocks/formal_langs/language_cpp_parseable_language.php');
use block_formal_langs_language_cpp_parseable_language;
use \qtype_cppexpression\errors\accepting_error;
use \qtype_cppexpression\errors\prohibit_use_error;

class tree_converter {

    /** @var  string expression in string */
    private $expression;
    /** @var  \block_formal_langs_ast_node_base tree created by formal_langs */
    private $fltree;
    /** @var  base_node tree for cppexpression plugin */
    private $tree;
    /** @var array list of error when building tree */
    private $errors;
    /** @var  int prohibit operators flags */
    private $prohibits;

    public function __construct($expr, $prohibits=0, $declarations=null) {
        $this->expression = $expr;
        $this->prohibits = $prohibits;
        $this->fltree = $this->tree = null;
        $this->errors = array();
        $this->build_tree($declarations);
    }

    public function getTree() {
        return $this->tree;
    }

    public function emptyErrors() {
        return empty($this->errors);
    }

    public function getErrors() {
        return $this->errors;
    }

    public function build_tree($declarations=null) {

        if ($this->expression === null || strlen($this->expression) == 0) {
            return null;
        }

        $result = $this->parse_code($this->expression, true);
        $syntaxtree = $result->syntaxtree;
        if ($syntaxtree == null) {
            $this->tree = null;
            $this->errors[] = new \qtype_cppexpression\errors\syntax_error(null,
                get_string('unknown_expr', 'qtype_cppexpression'), $this->expression, 'compexpr');
        } else if (count($syntaxtree) > 1 || $syntaxtree[0]->type() === 'operator_token') {
            $this->tree = null;
            $this->errors[] = new \qtype_cppexpression\errors\syntax_error($syntaxtree[0],
                get_string('unknown_expr', 'qtype_cppexpression'), $this->expression, 'compexpr');
        } else {
            // ... создать дерево свое.
            $this->tree = $this->filter_node($syntaxtree[0], $declarations);
            // ... root of tree will be sequencing operator
            if ($this->tree != null &&
                !is_a($this->tree,self::$class_names['expr_list'][0])){

                $tmp = $this->getinstance('expr_list');
                $tmp->children[] = $this->tree;
                $this->tree->parent = $tmp;
                $this->tree = $tmp;
            }
        }
    }

    public function normalize() {
        if ($this->tree !== null) {
            $this->tree->ptonewchild = null;
            $this->tree->convert($this->tree);
            while ($this->tree->ptonewchild !== null) {
                $this->tree = $this->tree->ptonewchild;
                $this->tree->ptonewchild = null;
                $this->tree->convert($this->tree);
            }
        }
    }

    /**
     * Функция для создания экземплю узла по именю класса
     */
    protected function getinstance($classname) {
        if (array_key_exists($classname, tree_converter::$class_names)) {
            return new tree_converter::$class_names[$classname][0];
        } else {
            return null;
        }
    }

    private function filter_operand($node, $declarations) {
        $nodetype = $node->type();   // ... тип узла.
        $curnode = null;
        if ($declarations !== null && $nodetype === 'identifier') {
            $curnode = $declarations->get_operand($node->value());
            if ($curnode == null) {
                $this->errors[] =
                    new \qtype_cppexpression\errors\missing_declaration($node, $this->expression, $node->value());
            }
        } else {
            // If declaration not exist, create operand.
            $curnode = $this->getinstance('identifier');
            $curnode->name = $node->value();
            $curnode->treeinstring = $curnode->name;
            if ($nodetype === 'numeric') {
                $tmp = $curnode->name->string();
                if ($tmp === (string) intval($tmp)) {
                    $curnode->typeofvar = 'int';
                    $curnode->value = intval($tmp);
                } else {
                    $curnode->typeofvar = 'double';
                    $curnode->value = doubleval($tmp);
                }
            } else if ($nodetype === 'string') {
                $curnode->typeofvar = 'string';
                $curnode->value = $node->value()->string();
            }
        }
        return $curnode;
    }

    private function filter_function_call($node, $declarations) {
        // Function has arguments?
        if (count($node->children) > 3) {
            $args = $this->filter_node($node->children[2], $declarations);
        }

        $curnode = $this->getinstance($node->type());
        $curnode->name = $node->children[0]->value();
        $curnode->children[] = $args;

        $curnode->treeinstring = $curnode->name . '(';
        $curnode->treeinstring .= ($args !== null) ? $args->treeinstring : " " . ') ';

        return $curnode;
    }

    private function filter_property_access($node, $declarations) {
        $leftchild = $this->filter_node($node->children[0]->children[0], $declarations);
        $rightchild = $this->filter_node($node->children[1], $declarations);

        $curnode = $this->getinstance($node->children[0]->type());
        $curnode->children[0] = $leftchild;
        $curnode->children[1] = $rightchild;
        $curnode->treeinstring = $node->children[0]->children[1]->value()." ";
        $curnode->treeinstring .= ($leftchild !== null) ? $leftchild->treeinstring : " "." ";
        $curnode->treeinstring .= ($rightchild !== null) ? $rightchild->treeinstring : " ";
        return $curnode;
    }

    private function filter_k_dim($node, $declarations) {
        $leftchild = $this->filter_node($node->children[0], $declarations);
        $rightchild = $this->filter_node($node->children[2], $declarations);

        $curnode = $this->getinstance($node->type());
        if ($leftchild !== null) {
            $curnode->children[] = $leftchild;
        }
        if ($rightchild !== null) {
            $curnode->children[] = $rightchild;
        }
        $curnode->go_up_children();
        $curnode->treeinstring = $node->children[1]->value();
        foreach ($curnode->children as $value) {
            if ($value !== null) {
                $curnode->treeinstring .= " " . $value->treeinstring;
            }
        }
        return $curnode;
    }

    private function filter_unary($node, $declarations) {
        $nodetype = $node->type();
        $indexoperator = 0;
        $indexoperation = 1;
        if (strpos($nodetype, 'expr_postfix_') === 0) {
            $indexoperator = 1;
            $indexoperation = 0;
        }
        $curnode = $this->getinstance($nodetype);
        $tmp = $this->filter_node($node->children[$indexoperation], $declarations);
        $curnode->children[0] = $tmp;

        $curnode->treeinstring = $node->children[$indexoperator]->value() . " ";
        $curnode->treeinstring .= ($curnode->children[0] !== null) ? $curnode->children[0]->treeinstring : " ";
        return $curnode;
    }

    private function filter_binary($node, $declarations) {
        $leftchild = $this->filter_node($node->children[0], $declarations);
        $rightchild = $this->filter_node($node->children[2], $declarations);
        $curnode = $this->getinstance($node->type());
        $curnode->children[0] = $leftchild;
        $curnode->children[1] = $rightchild;
        $curnode->treeinstring = $node->children[1]->value()." ";
        $curnode->treeinstring .= ($leftchild !== null) ? $leftchild->treeinstring : " "." ";
        $curnode->treeinstring .= ($rightchild !== null) ? $rightchild->treeinstring : " ";
        return $curnode;
    }

    /**
     * Функция для создания дерева от лексического дерева
     */
    private function filter_node($node, $declarations=null) {
        $curnode = null;             // ... указатель на текущий узел.
        $nodetype = $node->type();   // ... тип узла.

        if ($nodetype === 'identifier' || $nodetype === 'numeric' || $nodetype === 'string') {  // ... операнд?
            $curnode = $this->filter_operand($node, $declarations);
        } else if ($nodetype === 'expr_function_call') {               // ... pow функция?
            $curnode = $this->filter_function_call($node, $declarations, $curnode);
        } else if ($nodetype === 'expr_property_access') {             // ... операции . и -> ???
            $curnode = $this->filter_property_access($node, $declarations);
        } else if ($nodetype === 'expr_brackets') {                    // ... скобки??
            $curnode = $this->filter_node($node->children[1], $declarations);
        } else if (in_array($nodetype, array('expr_logical_or', 'expr_logical_and',
            'expr_plus', 'expr_multiply', 'expr_list'))) {
            // ... k-dim узел.
            $curnode = $this->filter_k_dim($node, $declarations);
        } else if (in_array($nodetype, array('expr_logical_not', 'expr_unary_minus',
            'expr_dereference', 'expr_take_adress',
            'expr_postfix_increment', 'expr_postfix_decrement',
            'expr_prefix_increment', 'expr_prefix_decrement'))) {
            // ... one-dim узел.
            $curnode = $this->filter_unary($node, $declarations);
        } else if (in_array($nodetype, array('expr_assign', 'expr_minus', 'expr_modulosign', 'expr_division',
            'expr_notequal', 'expr_equal', 'expr_array_access', 'expr_rightshift',
            'expr_leftshift', 'expr_lesser_or_equal', 'expr_greater_or_equal',
            'expr_lesser', 'expr_greater', 'expr_plus_assign', 'expr_minus_assign',
            'expr_multiply_assign', 'expr_division_assign', 'expr_leftshift_assign',
            'expr_rightshift_assign'))) {
            // ... двойчный узел.
            $curnode = $this->filter_binary($node, $declarations);
        } else {
            // ... ошибка ...
            $this->errors[] =
                new accepting_error($node,
                    get_string($node->type(), 'qtype_cppexpression'), $this->expression, 'compexpr');
        }
        if ($nodetype !== 'identifier' && array_key_exists($nodetype, tree_converter::$class_names)) {

            if (($this->prohibits >> tree_converter::$class_names[$nodetype][1]) & 1  == 1) {
            $this->errors[] = new prohibit_use_error($node, $this->expression);
        }
        }
        // Set parent for children.
        if ($curnode != null && empty($this->errors)) {
            if (!empty($curnode->children)) {
                foreach ($curnode->children as &$child) {
                    $child->parent = $curnode;
                }
            }
        }

        return $curnode;
    }

    private function parse_code($code, $stripcomments=true) {
        if ($code === null || strlen($code) == 0) {
            return null;
        }
        // ... создать лексическое дерево.
        $lang = new block_formal_langs_language_cpp_parseable_language();
        $lang->parser()->set_strip_comments($stripcomments);
        return $lang->create_from_string($code);
    }

    /**
     * Печать дерево в формат DOT
     *
     * \param [in] file указатель на выходный файл
     * \param [in] curnode указатель на текущий узел
     *
     */
    static function print_tree_to_dot($file, $curnode) {
        static $globalid = 0; // ... следующий идентификатор для узлов.
        $id = $globalid; // ... идентификатор для данный узла.
        // ... инкремент общий индетификатор.
        ++$globalid;
        // ... печать определение для данного узла?
        if  ($curnode == null) return;
        if (is_a($curnode, 'qtype_cppexpression\core\operand')) {
            fwrite($file, $id.' [label = "'.$curnode->name . "\"]\n");
        } else {
            fwrite($file, $id . ' [label = "' . $curnode->get_label() . "\"]\n");
            foreach ($curnode->children as $value) {
                $next = $globalid;
                tree_converter::print_tree_to_dot($file, $value);
                fwrite($file, $id . ' -> ' . $next . "\n");
            }
        }
    }

    public static function tree2dot($filename, $tree) {
        $file = fopen($filename, "w");
        fwrite($file, "digraph {\n");
        tree_converter::print_tree_to_dot($file, $tree);
        fwrite($file, '}');
        fclose($file);
    }

    /**
     * \brief Функция сравнения двух деревьев
     *
     * \param [in] tree1 указатель на узел первого дерева
     * \param [in] tree2 указатель на узел второго дерева
     * \param [in] isprintdiff флаг: печать ли разницы двух деревьев
     * \return true если два дерева равны, в противном случае false
     *
     */
    public static function is_tree_equal($tree1, $tree2, $isprintdiff = false) {

        if (get_class($tree1) != get_class($tree2)) {
            // ... печать разницы при тестировать?
            if ($isprintdiff) {
                echo "____ type of node diff\n";
                echo "__________ result = ".get_class($tree1)."\n";
                echo "__________ expected = ".get_class($tree2)."\n";
            }
            return false;
        }
        if (is_a($tree1, 'qtype_cppexpression\core\operand')) { // ... операнд?
            if ($tree1->name != $tree2->name) {
                // ... печать разницы при тестировать?
                if ($isprintdiff) {
                    echo "____ name of operand diff\n";
                    echo "__________ result = ".$tree1->name."\n";
                    echo "__________ expected = ".$tree2->name."\n";
                }
                return false;
            }
            if ($tree1->value != $tree2->value) {
                // ... печать разницы при тестировать?
                if ($isprintdiff) {
                    echo "____ number of operand diff\n";
                    echo "__________ result = ".$tree1->value."\n";
                    echo "__________ expected = ".$tree2->value."\n";
                }
                return false;
            }
            if ($tree1->typeofvar!= $tree2->typeofvar) {
                // ... печать разницы при тестировать?
                if ($isprintdiff) {
                    echo "____ number of operand diff\n";
                    echo "__________ result = ".$tree1->typeofvar."\n";
                    echo "__________ expected = ".$tree2->typeofvar."\n";
                }
                return false;
            }
        } else if ($tree1 != null && $tree2 != null) { // if (is_subclass_of($tree1, 'qtype_cppexpression\core\k_dim_node')) { // ... k-dim.
            $res = count($tree1->children) == count($tree2->children);
            $i = 0;
            while ($res && $i < count($tree1->children)) {
                $res = $res && tree_converter::is_tree_equal($tree1->children[$i], $tree2->children[$i], $isprintdiff);
                $i ++;
            }
            return $res;
        }

        return true;
    }

    static public $class_names = array(
        'identifier'=> array('qtype_cppexpression\core\operand', -1),
        'expr_logical_or'=> array('qtype_cppexpression\core\kdimnodes\or_logic_operator', 0),
        'expr_logical_and'=> array('qtype_cppexpression\core\kdimnodes\and_logic_operator', 1),
        'expr_plus'=> array('qtype_cppexpression\core\kdimnodes\plus_operator', 2),
        'expr_multiply'=> array('qtype_cppexpression\core\kdimnodes\multi_operator', 3),
        'expr_list'=> array('qtype_cppexpression\core\kdimnodes\sequencing_operator', 4),
        'expr_function_call'=> array('qtype_cppexpression\core\kdimnodes\function_call', 5),

        'expr_logical_not'=> array('qtype_cppexpression\core\onedimnodes\not_logic_operator', 6),
        'expr_unary_minus'=> array('qtype_cppexpression\core\onedimnodes\unary_minus_operator', 7),
        'expr_dereference'=> array('qtype_cppexpression\core\onedimnodes\dereference_operator', 8),
        'expr_take_adress'=> array('qtype_cppexpression\core\onedimnodes\reference_operator', 9),

        'expr_postfix_increment'=> array('qtype_cppexpression\core\onedimnodes\postfix_increment_operator', 10),
        'expr_postfix_decrement'=> array('qtype_cppexpression\core\onedimnodes\postfix_decrement_operator', 11),
        'expr_prefix_increment'=> array('qtype_cppexpression\core\onedimnodes\prefix_increment_operator', 12),
        'expr_prefix_decrement'=> array('qtype_cppexpression\core\onedimnodes\prefix_decrement_operator', 13),

        'expr_assign'=> array('qtype_cppexpression\core\binarynodes\assign_operator', 14),
        'expr_minus'=> array('qtype_cppexpression\core\binarynodes\minus_operator', 15),
        'expr_modulosign'=> array('qtype_cppexpression\core\binarynodes\mod_operator', 16),
        'expr_division'=> array('qtype_cppexpression\core\binarynodes\div_operator', 17),
        'expr_equal'=> array('qtype_cppexpression\core\binarynodes\equal_operator', 18),
        'expr_notequal'=> array('qtype_cppexpression\core\binarynodes\not_equal_operator', 19),
        'try_value_access'=> array('qtype_cppexpression\core\binarynodes\mem_acc_operator', 20),
        'try_pointer_access'=> array('qtype_cppexpression\core\binarynodes\pt_mem_acc_operator', 21),
        'expr_array_access'=> array('qtype_cppexpression\core\binarynodes\subscript_operator', 22),
        'expr_rightshift'=> array('qtype_cppexpression\core\binarynodes\shift_right_operator', 23),
        'expr_leftshift'=> array('qtype_cppexpression\core\binarynodes\shift_left_operator', 24),
        'expr_lesser_or_equal'=> array('qtype_cppexpression\core\binarynodes\less_equal_operator', 25),
        'expr_greater_or_equal'=> array('qtype_cppexpression\core\binarynodes\greater_equal_operator', 26),
        'expr_lesser'=> array('qtype_cppexpression\core\binarynodes\less_operator', 27),
        'expr_greater'=> array('qtype_cppexpression\core\binarynodes\greater_operator', 28),
        'expr_plus_assign'=> array('qtype_cppexpression\core\binarynodes\plus_assign_operator', 29),
        'expr_minus_assign'=> array('qtype_cppexpression\core\binarynodes\minus_assign_operator', 30),
        'expr_multiply_assign'=> array('qtype_cppexpression\core\binarynodes\multi_assign_operator', 31),
        'expr_division_assign'=> array('qtype_cppexpression\core\binarynodes\div_assign_operator', 32),
        'expr_leftshift_assign'=> array('qtype_cppexpression\core\binarynodes\shl_assign_operator', 33),
        'expr_rightshift_assign'=> array('qtype_cppexpression\core\binarynodes\shr_assign_operator', 34)
    );

}
