<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Declaration of variables from teacher
 *
 * @package     cppexpression
 * @author      godric
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * Created by PhpStorm.
 * User: godric
 * Date: 02.09.16
 * Time: 9:32
 */
namespace qtype_cppexpression\core;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->dirroot .'/blocks/formal_langs/language_cpp_parseable_language.php');

use block_formal_langs_language_cpp_parseable_language;
use qtype_cppexpression\decl_type;
use qtype_cppexpression\errors\redeclaration_error;
use qtype_cppexpression\errors\wrong_format_comment;
use qtype_cppexpression\errors\accepting_error;
use qtype_cppexpression\errors\declaration_error;
use qtype_cppexpression\errors\missing_comment;
use qtype_cppexpression\errors\syntax_error;


class declaration {
    private static $REG_CM_INT = '/from [1-9]?[\d]+ to [1-9]?[\d]+/';
    private static $REG_CM_DOUBLE = '/from [\d]*\.?[\d]+ to [\d]*\.?[\d]+ step [\d]*\.?[\d]/';
    /** @var  string code of declaration */
    private $code;
    /** @var  block_formal_langs_ast_node_base tree's root of declaration's presentation*/
    private $tree;
    /** @var  operand[] array of operands, which declarate by teacher */
    private $operands;
    /** @var  declaration_error[] array of errors in declaration, empty if declaration correct */
    private $errors;
    /** @var  string current processing declaration */
    private $currentprocdecl;
    /** @var  decl_type with/without comment */
    private $type;

    private $initvalue;
    private $fromvalue;
    private $tovalue;
    private $stepvalue;

    /**
     * declaration constructor.
     * @param $code string declaration of teacher
     */
    public function __construct($code, $type = decl_type::NO_DECLARATION) {
        $tree = null;
        $this->type = $type;
        $this->code = $code;
        $this->operands = array();
        $this->currentprocdecl = null;
        $this->errors = array();
    }

    /**
     * Печать дерево в формат DOT
     *
     * \param [in] file указатель на выходный файл
     * \param [in] curnode указатель на текущий узел
     *
     */
    private function print_tree_to_dot($file, $curnode) {
        static $globalid = 0; // ... следующий идентификатор для узлов.
        $id = $globalid; // ... идентификатор для данный узла.
        // ... инкремент общий индетификатор.
        ++$globalid;
        // ... печать определение для данного узла?
        if  ($curnode == null) return;
        if (is_a($curnode, 'qtype_cppexpression\core\operand')) {
            fwrite($file, $id.' [label = "'.$curnode->name . "\"]\n");
        } else {
            fwrite($file, $id . ' [label = "' . $curnode->get_label() . "\"]\n");
            foreach ($curnode->children as $value) {
                $next = $globalid;
                print_tree_to_dot($file, $value);
                fwrite($file, $id . ' -> ' . $next . "\n");
            }
        }
    }

    public function tree2dot($filename, $tree) {
        $file = fopen($filename, "w");
        fwrite($file, "digraph {\n");
        print_tree_to_dot($file, $tree);
        fwrite($file, '}');
        fclose($file);
    }

    /**
     * Get one operand with given name
     * @param $name name of operand, which wanna get
     * @return null|operand operand if declarated or null if not
     */
    public function get_operand($name) {
        foreach ($this->operands as $operand) {
            if ($operand->name == $name) {
                return clone $operand;
            }
        }
        return null;
    }

    /**
     * Getter for tree's root of declaration
     * @return block_formal_langs_ast_node_base root of tree
     */
    public function tree() {
        if ($this->tree === null) {
            // Parse and create tree from code.
            $res = parse_code($this->code, false);
            $this->tree = $res->syntaxtree;
        }
        return $this->tree;
    }

    /**
     * Checking is init values of variable is valid
     * @param $vardecl \block_formal_langs_ast_node_base node variable_declaration
     */
    protected function is_valid_init_values($vardecl) {
        // Have init?
        if (count($vardecl->children[1]->children) == 2) {
            $vartype = $vardecl->children[0]->value();
            $initnode = $vardecl->children[1]->children[0]->children[1];
            switch ($vartype) {
                case 'int':
                    break;
                case 'float':
                    break;
                case 'double':
                    break;
                case 'char':
                    break;
            }
        }
    }

    /**
     * @param $vardecl \block_formal_langs_ast_node_base node variable_declaration
     */
    protected function is_valid_comment($vardecl) {
        // ... have comment?
        $lastnode = $vardecl->children[count($vardecl->children)-1];
        if ($lastnode->type() == 'semicolon' &&
            count($lastnode->children) == 2 &&
            $lastnode->children[1]->children[0]->type() == 'singleline_comment') {
            // ... comment in correct format?
            $comment = trim(substr($lastnode->children[1]->children[0]->value(), 2));
            if ((strpos($vardecl->children[0]->value(), 'int') !== false && !preg_match(self::$REG_CM_INT, $comment)) ||
                ((strpos($vardecl->children[0]->value(), 'float') !== false || strpos($vardecl->children[0]->value(), 'double')) !== false
                 && !preg_match(self::$REG_CM_DOUBLE, $comment))) {
                array_push($this->errors, new wrong_format_comment($lastnode, $this->currentprocdecl));
            }
        } else {
            // ... create error missing comment.
            array_push($this->errors, new missing_comment($lastnode, $this->currentprocdecl));
        }
    }

    protected function is_redeclarate($vardecl) {
        $dup = null;
        $varname = $vardecl->children[1]->children[0]->children[0]->value();
        foreach ($this->operands as $operand) {
            if (!strcmp($varname, $operand->name)) {
                $dup = $operand;
                break;
            }
        }
        if ($dup != null) {
            array_push($this->errors, new redeclaration_error($vardecl, $this->currentprocdecl, $dup->name));
        }
    }

    /**
     * @param $definition array children of variable_declaration
     * @return array values for operand constructor
     */
    private function get_operand_init_value($definition) {

        $res = array(array(),
                     null,
                     $definition[0]->value(),
                     null, null, null);
        for ($i = 0; $i < count($definition[1]->children); $i += 2) {
            $res[0][] = $this->get_variable_name($definition[1]->children[$i]);
        }
        if ($definition[1]->children[0]->type() == 'definition') {
            $type = $definition[1]->children[0]->children[1]->children[1]->type();
            $res[1] = "";
            switch ($type) {
                case 'numeric':
                    $res[1] = $definition[1]->children[0]->children[1]->children[1]->value()->string();
                    $res[1] = strpos($res[1], '.') === false ? intval($res[1]) : floatval($res[1]);
                    break;
            }
        }

        if (count($definition[2]->children) == 2) {
            $comment = explode(" ", trim(substr($definition[2]->children[1]->children[0]->value(), 2)));
            if (count($comment) > 3) {
                $res[3] = strpos($comment[1], '.') === false ? intval($comment[1]) : floatval($comment[1]);
                $res[4] = strpos($comment[3], '.') === false ? intval($comment[3]) : floatval($comment[3]);
                if (count($comment) == 6 && $comment[4] == 'step') {
                    $res[5] = strpos($comment[5], '.') === false ? intval($comment[5]) : floatval($comment[5]);
                }
            }
        }
        return $res;
    }

    function get_variable_type($vardecl) {
        if (empty($vardecl->children) || empty($vardecl->children[0]->children)) {
            return "";
        }
        $res = "";
        foreach ($vardecl->children[0]->children as $child) {
            $res .= $child->value() . " ";
        }
        return rtrim($res);
    }

    function get_variable_name($lvalue) {
        $count = count($lvalue->children);
        if ($count == 1) {
            return $lvalue->children[0]->value();
        } else if ($count == 2) {
            return ($lvalue->children[0]->type() == 'lvalue') ?
                $this->get_variable_name($lvalue->children[0]) :
                $this->get_variable_name($lvalue->children[1]);
        }
    }

    protected function variable_declaration($decl) {
        // TODO checking accordingly type and init value.
        $this->is_valid_init_values($decl);
        // TODO checking comment.
        if ($this->type == DECL_TYPE::DECL_WITH_COMMENT) {
            $this->is_valid_comment($decl);
        }
        $this->is_redeclarate($decl);

        // Is declaraion correct???
        if (empty($this->errors)) {
            $type = $this->get_variable_type($decl);
            // ... create new operand.
            $inits = $this->get_operand_init_value($decl->children);
            foreach ($inits[0] as $name) {
                $tmp = new operand($name, null, $inits[1], $inits[2], $inits[3], $inits[4], $inits[5]);
                $tmp->typeofvar = $type;
                $this->operands[] = $tmp;

            }
        }
    }

    protected function struct_declaration($header, $body) {
        if (count($body->children) == 3) {
            foreach ($body->children[1]->children as $decl) {
                $this->process_each_declaration($decl);
            }
        }
    }

    /**
     * Checking each variable's declaration and
     * create and store if correct declaration given.
     * @param $decl block_formal_langs_ast_node_base node of declaration
     */
    protected function process_each_declaration($decl) {
        switch ($decl->type()) {
            case 'variable_declaration':
                $this->variable_declaration($decl);
                break;
            case 'struct_definition':
            case 'union_definition':
                $this->struct_declaration($decl->children[0], $decl->children[1]);
        }
    }

    /**
     * Checker for teacher's declaration
     * @return accepting_error[] array of errors. empty array if ok
     */
    public function accept_declaration() {
        $this->errors = array();

        if (!empty($this->code)) {

            // ... создать лексическое дерево.
            $lang = new block_formal_langs_language_cpp_parseable_language();
            $lang->parser()->set_strip_comments(false);
            $prog = $lang->create_from_string($this->code)->syntaxtree;

            if (count($prog) != 1 || $prog[0]->type() != 'program') {
                $this->errors[] = new syntax_error(null, get_string('decl_syntax_mes_error', 'qtype_cppexpression'), null);
                return $this->errors;
            }
            foreach ($prog[0]->children as $child) {
                $pos = $child->position();
                $declaration = substr($this->code, $pos->stringstart(), $pos->stringend() - $pos->stringstart() + 1);
                $this->currentprocdecl = trim($declaration);;
                $this->process_each_declaration($child);
            }
        }
        return $this->errors;
    }

}