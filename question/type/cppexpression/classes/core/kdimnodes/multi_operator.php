<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:14
 *
 * \class multi_operator
 *
 * \brief Класс для операции умножения
 *
 */
namespace qtype_cppexpression\core\kdimnodes;
use qtype_cppexpression\core\binarynodes\div_operator;
use qtype_cppexpression\core\k_dim_node;
use qtype_cppexpression\core\onedimnodes\unary_minus_operator;
use qtype_cppexpression\core\operand;

class multi_operator extends k_dim_node {

    public function convert($parent) {
        $ishaveplus = false;
        $isadd = false;

        // 1.	Проверка каждого его сына:
        // ... его сын – операция сложения то добавить его сыновья вверх – вызов goUpChildren.
        $this->go_up_children();
        // 2.	Вызов функции преобразования каждого его сына.
        // ... снова сыновья вверх – вызов goUpChildren.
        $this->convert_each_children();
        $this->go_up_children();
        // 3.	Вычислить константу: сложение констант в списке сыновьей.
        $value = $this->calculate_const($ishaveplus, $isadd);
        // ... сортировать сыновья.
        $this->sort_children();
        // ... преобразовать дробей.
        $this->convert_div_in_mult();
        // ... сортировать сыновья.
        $this->sort_children();
        // ... если есть операция сложения то раскрывать скобки.
        if ($ishaveplus) {
            $openedsum = $this->open_bracket();
            $this->ptonewchild = $openedsum;
            return;
        }
        // ... только 1 сын то преобразовать в вид только его?
        if (count($this->children) == 1) {
            $this->ptonewchild = $this->children[0];
            return;
        }
        // ... проверка знак. число операнд отрицательно.
        $numnegative = $this->count_negative($value);
        // ... нечетно то добавляем знак унарный минус?
        if ($numnegative % 2 == 1) {
            $tmp = new unary_minus_operator();
            $tmp->children[0] = clone $this;
            $tmp->calculate_tree_in_string();
            $this->ptonewchild = $tmp;
            return;
        }
        // ... дублировать сыны?
        if ($isadd) {
            $this->duplicate_child($value);
        }
    }

    /**
     * Вычислить константу: сложение констант в списке сыновьей.
     *
     * \param [out] isHavePlus флаг: есть ли в списке сыновей операция сложения
     * \param [out] isAdd флаг: есть ли в списке сыновей целая константа
     *
     * \return значение целой константы если есть
     */
    public function calculate_const(&$ishaveplus, &$isadd) {
        $value = 1; // ... произведение целых констант.
        $dvalue = 1; // ... произведение вещественных констант.
        $isdadd = false; // ... флаг есть ли вещественных констант.
        // ... для каждого сына.
        for ($i = 0; $i < count($this->children); $i++) {
            // ... проверка есть ли операция сложения.
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\kdimnodes\plus_operator') {
                $ishaveplus = true;
            }
            // ... вычисление констант.
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\operand') {
                if (is_int($this->children[$i]->value)) {
                    $value *= $this->children[$i]->value;
                    array_splice($this->children, $i, 1);
                    $i--;
                    $isadd = true;
                } else if (is_double($this->children[$i]->value)) {
                    $dvalue *= $this->children[$i]->value;
                    array_splice($this->children, $i, 1);
                    $i--;
                    $isdadd = true;
                }
            }
        }

        if ($isadd) {
            // ... создать новую константу.
            $tmp = new operand(strval($value), $value, $value, gettype($value)=='integer'?'int':'double');
            $tmp->calculate_tree_in_string();
            // ... добавлять в список сыновей.
            array_push($this->children, $tmp);
        }
        if ($isdadd) {
            // ... создать новую константу.
            $tmp = new operand(strval($dvalue), $dvalue, $dvalue, gettype($dvalue)=='integer'?'int':'double');
            $tmp->calculate_tree_in_string();
            // ... добавлять в список сыновей.
            array_push($this->children, $tmp);
        }
        return $value;
    }

    /**
     * Считать количесво операндов отрицательные
     *
     * \param [out] value значение целой константы после преобразования знака
     * \return
     */
    public function count_negative(&$value) {
        $numnegative = 0; // ... результат.
        for ($i = 0; $i < count($this->children); $i++) {
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\onedimnodes\unary_minus_operator') {
                ++$numnegative;
                $this->children[$i] = $this->children[$i]->children[0];
            } else if (get_class($this->children[$i]) == 'qtype_cppexpression\core\operand' &&
                $this->children[$i]->value !== null &&
                $this->children[$i]->value < 0) {
                ++$numnegative;
                $this->children[$i]->value = -$this->children[$i]->value;
                $this->children[$i]->name = strval($this->children[$i]->value);
                if (is_int($this->children[$i]->value)) {
                    $value = -$value;
                }
            }

        }
        return $numnegative;
    }

    /**
     * Дублировать сыновей
     *
     * \param [in] value количество раз для дублирования сыновей
     */
    public function duplicate_child($value) {
        $todup = array();
        for ($i = 0; $i < count($this->children); $i++) {
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\operand') {
                if ($this->children[$i]->value === null || is_double($this->children[$i]->value)) {
                    array_push($todup, $this->children[$i]);
                }
            }
            if (get_class($this->children[$i]) != 'qtype_cppexpression\core\operand') {
                array_push($todup, $this->children[$i]);
            }
        }
        // ... создать сын для добавления.
        $childtoadd = null;
        if (count($todup) == 1) {
            $childtoadd = $todup[0];
        } else {
            $childtoadd = new multi_operator();
            $childtoadd->children = $todup;
            $childtoadd->calculate_tree_in_string();
        }
        // ... преобразовать в plus_operator.
        $tmp = new plus_operator();
        for ($i = 0; $i < abs($value); $i++) {
            array_push($tmp->children, clone $childtoadd);
        }
        $tmp->calculate_tree_in_string();

        // ... добавление знак.
        if ($value < 0) {
            $t = new unary_minus_operator();
            $t->children[0] = $tmp;
            $t->calculate_tree_in_string();
            $this->ptonewchild = $t;
        } else {
            $this->ptonewchild = $tmp;
        }
    }

    /**
     * Функция преобразования дробей при умножении
     */
    public function convert_div_in_mult() {
        $divop = array(); // ... временный массив.
        // ... взять дроби.
        for ($i = 0; $i < count($this->children); $i ++) {
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\binarynodes\div_operator') {
                array_push($divop, $this->children[$i]);
                array_splice($this->children, $i, 1);
                $i --;
            }
        }
        // ... создать новый.
        if (count($divop) > 1) {
            $tmp = new multi_operator();
            foreach ($divop as $i => $value) {
                array_push($tmp->children, $value->children[1]);
            }
            $tmp->go_up_children();
            $tmp->sort_children();
            $newch = new div_operator();
            $newch->children[0] = new operand("1", 1, 1, 'int');
            $newch->children[1] = $tmp;
            $newch->calculate_tree_in_string();
            $divop = array($newch);
        }
        // ... возвращать в списку сыновей.
        foreach ($divop as $value) {
            array_push($this->children, $value);
        }
    }

    /**
     * Функция раскрытия скобок
     * \return указатель на узел сложения произведений
     */
    public function open_bracket() {
        $multelements = array(); // ... список умножителей.
        $conf = array(); // ... перестановка.
        // ... Инициализировать список умножителей..
        for ($i = 0; $i < count($this->children); $i++) {
            $conf[$i] = 0;
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\kdimnodes\plus_operator') {
                array_push($multelements, $this->children[$i]->children);
            } else {
                array_push($multelements, array($this->children[$i]));
            }
        }

        $res = new plus_operator(); // ... новая корень.
        $isstop = false; // ... флаг стопа.
        while (!$isstop) {
            // ... создать узел умножения.
            $cur = new multi_operator();
            for ($i = 0; $i < count($multelements); $i++) {
                array_push($cur->children, clone $multelements[$i][$conf[$i]]);
            }
            array_push($res->children, $cur);

            // ... Вычисление следующей перестановки.
            $prev = 1;
            for ($i = count($multelements) - 1; $i >= 0; $i--) {
                $conf[$i] = (1 + $conf[$i]) % count($multelements[$i]);
                if ($i == 0 && $conf[$i] == 0) {
                    $isstop = true;
                }
                if ($conf[$i] != 0) {
                    break;
                }
            }

        }
        return $res;
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '*';
    }
}
