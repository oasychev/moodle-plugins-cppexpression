<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:13
 *
 * \class plus_operator
 *
 * \brief Класс для операции сложения
 *
 */
namespace qtype_cppexpression\core\kdimnodes;
use qtype_cppexpression\core\k_dim_node;
use qtype_cppexpression\core\operand;

class plus_operator extends k_dim_node {

    public function convert($parent) {
        // 1.	Проверка каждого его сына:
        // ... его сын – операция сложения то добавить его сыновья вверх – вызов.
        $this->go_up_children();

        // 2.	Вызов функции преобразования каждого его сына.
        // ... Снова вызов goUpChildren.
        $this->convert_each_children();
        $this->go_up_children();

        // 3.	Вычислить константу: сложение констант в списке сыновьей.
        $value = 0; // ... новое значение.
        $isadd = false; // ... флаг что есть ли константа.
        for ($i = 0; $i < count($this->children); $i ++) {
            // ... Если сын - это операция.
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\operand') {
                // ... Если сын - это констнатна.
                if ($this->children[$i]->value !== null) {
                    // ... добавить значение.
                    $value += $this->children[$i]->value;
                    // ... удалить этот сын из списка сыновей.
                    array_splice($this->children, $i, 1);
                    $i--;
                    // ... установить флаг.
                    $isadd = true;
                }
            }
        }

        // ... Если есть константа и ее значение не равно нулю.
        if ($isadd && $value != 0) {
            // ... создать новую константу.
            $tmp = new operand(strval($value), $value, $value, gettype($value)=='integer'?'int':'double');
            $tmp->calculate_tree_in_string();
            // ... добавлять в список сыновей.
            array_push($this->children, $tmp);
        }

        // 4.	Сортировать сыновей – вызов функция sort_children.
        $this->sort_children();

        // 5.	Если в списке сыновей только один сын то узел преобразуется в вид его сын.
        if (count($this->children) == 1) {
            $this->ptonewchild = $this->children[0];
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '+';
    }
}
