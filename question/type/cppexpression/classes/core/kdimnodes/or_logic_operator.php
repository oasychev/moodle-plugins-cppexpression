<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:15
 *
 * \class or_logic_operator
 *
 * \brief Класс для операции логического сложения
 *
 */
namespace qtype_cppexpression\core\kdimnodes;
use qtype_cppexpression\core\binarynodes\greater_equal_operator;
use qtype_cppexpression\core\binarynodes\less_equal_operator;
use qtype_cppexpression\core\k_dim_node;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\core\onedimnodes\not_logic_operator;
use qtype_cppexpression\core\onedimnodes\unary_minus_operator;

class or_logic_operator extends k_dim_node {

    public function convert($parent) {
        // ... преобразуем каждый сын.
        $this->convert_each_children();
        // ... удалить одинаковые узлы.
        $vtemp = array(); // ... список новых сыновей после удаления.
        for ($i = 0; $i < count($this->children); $i++) {
            $isadd = true;
            $j = 0;
            // ... проверка текущего узла был ли раньше.
            while ($isadd && $j < count($vtemp)) {
                if (tree_converter::is_tree_equal($this->children[$i], $vtemp[$j])) {
                    $isadd = false;
                }
                ++$j;
            }
            // ... если не был то добавляем.
            if ($isadd) {
                array_push($vtemp, $this->children[$i]);
            }
        }
        // ... присваивать новый список сыновей.
        $this->children = $vtemp;
        // ... преобразуем сравнений операций в вид >=, <=.
        $this->reduce_compare();
        // ... преобразуем каждый сын.
        $this->convert_each_children();
        // ... сортируем сыновья.
        $this->sort_children();
        // ... преобразуем в МДНФ.
        $this->convert_quine_mc_cluskey();
    }

    /**
     * Проверка можно ли два операнды операции сравнения одинаковые
     *
     * \param [in] one операнд первой операции сравнения
     * \param [in] two операнд второй операции сравнения
     *
     * \return true если они одинаковые и можно преобразовать в вид >= или <=, в противном случае false
     */
    public function is_childs_same($one, $two) {
        // ... проверка левого сына a и b.
        if (tree_converter::is_tree_equal($one->children[0], $two->children[0])) {
            return true;
        }
        // ... проверка левого сына а с обратном знаком.
        $tmp = new unary_minus_operator();
        // ... создать унарный минус.
        $tmp->children[0] = clone $one->children[0];
        // ... преобразовать новый узел.
        $tmp->ptonewchild = null;
        $tmp->convert($tmp);
        while ($tmp->ptonewchild !== null) {
            $tmp = $tmp->ptonewchild;
            $tmp->ptonewchild = null;
            $tmp->convert($tmp);
        }
        // ... сравнение сына a с обратном знаком.
        return tree_converter::is_tree_equal($tmp, $two->children[0]);
    }

    /**
     * Преобразовать операций сравнений > (<) и == в вид >= (<=)
     */
    public function reduce_compare() {
        $vtemp = array(); // ... временной вектор сыновей.
        // ... нахождение сравнения можно сокращать.
        for ($i = 0; $i < count($this->children); $i++) {
            if ($this->children[$i] !== null) {
                $isadd = true; // .... флаг : добавить текущий узел в временной вектор.
                $lnode = null; // ... указатель на узел сравнение.
                $enode = null; // ... указатель на узел равенства.
                for ($j = $i + 1; $j < count($this->children) && $isadd; $j++) {
                    $iscomp = false; // ... флаг: нашлось ли узлы для сокращения.
                    if (get_class($this->children[$i]) == 'qtype_cppexpression\core\binarynodes\greater_operator' &&
                        get_class($this->children[$j]) == 'qtype_cppexpression\core\binarynodes\equal_operator') {
                        $iscomp = true;
                        $lnode = $this->children[$i];
                        $enode = $this->children[$j];
                    } else if (get_class($this->children[$i]) == 'qtype_cppexpression\core\binarynodes\equal_operator' &&
                        get_class($this->children[$j]) == 'qtype_cppexpression\core\binarynodes\greater_operator') {
                        $iscomp = true;
                        $lnode = $this->children[$j];
                        $enode = $this->children[$i];
                    } else if (get_class($this->children[$i]) == 'qtype_cppexpression\core\binarynodes\less_operator' &&
                        get_class($this->children[$j]) == 'qtype_cppexpression\core\binarynodes\equal_operator') {
                        $iscomp = true;
                        $lnode = $this->children[$i];
                        $enode = $this->children[$j];
                    } else if (get_class($this->children[$i]) == 'qtype_cppexpression\core\binarynodes\equal_operator' &&
                        get_class($this->children[$j]) == 'qtype_cppexpression\core\binarynodes\less_operator') {
                        $iscomp = true;
                        $lnode = $this->children[$j];
                        $enode = $this->children[$i];
                    }
                    // ... проверка можно ли сокращать.
                    if ($iscomp && $this->is_childs_same($lnode, $enode)) {
                        // ... создать новый узел.
                        if (get_class($lnode) == 'qtype_cppexpression\core\binarynodes\less_operator') {
                            $tmp = new less_equal_operator();
                        } else {
                            $tmp = new greater_equal_operator();
                        }
                        // ... присваивать значения.
                        $tmp->children[0] = $lnode->children[0];
                        $tmp->children[1] = $lnode->children[1];
                        $isadd = false;
                        $tmp->calculate_tree_in_string();
                        // ... добавить в временной вектор сыновей.
                        array_push($vtemp, $tmp);
                        // ... удалить из вектора сыновей.
                        $this->children[$i] = $this->children[$j] = null;
                    }

                }
                if ($isadd) {
                    array_push($vtemp, $this->children[$i]);
                }
            }
        }
        // ... присваивать новые сыновья.
        $this->children = $vtemp;
    }

    /**
     * Проверка возможно скеивания и нахождение разной позиции в записях сыновей
     *
     * \param [in] one первая двоичная запись сына
     * \param [in] two вторая двоичная запись сына
     *
     * \return неотрицательное число - разная позиция для склеивания при возможно, в противном случае -1
     */
    public function is_changeable($one, $two) {
        $diff = 0; // ... общее число разных позициях.
        $diff01 = 0; // ... число разных позициях 0 и 1.
        $res = -1; // ... результат.
        // ... проверка каждой позиции записях.
        for ($i = 0; $i < strlen($one); $i++) {
            if ($one[$i] != $two[$i]) {
                $diff ++;
                if ($one[$i] != '-' && $two[$i] != '-') {
                    ++$diff01;
                }
                $res = $i;
            }
        }
        // ... если 2 записи разные только в 1 позиции.
        // ... и в этой позиции 0 и 1.
        if ($diff == 1 && $diff01 == 1) {
            return $res;
        }
        // ... невозможно склеивать.
        return -1;
    }

    /**
     * Считать значение в таблице покрытий
     *
     * \param [in] imp простая импликанта
     * \param [in] old импликанта
     *
     * \return значение в таблице покрытий
     */
    public function is_cover($imp, $old) {
        // ... проверка каждой записи.
        for ($i = 0; $i < strlen($imp); $i++) {
            if ($imp[$i] != '-' && $imp[$i] != $old[$i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Проверка узел был ли в векторе
     * \param [in] node укзатель узла, нужен проверять
     * \param [in] vec вектор узлов
     *
     * \return true если в векторе появился узел, в противном случае false
     */
    public function is_have_node($node, $vec) {
        foreach ($vec as $value) {
            if (tree_converter::is_tree_equal($node, $value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Найти позицию сына в полном виде функции
     * \param [in] node узказатель на сын
     * \param [in] vec вектор полного вида функции
     *
     * \return позицию сына в полном виде функции
     */
    public function pos_in_full_exp($node, $vec) {
        for ($i = 0; $i < count($vec); $i ++) {
            if (tree_converter::is_tree_equal($node, $vec[$i])) {
                return $i;
            }
        }
        return -1;
    }

    /**
     * Создать полный вид функции
     *
     * \param [out] fullExp полный вид функции
     */
    public function make_full_exp(&$fullexp) {
        // ... каждый сын.
        for ($i = 0; $i < count($this->children); $i++) {
            $nodetype = get_class($this->children[$i]);
            // ... он операция ! ???
            if ($nodetype == 'qtype_cppexpression\core\onedimnodes\not_logic_operator') {
                if (!$this->is_have_node($this->children[$i]->children[0], $fullexp)) {
                    array_push($fullexp, $this->children[$i]->children[0]);
                }
            } else if ($nodetype == 'qtype_cppexpression\core\kdimnodes\and_logic_operator') { // ... он операция && ???
                // ... каждый сын операции && .
                for ($j = 0; $j < count($this->children[$i]->children); $j++) {
                    $nodetype = get_class($this->children[$i]->children[$j]);
                    if ($nodetype == 'qtype_cppexpression\core\onedimnodes\not_logic_operator') { // ... если операция !.
                        if (! $this->is_have_node($this->children[$i]->children[$j]->children[0], $fullexp)) {
                            array_push($fullexp, $this->children[$i]->children[$j]->children[0]);
                        }
                    } else {
                        if (!$this->is_have_node($this->children[$i]->children[$j], $fullexp)) {
                            array_push($fullexp, $this->children[$i]->children[$j]);
                        }
                    }
                }
            } else {
                if (!$this->is_have_node($this->children[$i], $fullexp)) {
                    array_push($fullexp, $this->children[$i]);
                }
            }
        }
    }

    /**
     * Создать двоичный вид для каждого сына
     *
     * \param [in] fullExp полный вид функции
     * \param [out] eachChild двоичные записи каждого сына
     */
    public function make_each_children_notation($fullexp, &$eachchild) {
        // ... создать двоичный вид для каждого сына.
        for ($i = 0; $i < count($this->children); $i++) {
            // ... инициализация все -----.
            $eachchild[$i] = "";
            for ($j = 0; $j < count($fullexp); $j++) {
                $eachchild[$i] .= '-';
            }
            $nodetype = get_class($this->children[$i]);
            // ... операция ! ??
            if ($nodetype == 'qtype_cppexpression\core\onedimnodes\not_logic_operator') {
                $eachchild[$i][$this->pos_in_full_exp($this->children[$i]->children[0], $fullexp)] = '0';
            } else if ($nodetype == 'qtype_cppexpression\core\kdimnodes\and_logic_operator') { // ... операция && ??
                for ($k = 0; $k < count($this->children[$i]->children); $k++) {
                    $nodetype = get_class($this->children[$i]->children[$k]);
                    if ($nodetype == 'qtype_cppexpression\core\onedimnodes\not_logic_operator') { // ... он операция ! ??
                        $eachchild[$i][$this->pos_in_full_exp($this->children[$i]->children[$k]->children[0], $fullexp)] = '0';
                    } else {
                        $eachchild[$i][$this->pos_in_full_exp($this->children[$i]->children[$k], $fullexp)] = '1';
                    }
                }
            } else { // ... просто операнд.
                $eachchild[$i][$this->pos_in_full_exp($this->children[$i], $fullexp)] = '1';
            }
        }
    }

    /**
     * Создать простые импликанты из двоичных записях
     *
     * \param [in,out] eachChild двоичные записи преобразуется в простые импликанты
     */
    public function make_implicate(&$eachchild) {
        // ... создать простые импликанты.
        $isstop = false; // ... флаг стопа.
        while (!$isstop) {
            $tmp = array(); // ... временной вектор.
            $isstop = true;
            $ischanged = array_fill(0, count($eachchild), false); // ... флаг склеивали записях.
            for ($i = 0; $i < count($eachchild); $i++) {
                for ($j = $i + 1; $j < count($eachchild); $j++) {
                    // ... взять разную позицию в записях.
                    $pos = $this->is_changeable($eachchild[$i], $eachchild[$j]);
                    if ($pos != -1) {
                        // ... склеивать.
                        $s = $eachchild[$i];
                        $s[$pos] = '-';
                        $isadded = false;
                        for ($k = 0; $k < count($tmp); $k++) {
                            if ($tmp[$k] == $s) {
                                $isadded = true;
                            }
                        }
                        if (!$isadded) {
                            array_push($tmp, $s);
                        }
                        $ischanged[$i] = true;
                        $ischanged[$j] = true;
                        $isstop = false;
                    }
                }
            }
            // ... добавлять записи не склеивали.
            for ($i = 0; $i < count($eachchild); $i++) {
                if (!$ischanged[$i]) {
                    array_push($tmp, $eachchild[$i]);
                }
            }
            $eachchild = $tmp;
        }

        // ... удалить лишный.
        $isdel = false;
        $i = 0;
        while ($i < count($eachchild) && !$isdel) {
            if ($this->find_first_not_of($eachchild[$i], '-') == -1) {
                $isdel = true;
            }
            ++$i;
        }
        if ($isdel) {
            $eachchild = array();
        }
    }

    /**
     * Нахождение совокупности простых импликант, соответствующих минимальной ДНФ
     * \param [in] eachChild простые импликанты
     * \param [in] impl начальные импликанты
     * \param [in] coverage таблица покрытий
     *
     * \return число, биты которого описывается МДНФ
     */
    public function find_min_cover($eachchild, $impl, $coverage) {
        // ... нахождение совокупности простых импликант, соответствующих минимальной ДНФ.
        $mincf = (1 << count($eachchild)) - 1; // .... минимальный совокупность, отмечать в битах.
        $minsize = 1000000; // ... текущий размер.
        $cur = null; // ... минимальный размер.
        $columcover = null; // ... текущий совокупность, отмечать в битах.

        for ($cf = 1; $cf < (1 << count($eachchild)); $cf++) {
            $cur = 0;
            $columcover = 0;

            for ($i = 0; $i < count($eachchild); $i++) {
                // ... i-й бит.
                if (($cf >> $i) & 1 == 1) {
                    ++$cur;
                    if ($cur > $minsize) {
                        break;
                    }
                    // ... если покрывал то включить j-й бит.
                    for ($j = 0; $j < count($impl); $j++) {
                        if ($coverage[$i][$j]) {
                            $columcover = $columcover | (1 << $j);
                        }
                    }
                }
            }

            // ... если текущая совокупность покрывал все.
            if ($columcover == (1 << count($impl)) - 1 && $cur < $minsize) {
                $minsize = $cur;
                $mincf = $cf;
            }
        }

        return $mincf;
    }

    /**
     * Функция нахождения первой позиции не символа в строке
     * \param [in] str строка, в которой нужно найти
     * \param [in] ch символ
     * \param [pos] начальная позиция нахождения
     * \return позиция при удалось найти, инача -1
     */
    public function find_first_not_of($str, $ch, $pos = 0) {
        for ($i = $pos; $i < strlen($str); $i ++) {
            if ($str[$i] != $ch) {
                return $i;
            }
        }
        return -1;
    }

    /**
     * Создать новый дерево МДНФ
     *
     * \param [in] eachChild простые импликанты
     * \param [in] mincf число, биты которого описывается МДНФ
     * \param [in] fullExp полный вид функции
     */
    public function make_min_tree($eachchild, $mincf, $fullexp) {
        // ... удалить текущие сыновья.
        $this->children = array();
        for ($i = 0; $i < count($eachchild); $i++) {
            // ... Если i-й бит включенно.
            if (($mincf >> $i) & 1 == 1) {
                $isone = -1 == $this->find_first_not_of($eachchild[$i], '-', $this->find_first_not_of($eachchild[$i], '-') + 1);
                // ... только 1 операнд?
                if ($isone) {
                    $pos = $this->find_first_not_of($eachchild[$i], '-');
                    if ($pos != -1) {
                        if ($eachchild[$i][$pos] == '1') {
                            array_push($this->children, clone $fullexp[$pos]);
                        } else {
                            $tmp = new not_logic_operator();
                            $tmp->children[0] = clone $fullexp[$pos];
                            array_push($this->children, $tmp);
                            $tmp->calculate_tree_in_string();
                        }
                    }
                } else { // ... операция &&.
                    $tmp = new and_logic_operator();
                    for ($j = 0; $j < strlen($eachchild[$i]); $j++) {
                        if ($eachchild[$i][$j] == '1') {
                            array_push($tmp->children, clone $fullexp[$j]);
                        } else if ($eachchild[$i][$j] == '0') {
                            $notop = new not_logic_operator();
                            $notop->children[0] = clone $fullexp[$j];
                            $notop->calculate_tree_in_string();
                            array_push($tmp->children, $notop);
                        }
                    }
                    $tmp->sort_children();
                    $tmp->calculate_tree_in_string();
                    array_push($this->children, $tmp);
                }
            }
        }
    }

    /**
     * Преобразовать методом Квайна - Мак-Класки
     */
    public function convert_quine_mc_cluskey() {
        $fullexp = array(); // ... полный вид функции.
        $eachchild = array(); // ... двоичная запись каждого сына.

        // ... создать полный вид функции.
        $this->make_full_exp($fullexp);

        // ... создать двоичный вид для каждого сына.
        $this->make_each_children_notation($fullexp, $eachchild);

        $impl = $eachchild; // ... вектор импликант.
        // ... создать простые импликанты, сохраняются в векторе eachChild.
        $this->make_implicate($eachchild);

        // ... создать таблицу покрытий.
        $coverage = array();
        for ($i = 0; $i < count($eachchild); $i++) {
            $coverage[$i] = array();
            for ($j = 0; $j < count($impl); $j++) {
                $coverage[$i][$j] = $this->is_cover($eachchild[$i], $impl[$j]);
            }
        }

        // ... нахождение совокупности простых, импликантсоответствующих минимальной ДНФ.
        $mincf = $this->find_min_cover($eachchild, $impl, $coverage);

        // ... создать новый дерево МДНФ.
        $this->make_min_tree($eachchild, $mincf, $fullexp);
        // ... только 1 сын то преобразовать в вид только сын?
        if (count($this->children) == 1) {
            $this->ptonewchild = $this->children[0];
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '||';
    }
}