<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 *
 * class sequencing_operator
 *
 * \brief Класс для операции следования
 *
 */

namespace qtype_cppexpression\core\kdimnodes;

use qtype_cppexpression\core\k_dim_node;

class sequencing_operator extends k_dim_node {

    /**
     * Функция преобразования узла
     * \param [in] parent - указатель на родитель
     */
    public function convert($parent) {
        // TODO: Implement convert() method.
        $this->convert_each_children();
        $this->go_up_children();
        $this->convert_each_children();
        if (!is_a($parent, 'qtype_cppexpression\core\kdimnodes\function_call')) {
            $this->sort_children();
            //$this->calculate_tree_in_string();
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return ',';
    }

    /**
     * Сортировка сыновьей с учетом порядка выполнения
     * Модификация алгоритм сортировка пузырьком
     */
    public function sort_children() {
        $n = count($this->children);
        for ($i = 1; $i < $n; ++$i) {
            $j = $i;
            $k = $j-1;
            // Сдвиг текущий элемент до места где его порядок выполнения зависит.
            while ($k >= 0 && $this->is_independent($k, $j)){
                if ($this->children[$k]->treeinstring > $this->children[$j]->treeinstring) {
                    for ($t=$j; $k < $t; --$t) {
                        $tmp = $this->children[$t];
                        $this->children[$t] = $this->children[$t-1];
                        $this->children[$t-1] = $tmp;
                    }
                    $j = $k;
                }
                --$k;
            }
        }
    }

    /**
     * Проверка зависимости порядка выполнения между двух сыновьей
     * \param [in] p - индекс сына на лево
     * \param [in] p - индекс сына на право
     * @return boolean истина если порядок выпонения двух сыновьей независимы
     */
    private function is_independent($p, $i) {
        $isp = is_a($this->children[$p], 'qtype_cppexpression\core\binarynodes\assign_operator');
        $isi = is_a($this->children[$i], 'qtype_cppexpression\core\binarynodes\assign_operator');
        if (!($isi || $isp)) {
            return true;
        }
        $childrenp = $this->get_operands($this->children[$p]);
        $childreni = $this->get_operands($this->children[$i]);
        return !(($isi && $this->check_in_array($this->children[$i]->children[0], $childrenp)) ||
                ($isp && $this->check_in_array($this->children[$p]->children[0], $childreni)));
    }

    /**
     * Проверка наличии операнда в массива
     * \param [in] node - операнд
     * \param [in] arr - массив операндов
     * @return boolean истина если массив имеет операнд
     */
    private function check_in_array($node, $arr) {
        foreach ($arr as $e) {
            if ($node->name == $e->name)
                return true;
        }
        return false;
    }

    /**
     * Нахождение всех операндов поддерева
     * \param [in] node - корень поддерева
     * @return array список операндов поддерева
     */
    private function get_operands($node) {
        $res = array();
        $heap = array($node);
        $l = 0;
        while ($l < count($heap)) {
            if (is_a($heap[$l], 'qtype_cppexpression\core\operand')) {
                $res[] = $heap[$l];
            } else {
                foreach ($heap[$l]->children as $child) {
                    $heap[] = $child;
                }
            }
            ++$l;
        }
        return $res;
    }
}