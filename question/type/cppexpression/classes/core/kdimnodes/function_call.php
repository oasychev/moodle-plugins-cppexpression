<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 15.03.17
 * Time: 19:58
 *
 * \class pow_function
 *
 * \brief Класс для вызова функции
 *
 */
namespace qtype_cppexpression\core\kdimnodes;

use qtype_cppexpression\core\k_dim_node;


class function_call extends k_dim_node {


    /** @var null|string name of function */
    public $name = null;

    /**
     * function_call constructor.
     * @param null|string $name
     */
    public function __construct($name="") {
        $this->name = $name;
    }


    /**
     * Функция преобразования узла
     * \param [in] parent - указатель на родитель
     */
    public function convert($parent) {
        // ... преобразуем каждый сын.
        $this->convert_each_children();

        if (get_class($this->children[0]) == 'qtype_cppexpression\core\kdimnodes\sequencing_operator') {
            foreach ($this->children[0]->children as $child) {
                $this->children[] = $child;
            }
            array_shift($this->children);
        }

        if ($this->name == 'pow') {
            if (count($this->children) == 2 && is_int($this->children[1]->value)) {
                $newnode = new multi_operator();
                // ... присваивать значения нового узла.
                for ($i = 0; $i < $this->children[1]->value; $i++) {
                    array_push($newnode->children, clone $this->children[0]);
                }
                $newnode->calculate_tree_in_string();
                $this->ptonewchild = $newnode;
            }
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return $this->name;
    }
}