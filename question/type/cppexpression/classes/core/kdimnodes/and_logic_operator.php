<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 13.08.16
 * Time: 10:14
 *
 * \class and_logic_operator
 *
 * \brief Класс для операции логического умножения
 *
 */
namespace qtype_cppexpression\core\kdimnodes;
use qtype_cppexpression\core\k_dim_node;
use qtype_cppexpression\core\tree_converter;
use qtype_cppexpression\core\onedimnodes\not_logic_operator;

class and_logic_operator extends k_dim_node {

    public function convert($parent) {
        // ... преобразуем каждый сын.
        $this->convert_each_children();
        // ... При использовании логической операции И одинаковых операндов.
        // ... преобразуется в вид только операнд.
        $isallnot = true;
        for ($i = 0; $i < count($this->children); $i++) {
            if (get_class($this->children[$i]) != 'qtype_cppexpression\core\onedimnodes\not_logic_operator') {
                $isallnot = false;
            }
            for ($j = $i + 1; $j < count($this->children); $j++) {
                // ... если одинаковые то удалить один узел.
                if (tree_converter::is_tree_equal($this->children[$i], $this->children[$j])) {
                    array_splice($this->children, $i, 1);
                    $j--;
                }
            }
        }

        // ... если отстаться только 1 узел то преобразуем в виде только сын.
        if (count($this->children) == 1) {
            $this->ptonewchild = $this->children[0];
            return;
        }

        // ... если все операнды - операция ! то преобразовать в вид  операции || .
        if ($isallnot) {
            $newchild = new or_logic_operator();
            for ($i = 0; $i < count($this->children); $i++) {
                array_push($newchild->children, clone $this->children[$i]->children[0]);
            }
            $newchild->calculate_tree_in_string();

            $tmp = new not_logic_operator();
            $tmp->children[0] = $newchild;
            $tmp->calculate_tree_in_string();
            $this->ptonewchild = $tmp;
            return;
        }
        // ... сортировать сыновья.
        $this->sort_children();
        // ... нормализовать в КНФ.
        $this->to_dnf();
    }

    /**
     * Преобразовать в КНД
     */
    public function to_dnf() {
        $hasOR = false;
        $orElements = array(); // ... список умножителей.
        $conf = array(); // ... перестановка.
        // ... Инициализировать список умножителей..
        for ($i = 0; $i < count($this->children); $i++) {
            $conf[$i] = 0;
            if (get_class($this->children[$i]) == 'qtype_cppexpression\core\kdimnodes\or_logic_operator') {
                $orElements[] = $this->children[$i]->children;
                $hasOR = true;
            } else {
                $orElements[] = array($this->children[$i]);
            }
        }
        // ... преобразовать если есть операция ИЛИ
        if ($hasOR) {
            $res = new or_logic_operator(); // ... новая корень.
            $isstop = false; // ... флаг стопа.
            while (!$isstop) {
                // ... создать узел умножения.
                $cur = new and_logic_operator();
                for ($i = 0; $i < count($orElements); $i++) {
                    $cur->children[] = clone $orElements[$i][$conf[$i]];
                }
                $res->children[] = $cur;

                // ... Вычисление следующей перестановки.
                $prev = 1;
                for ($i = count($orElements) - 1; $i >= 0; $i--) {
                    $conf[$i] = (1 + $conf[$i]) % count($orElements[$i]);
                    if ($i == 0 && $conf[$i] == 0) {
                        $isstop = true;
                    }
                    if ($conf[$i] != 0) {
                        break;
                    }
                }

            }
            $this->ptonewchild = $res;
        }
    }

    /**
     * Определить марка для узля когда печать дерево
     * @return string марка узла
     */
    public function get_label() {
        return '&&';
    }
}
