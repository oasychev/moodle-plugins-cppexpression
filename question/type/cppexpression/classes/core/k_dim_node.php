<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \class qtype_cppexpression_k_dim_node
 *
 * \brief Базовый класс для операции сложения, умножения, логического сложения, логического умножения
 *
 */
namespace qtype_cppexpression\core;

abstract class k_dim_node extends base_node {

    /**
     * Функция сортировки сыновей
     */
    public function sort_children() {

        for ($i = 0; $i < count($this->children); $i++) {
            for ($j = $i + 1; $j < count($this->children); $j++) {
                if ($this->children[$i]->treeinstring > $this->children[$j]->treeinstring) {
                    $tmp = $this->children[$i];
                    $this->children[$i] = $this->children[$j];
                    $this->children[$j] = $tmp;
                }
            }
        }

    }

    /**
     * Функция перенести сын вверх
     */
    public function go_up_children() {
        $tmp = $this->children;
        $this->children = array();
        for ($i = 0; $i < count($tmp); $i++) {
            if (get_class($this) == get_class($tmp[$i])) {
                for ($j = 0; $j < count($tmp[$i]->children); ++$j) {
                    $this->children[] = $tmp[$i]->children[$j];
                }
            } else {
                $this->children[] = $tmp[$i];
            }
        }
    }

//    public function convert_each_children() {
//
//        foreach ($this->children as &$child) {
//            // ... convert each children.
//            $child->ptonewchild = null;
//            $child->convert($this);
//            while ($child->ptonewchild !== null) {
//                $tmp = $child->ptonewchild;
//                $child = clone $child->ptonewchild;
//                $child->parent = $this;
//                $tmp->delete_children();
//                $child->ptonewchild = null;
////                echo '------------------ </br>';
////                tree_converter::tree2dot('/tmp/tmp.dot', $child);
////                echo file_get_contents('/tmp/tmp.dot') . '</br>';
//                $child->convert($this);
//            }
////            echo '------------------ </br>';
////            tree_converter::tree2dot('/tmp/tmp.dot', $child);
////            echo file_get_contents('/tmp/tmp.dot') . '</br>';
//        }
//    }

    public function delete_children() {
        for ($i = 0; $i < count($this->children); $i ++) {
            $this->children[$i]->delete_children();
        }
        unset ($this->children);
        $this->children = array();
    }

    public function __clone() {
        for ($i = 0; $i < count($this->children); $i++) {
            $this->children[$i] = clone $this->children[$i];
        }
    }
}
