<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Class for compare and find differences of two expression's trees
 *
 * @package    cppexpression
 * @author     Phan Tuan Anh
 * @copyright  &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression\core;

class comparator {
    /** @var  base_node root of answer (of teacher) */
    private $answer;
    /** @var  base_node root of response (of student) */
    private $response;
    /** @var double fitness value after compare */
    private $fitness = null;

    /** @var null hash map of <id, base_node> */
    private $hashanswer = null;

    /** @var hashmap<node1*, node2*, int, array<pair<node1*,node2*>> maximum common subtree's size*/
    private $commons = array();

    /** @var int  */
    private $answersize = 0;
    /** @var int  */
    private $responsesize = 0;
    /** @var int  */
    private $commontreesize = 0;
    /** @var  base_node root of common tree */
    private $commonroot = null;

    /**
     * comparator constructor.
     * @param $answer base_node root of answer tree
     * @param $response base_node root of response tree
     * @return comparator
     */
    public function __construct($answer, $response) {
        $this->answer = $answer;
        $this->response = $response;
        $this->fill_hashanswer($this->answer);
        if ($response === null) {
            $this->fitness = 0;
        } else {
            $this->answer->set_all_number(0);
            $this->response->set_all_number(0);
        }
    }

    /**
     * fill hash map <id, base_node*> and calculate size of answer tree
     * @param $node base_node
     */
    private function fill_hashanswer($node) {
        $this->answersize ++;
        $this->hashanswer[$node->id()] = $node;
        foreach ($node->children as $child) {
            $this->fill_hashanswer($child);
            $child->parent = $node;
        }
    }

    /**
     * find position of maximum common value
     * @param $pos int identificator of node's response
     * @return int result is position
     */
    private function find_pos_max_in_commons($id) {
        $pos = -1;
        $value = -1;
        foreach ($this->commons[$id] as $cur => $each) {
            if ($each[0] >= $value) {
                $value = $each[0];
                $pos = $cur;
            }
        }
        return $pos;
    }

    public function get_fitness() {
        if ($this->fitness === null) {
            $this->compare($this->response, $this->answer);
            // Minus common sequencing operator in root.
            $this->commontreesize -= 1;
            $this->answersize -= 1;
            $this->responsesize -= 1;
            $this->fitness = round($this->commontreesize*($this->answersize + $this->responsesize)/(2*$this->answersize*$this->responsesize), 1);
        }
        return $this->fitness;
    }

    /**
     * Match pair children of two tree to maximum common tree
     * Using LCS algorithm
     * @param $children1 base_node[] children of tree1's current node
     * @param $children2 base_node[] children of tree2's current node
     * $param $pairs result array of pair after matching
     * @return int maximum value
     */
    public function match_children_lcs($children1, $children2, &$pairs) {
        // Calculate dynamic table.
        $f = array();
        $id1 = array();
        $id2 = array();
        for ($i = 0; $i < count($children1); $i++) {
            $f[$i][0] = 0;
            $id1[$i] = $children1[$i]->id();
        }
        $f[$i][0] = 0;
        for ($i = 0; $i < count($children2); $i++) {
            $f[0][$i] = 0;
            $id2[$i] = $children2[$i]->id();
        }
        $f[0][$i] = 0;

        for ($i = 1; $i <= count($children1); $i++) {
            for ($j = 1; $j <= count($children2); $j++) {
                $f[$i][$j] = max($f[$i - 1][$j], $f[$i][$j - 1]);
                if ($this->is_nodes_equals($children1[$i - 1], $children2[$j - 1]) &&
                    $f[$i][$j] < $f[$i - 1][$j - 1] + $this->commons[$id1[$i - 1]][$id2[$j - 1]][0]) {
                        $f[$i][$j] = $f[$i - 1][$j - 1] + $this->commons[$id1[$i - 1]][$id2[$j - 1]][0];
                }
            }
        }
        // Traceback.
        $i = count($children1);
        $j = count($children2);
        while ($i > 0 && $j > 0 && $f[$i][$j] != 0) {
            if ($this->is_nodes_equals($children1[$i - 1], $children2[$j - 1]) &&
                max($f[$i - 1][$j],
                    $f[$i][$j - 1]) < $f[$i - 1][$j - 1] + $this->commons[$id1[$i - 1]][$id2[$j - 1]][0]) {

                    $pairs[$id1[$i - 1]] = $id2[$j - 1];
                    $i --;
                    $j --;

            } else {
                if ($f[$i - 1][$j] > $f[$i][$j - 1]) {
                    $i --;
                } else {
                    $j --;
                }
            }
        }
        $pairs = array_reverse($pairs, true);
        return $f[count($children1)][count($children2)];
    }

    /**
     * Compare separate nodes (not resursive)
     * @param $node1 base_node node of tree
     * @param $node2 base_node node of other tree
     * @return bool true if two nodes equals
     */
    private function is_nodes_equals($node1, $node2) {
        $res = get_class($node1) == get_class($node2);
        if (is_a($node1, 'qtype_cppexpression\core\operand') &&
            is_a($node2, 'qtype_cppexpression\core\operand') ) {
            $res = $node1->name == $node2->name ||
                (!$node1->value != null && $node2->value != null &&
                    $node1->value == $node2->value && $node1->typeofvar == $node2->typeofvar);
        }
        return $res;
    }

    /**
     * Find all thee same type nodes in tree
     * @param $node base_node primitive node
     * @param $tree base_node root of tree, where need to find
     * @return array list of alike primitive node
     */
    private function find_same_node_in_tree($node, $tree) {
        $result = array();
        // Find in children of current node.
        foreach ($tree->children as $child) {
            $result = array_merge($result, $this->find_same_node_in_tree($node, $child));
        }
        if ((is_a($node, 'qtype_cppexpression\core\operand') &&
                is_a($tree, 'qtype_cppexpression\core\operand')) ||
            (!is_a($node, 'qtype_cppexpression\core\operand') &&
                !is_a($tree, 'qtype_cppexpression\core\operand')) ) {
            array_push($result, $tree);
        }
        return $result;
    }

    /**
     * Compare two trees
     * @param $tree1 base_node response of teacher
     * @param $tree2 base_node answer of student
     */
    public function compare($tree1, $tree2) {
        $this->responsesize ++;
        // Connect all operands of two tree.
        if (is_a($tree1, 'qtype_cppexpression\core\operand')) {
            $operands = $this->find_same_node_in_tree($tree1, $tree2);
            $this->commons[$tree1->id()] = array();
            foreach ($operands as $other) {
                if ($this->is_nodes_equals($tree1, $other)) {
                    $this->commons[$tree1->id()][$other->id()][0] = 1;
                    $this->commons[$tree1->id()][$other->id()][1] = array();
                    if ($this->commons[$tree1->id()][$other->id()][0] >= $this->commontreesize) {
                        $this->commonroot = $tree1;
                        $this->commontreesize = $this->commons[$tree1->id()][$other->id()][0];
                    }

                }
            }
        } else {
            // Recurs with children.
            foreach ($tree1->children as $child) {
                $child->parent = $tree1;
                $this->compare($child, $tree2);
            }
            // Matching children.
            $variants = $this->find_same_node_in_tree($tree1, $tree2);
            foreach ($variants as $variant) {
                $pairs = array();
                $this->commons[$tree1->id()][$variant->id()][0] = $this->match_children_lcs($tree1->children,
                    $variant->children, $pairs);
                if ($this->is_nodes_equals($tree1, $variant)) {
                    $this->commons[$tree1->id()][$variant->id()][0] ++;
                }
                $this->commons[$tree1->id()][$variant->id()][1] = $pairs;
                if ($this->commons[$tree1->id()][$variant->id()][0] >= $this->commontreesize) {
                    $this->commonroot = $tree1;
                    $this->commontreesize = $this->commons[$tree1->id()][$variant->id()][0];
                }
            }
        }

    }

    private function match_left_children() {
    }

    /**
     * @param $node base_node
     * @return string content of dot file
     */
    private function print_max_node($node, $with) {
        $id = $node->id();
        $label = '';
        if ($with != -1 && $this->is_nodes_equals($node, $this->hashanswer[$with])) {
            $label = is_a($node,
                'qtype_cppexpression\core\operand') ? $node->treeinstring : $node->get_label(get_class($node));
        }
        $res = "$id [label=\"$label\"]" . PHP_EOL;
        foreach ($node->children as $child) {
            $childid = $child->id();
            if (is_a($child, 'qtype_cppexpression\core\operand')) {
                if ($with != -1 && key_exists($childid, $this->commons[$id][$with][1])) {
                    $res .= "$childid [label=\"$child->treeinstring\"]\n";
                } else {
                    $res .= "$childid [label=\"\"]\n";
                }
            } else {
                $res .= $this->print_max_node($child,
                    isset($this->commons[$id][$with][1][$childid]) ? $this->commons[$id][$with][1][$childid] : -1);
            }
            $res .= "$id -> $childid" . PHP_EOL;
        }
        return $res;
    }

    /**
     * print max common tree to file in dot format
     * @param $filename
     */
    public function print_max_common_tree_to_dot($filename) {
        $file = fopen($filename, "w");
        fwrite($file, "digraph {\n");

        if (is_a($this->commonroot, 'qtype_cppexpression\core\operand')) {
            if (!empty($this->commons[$this->commonroot->id()])) {
                fwrite($file, $this->print_max_node($this->commonroot, key($this->commons[$this->commonroot->id()])));
            }
        } else if ($this->commonroot !== null) {
            fwrite($file, $this->print_max_node($this->commonroot,
                $this->find_pos_max_in_commons($this->response->id())));
        }

        fwrite($file, '}');
        fclose($file);
    }

    /**
     * Get all pair<node1, node2> where node1 is match with node2 in common tree
     * @return array pair<node1, node2>
     */
    public function get_matching_pairs() {
        $res = array();
        $nodes = $this->commonroot == null ? null : array( array($this->commonroot,
            $this->find_pos_max_in_commons($this->response->id())));
        while (!empty($nodes)) {
            $cur = array_pop($nodes);
            if ($cur[1] != -1 && $this->is_nodes_equals($cur[0], $this->hashanswer[$cur[1]])) {
                array_push($res, array($cur[0], $this->hashanswer[$cur[1]]));
            }
            foreach ($cur[0]->children as $child) {
                if (is_a($child, 'qtype_cppexpression\core\operand') &&
                    $cur[1] != -1 && key_exists($child->id(), $this->commons[$cur[0]->id()][$cur[1]][1])) {
                        array_push($res,
                            array($child, $this->hashanswer[$this->commons[$cur[0]->id()][$cur[1]][1][$child->id()]]));
                } else if (isset($this->commons[$cur[0]->id()][$cur[1]][1][$child->id()])) {
                    array_push($nodes, array($child, $this->commons[$cur[0]->id()][$cur[1]][1][$child->id()]));
                }
            }
        }
        return $res;
    }
}
