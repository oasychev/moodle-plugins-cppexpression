<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
// require_once('base_node.php');.
namespace qtype_cppexpression\core;

abstract class binary_node extends base_node {

    public function delete_children() {
        $this->children[0]->delete_children();
        $this->children[1]->delete_children();
        unset ($this->children[0]);
        unset ($this->children[1]);
        $this->children[0] = null;
        $this->children[1] = null;
    }

    public function __clone() {
        $this->children[0] = clone $this->children[0];
        $this->children[1] = clone $this->children[1];
    }
}