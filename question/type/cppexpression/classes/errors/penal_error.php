<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class error with penal grande
 *
 * @package     cppexpression
 * @author      godric
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * Date: 05.10.16
 * Time: 8:47
 */
namespace qtype_cppexpression\errors;

class penal_error extends error{
    /** @var  int penal score if student respond incorrectly */
    private $penalvalue;

    public function __construct($node, $message, $expression, $penalvalue) {
        parent::__construct($node, $message, $expression);
        $this->set_penalvalue($penalvalue);
    }

    public function get_penalvalue() {
        return $this->penalvalue;
    }

    public function set_penalvalue($value) {
        $this->penalvalue = $value;
    }
}