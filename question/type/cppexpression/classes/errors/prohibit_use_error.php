<?php
/**
 * Created by PhpStorm.
 * User: godric
 * Date: 26.04.17
 * Time: 22:36
 */

namespace qtype_cppexpression\errors;


class prohibit_use_error extends penal_error {

    public function __construct($node, $expression, $penalvalue = 0) {
        $message = get_string('prohibit_use_error', 'qtype_cppexpression').': '.get_string($node->type(), 'qtype_cppexpression');
        parent::__construct($node, $message, $expression, $penalvalue);
    }
}