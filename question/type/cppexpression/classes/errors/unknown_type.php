<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Declaration of variables from teacher
 *
 * @package     cppexpression
 * @author      godric
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * User: godric
 * Date: 07.10.16
 * Time: 15:56
 */

namespace qtype_cppexpression\errors;

class unknown_type extends syntax_error {
    public function __construct($node, $expression, $penalvalue = 0) {
        parent::__construct($node, get_string('unknown_type_error', 'qtype_cppexpression'), $expression, $penalvalue);
    }
}