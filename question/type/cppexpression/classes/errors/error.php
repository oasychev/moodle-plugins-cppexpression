<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * General error class - parsing errors will generate it.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qtype_cppexpression\errors;
defined('MOODLE_INTERNAL') || die();

class error {
    /** @var string Human-readable error message without highlighed expression. */
    public $errormsg;
    /** @var string Highlighted expression. */
    public $highlightedexpr;
    /** @var block_formal_langs_ast_node Node that caused error.*/
    public $node;

    /**
     * Creates error object
     *
     * @param node block_formal_langs_ast_node child object Node that caused error.
     * @param message string Human readable message why this is error.
     * @param expression string Expression containing error.
     */
    public function __construct($node, $message, $expression) {
        $this->errormsg = $message;
        $this->node = $node;
        $this->highlightedexpr = $this->highlight_node($expression, $node);
    }

    /**
     * Returns HTML string : expression with highlighted (bold) node ($this->node) with error.
     *
     * @param expression string Expression containing error.
     * @param node block_formal_langs_ast_node child object Node that caused error.
     * @return string highlighted string
     */
    public function highlight_node($expression, $node) {
        // TODO - use position (block_formal_langs_node_position from block/formal_langs/tokens_base.php) from $node
        // to write function like highlight_expressions() in question/type/preg/preg_errors.php.
        if ($expression === null) {
            return '';
        }
        if ($node === null) {
            return $expression;
        }
        $start = $node->position()->colstart();
        $end = $node->position()->colend();
        return substr($expression, 0, $start) .
        '<b>' . substr($expression, $start, $end - $start + 1) . '</b>' .
            substr($expression, $end + 1);
    }

    public function error_message() {
        if (empty($this->highlightedexpr)) {
            return "{$this->errormsg}";
        }
        return "{$this->errormsg}<br/>{$this->highlightedexpr}";
    }
}
