<?php
// This file is part of Preg question type - https://code.google.com/p/oasychev-moodle-plugins/
//
// Preg question type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings for the cppexpression question type.
 *
 * @package    qtype_cppexpression
 * @copyright  2014 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $CFG;

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_heading('questioneditingheading', get_string('questioneditingheading', 'qtype_cppexpression'), ''));
    $settings->add(new admin_setting_configtext('qtype_cppexpression_maxerrorsshown', get_string('maxerrorsshownlabel', 'qtype_cppexpression'),
                get_string('maxerrorsshowndescription', 'qtype_cppexpression'), 5, PARAM_INT));
}