<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Question type class for the cppexpression question type.
 *
 * @package    qtype
 * @subpackage cppexpression
 * @copyright  2014 Khrzhanovskaya Olga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


global $CFG;
require_once($CFG->dirroot . '/question/type/shortanswer/questiontype.php');

/**
 * Cpp expression question type.
 *
 * @package     cppexpression
 * @author      Khrzhanovskaya Olga, Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \qtype_cppexpression\core\tree_converter;

class qtype_cppexpression extends qtype_shortanswer {

    public function extra_question_fields() {
        $extrafields = array('qtype_cppexpression_options', 'declarations', 'gradinganalyzer', 'hintgradeborder', 'prohibitoperators');

        $hintanalyzernames = $this->get_hint_analyzers_names();

        foreach($hintanalyzernames as $hintanalyzername => $definition) {
            $extrafields[] = $hintanalyzername.'mode';
            $extrafields[] = $hintanalyzername.'penalty';
        }
        return $extrafields;
    }

    public function save_question($question, $form) {
        // Collect prohibit operators.
        $prohibits = 0;
        foreach (array_slice(array_keys(tree_converter::$class_names), 1) as $key) {
            if (property_exists($form, 'proh_'.$key)) {
                $prohibits |= (1 << tree_converter::$class_names[$key][1]);
            }
        }
        $form->prohibitoperators = $prohibits;
        return parent::save_question($question, $form);
    }


    protected function save_hint_options($formdata, $number, $withparts) {
        $options = '';

        if(isset($formdata->hintoptions)){
            $options = $formdata->hintoptions[$number];
        }
        else {
            $hintanalyzernames = $this->get_hint_analyzers_names();
            $optionsarr = array();

            foreach($hintanalyzernames as $hintanalyzername) {
                $field = $hintanalyzername.'interactive';
                $numobj = $formdata->$field;
                if($numobj[$number]) {
                    $optionsarr[] = $hintanalyzername . '_' . $numobj[$number];
                }
            }
            $options .= join('\n', $optionsarr);
        }
        return $options;
    }

    /** Overload hints functions to be able to work with interactivehints*/
    protected function make_hint($hint) {
        return qtype_poasquestion_moodlehint_adapter::load_from_record($hint);
    }

    /** Overload import from Moodle XML format to import hints */
    public function import_from_xml($data, $question, qformat_xml $format, $extra=null) {
        $qo = parent::import_from_xml($data, $question, $format, $extra);
        $format->import_hints($qo, $data, false, true);
        return $qo;
    }

    /**
     * Returns an array where key is equal to the value, returned by analyzer
     * class name() method, value is description in user-readable form.
     */
    public function get_hint_analyzers_names(){
        return array (
            'array2d' => get_string('array2d_hint', 'qtype_cppexpression'),
            'compexpr' => get_string('compexpr_hint', 'qtype_cppexpression')
        );
    }

    /**
     * Returns an array where key is equal to the value, returned by analyzer
     * class name() method, value is description in user-readable form.
     */
    public function get_grading_analyzers_names(){
        //return array ('array2d' => get_string('array2d_grader', 'qtype_cppexpression'));
        return array (
            'array2d' => get_string('array2d_grader', 'qtype_cppexpression'),
            'compexpr' => get_string('compexpr_grader', 'qtype_cppexpression')
        );
    }
}
