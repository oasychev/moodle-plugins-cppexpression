<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_cppexpression', language 'en'
 *
 * @package    qtype
 * @subpackage cppexpression
 * @copyright &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addmoreexpressionblank'] = 'Добавить еще вариант';
$string['answer'] = 'Ответ: {$a}';
$string['answermustbegiven'] = 'Вы должны ввести ответ, если есть оценка или отзывы.';
$string['answerno'] = 'Ответ {$a}';
$string['caseno'] = 'Нет, регистр не важен';
$string['casesensitive'] = 'Важен ли регистр?';
$string['caseyes'] = 'Да, регистр важен';
$string['correctansweris'] = 'Правильный ответ: {$a}';
$string['correctanswers'] = 'Правильные ответы';
$string['filloutoneanswer'] = 'Вы должны вводить хотя бы один возможный ответ. Пустые ответы не будут использоваться. \'* \' Можно использовать в качестве подстановочного знака для соответствия любым символам. Первый ответный ответ будет использован для определения оценки и обратной связи.';
$string['maxerrorsshowndescription'] = 'Maximum number of errors shown for each expression in the question editing form';
$string['maxerrorsshownlabel'] = 'Максимальное количество отображаемых ошибок';
$string['notenoughanswers'] = 'Этот тип вопроса требует не менее {$a} ответов';
$string['pleaseenterananswer'] = 'Пожалуйста, введите ответ.';
$string['pluginname'] = 'Cppexpression';
$string['pluginname_help'] = 'In response to a question, the respondent types a expression in C or C++. There may be several possible correct answers, each with a different grade. Response is graded by different analyzers.';
$string['pluginname_link'] = 'question/type/cppexpression';
$string['pluginnameadding'] = 'Добавление вопроса в виде выражение на языке С или C++';
$string['pluginnameediting'] = 'Редактирование вопроса в виде выражение на языке С или C++';
$string['pluginnamesummary'] = 'Позволяет оценивается ответ в виде выражения C / C ++ различными анализаторами.';
$string['questioneditingheading'] = 'Редактирование настроек вопроса';

// Hinting options.
$string['cppexpr_hintno'] = 'Don\'t show' ;
$string['cppexpr_hintstudent'] = 'Show for the student\'s response';
$string['cppexpr_hintanswer'] = 'Show for the correct answer';
$string['cppexpr_hintboth'] = 'Show for both answer and response';
$string['cppexpr_hintcalculatedresult'] = 'Show calculated result';

// Grading options.
//$string['cppexpr_gradewithshowingerrors'] = 'Use with showing errors' ;
//$string['cppexpr_gradeonly'] = 'Only grade' ;

// Prohibitive operators
$string['program'] = 'оператор';
$string['prohibit_notice'] = 'Использование следующих операции запрещено: ';
$string['prohibitive_operators_header'] = 'Запрещенные операции';
$string['expr_logical_or'] = 'Логический ИЛИ';
$string['expr_logical_and'] = 'Логический И';
$string['expr_plus'] = 'Сложение \'+\'';
$string['expr_multiply'] ='Умножение \'*\'';
$string['expr_list'] = 'Последовательное вычисление \',\'';
$string['expr_function_call'] = 'Вызов функции';

$string['expr_logical_not'] = 'Логический НЕ';
$string['expr_unary_minus'] = 'Унарный минус \'-\'';
$string['expr_unary_plus'] = 'Унарный плюс \'+\'';
$string['expr_dereference'] = 'Непрямое обращение \'*\'';
$string['expr_take_adress'] = 'Адрес \'&\'';

$string['expr_postfix_increment'] = 'Постфиксный инкремент \'++\'';
$string['expr_postfix_decrement'] = 'Постфиксный декремент \'--\'';
$string['expr_prefix_increment'] = 'Префиксный инкремент \'++\'';
$string['expr_prefix_decrement'] = 'Префиксный декремент \'--\'';

$string['expr_assign'] = 'Присваивания \'=\'';
$string['expr_minus'] = 'Минус \'-\'';
$string['expr_modulosign'] = 'Модуль \'%\'';
$string['expr_division'] = 'Деление \'\\\'';
$string['expr_equal'] = 'Равенство \'==\'';
$string['expr_notequal'] = 'Неравенство \'!=\'';
$string['try_value_access'] = 'Обращение \'.\'';
$string['try_pointer_access'] = 'Непрямое обращение \'->\'';
$string['expr_array_access'] = 'Обращение \'[]\'';
$string['expr_rightshift'] = 'Сдвиг вправо \'>>\'';
$string['expr_leftshift'] = 'Сдвиг влево \'<<\'';
$string['expr_lesser_or_equal'] = 'Меньше или равно \'<=\'';
$string['expr_greater_or_equal'] = 'Больше или равно \'>=\'';
$string['expr_lesser'] = 'Меньше \'<\'';
$string['expr_greater'] = 'Больше \'>\'';
$string['expr_plus_assign'] = 'Составное присваивание \'+=\'';
$string['expr_minus_assign'] = 'Составное присваивание \'-=\'';
$string['expr_multiply_assign'] = 'Составное присваивание \'*=\'';
$string['expr_division_assign'] = 'Составное присваивание \'/=\'';
$string['expr_leftshift_assign'] = 'Составное присваивание \'<<=\'';
$string['expr_rightshift_assign'] = 'Составное присваивание \'>>=\'';

// Question editing form.
$string['form_answerinstruct'] = 'Заполните ответы как выражения C или С++, состоит из операндов и знаков операций. Переменные должны объявиться. Вы должны ввести хотя бы один ответ со 100% оценкой.';
$string['form_declarations'] = 'Объявление переменных';
$string['form_gradinganalyzer'] = 'Анализатор оценки';
$string['form_gradeborder'] = 'Минимальная граница оценки ответа для анализа ответа';
$string['form_penalty'] = 'Штраф за {$a} подсказки';
$string['form_analyzersheader'] = 'Подсказки';
$string['form_answer'] = 'Выражение С / C++';
$string['cppexpr_answer'] = 'Выражение {no}';
$string['usedeclaration'] = 'Использование объявления';

// Error messages.
$string['form_erroracceptfail'] = '{$a->analyzer} анализатор не поддерживает: {$a->errormsg}.';
$string['form_errorhintgradeborder'] = 'Минимальная граница оценки должен от 0 до 1.';
$string['form_errorpenalty'] = 'Минимальная граница оценки должен от 0 до 1.';
$string['form_toomanyerrors'] = '.......{$a} ошибок';

// Notice messages.
$string['notice_not_support'] = 'Этот вопрос автоматически оценивается анализатором. НЕ рекомендуется использовать следующие операторы:';
$string['notice_support'] = 'Этот вопрос автоматически оценивается анализатором. Рекомендуется использовать следующие операторы:';

$string['stmt_list'] = 'операторы';
$string['enter_other'] = 'Анализатор не смог оценить ответ так как используется нерекомендующие операции.';
// Array2d analyzers.
$string['form_array2dmode'] = 'Заполнение 2D матрицы режим работы';
$string['array2d_hint'] = 'Заполнение 2D матрицы подсказки';
$string['array2d_grader'] = 'Заполнение 2D матрицы оценки';
$string['array2d_hint_description'] = 'заполненная матрица';
$string['array2d_hint_description_for_mode1'] = "Заполненные матрицы различных размеров для ответа.";
$string['array2d_hint_description_for_mode3'] = "Заполненные матрицы различных размеров для совмещенного текущего ответа с ожидаемым. Совпадающие ячейки выделены зеленым, несовпадающие - красным. Некорректные значения перечеркнуты, ожидаемые находятся справа от них.";


// Normalization and comparison cpp expression analyzers
$string['form_compexprmode'] = 'Сравнение выражения режим работы';
$string['compexpr'] = 'Нормализация и сравнение cppexpression';
$string['compexpr_hint'] = 'Сравнение выражения C/C++ подсказки';
$string['compexpr_grader'] = 'Сравнение выражения C/C++ оценки';

// Help tool tip
$string['declarationstip'] = 'Правила объявления';
$string['declarationstip_help'] = '<p>Поддерживает примитивные типы C/C++. Каждое объявление должно</p>
<p>Для целых чисел:</p> <p>// from < start_value > to < end_value >.</p>
<p>Для вещественных чисел:</p> <p>// from < start_value > to < end_value > step < step_value >.</p>
<p>Для остальных просто комментарий //</p>';

$string['syntax_error'] = 'Синтаксическая ошибка';
$string['unknown_expr'] = 'Не правильное выражение';
$string['declaration_error'] = 'Ошибка объявления';
$string['prohibit_use_error'] = 'Использование запрещающих операции';
$string['unknown_type_error'] = 'Не правильное выражение';
$string['redecl_error'] = 'повторно объявление переменной';
$string['wrong_format_cmt_error'] = 'Детальное описание переменной не правильное';
$string['missing_decl_error'] = 'отсутствие объявления переменной';
$string['decl_syntax_mes_error'] = 'проверьте объявление пожалуйста';
$string['notice'] = 'Внимание';