<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_cppexpression', language 'en'
 *
 * @package    qtype
 * @subpackage cppexpression
 * @copyright &copy; 2014 Oleg Sychev, Volgograd State Technical University 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addmoreexpressionblank'] = 'Blank for {no} more expression';
$string['answer'] = 'Answer: {$a}';
$string['answermustbegiven'] = 'You must enter an answer if there is a grade or feedback.';
$string['answerno'] = 'Answer {$a}';
$string['caseno'] = 'No, case is unimportant';
$string['casesensitive'] = 'Case sensitivity';
$string['caseyes'] = 'Yes, case must match';
$string['correctansweris'] = 'The correct answer is: {$a}';
$string['correctanswers'] = 'Correct answers';
$string['filloutoneanswer'] = 'You must provide at least one possible answer. Answers left blank will not be used. \'*\' can be used as a wildcard to match any characters. The first matching answer will be used to determine the score and feedback.';
$string['maxerrorsshowndescription'] = 'Maximum number of errors shown for each expression in the question editing form';
$string['maxerrorsshownlabel'] = 'Maximum number of errors shown';
$string['notenoughanswers'] = 'This type of question requires at least {$a} answers';
$string['pleaseenterananswer'] = 'Please enter an answer.';
$string['pluginname'] = 'Cppexpression';
$string['pluginname_help'] = 'In response to a question, the respondent types a expression in C or C++. There may be several possible correct answers, each with a different grade. Response is graded by different analyzers.';
$string['pluginname_link'] = 'question/type/cppexpression';
$string['pluginnameadding'] = 'Adding a C++ expression question';
$string['pluginnameediting'] = 'Editing a C++ expression question';
$string['pluginnamesummary'] = 'Allows a response C/C++ expression that is graded by different analyzers.';
$string['questioneditingheading'] = 'Question editing settings';

// Hinting options.
$string['cppexpr_hintno'] = 'Don\'t show' ;
$string['cppexpr_hintstudent'] = 'Show for the student\'s response';
$string['cppexpr_hintanswer'] = 'Show for the correct answer';
$string['cppexpr_hintboth'] = 'Show for both answer and response';
$string['cppexpr_hintcalculatedresult'] = 'Show calculated result';

// Grading options.
//$string['cppexpr_gradewithshowingerrors'] = 'Use with showing errors' ;
//$string['cppexpr_gradeonly'] = 'Only grade' ;

// Prohibitive operators
$string['program'] = 'statement';
$string['prohibit_notice'] = 'Using the following operators is prohibited: ';
$string['prohibitive_operators_header'] = 'Prohibit operators';
$string['expr_logical_or'] = 'Logical OR';
$string['expr_logical_and'] = 'Logical AND';
$string['expr_plus'] = 'Addition \'+\'';
$string['expr_multiply'] ='Multiply \'*\'';
$string['expr_list'] = 'Comma separator \',\'';
$string['expr_function_call'] = 'Function call';

$string['expr_logical_not'] = 'Logical NOT';
$string['expr_unary_minus'] = 'Unary \'-\'';
$string['expr_unary_plus'] = 'Unary \'+\'';
$string['expr_dereference'] = 'Dereference \'*\'';
$string['expr_take_adress'] = 'Reference \'&\'';

$string['expr_postfix_increment'] = 'Postfix increment \'++\'';
$string['expr_postfix_decrement'] = 'Postfix decrement \'--\'';
$string['expr_prefix_increment'] = 'Prefix increment \'++\'';
$string['expr_prefix_decrement'] = 'Prefix increment \'--\'';

$string['expr_assign'] = 'Assignment \'=\'';
$string['expr_minus'] = 'Minus \'-\'';
$string['expr_modulosign'] = 'Modulo \'%\'';
$string['expr_division'] = 'Divide \'\\\'';
$string['expr_equal'] = 'Equality \'==\'';
$string['expr_notequal'] = 'Inequality \'!=\'';
$string['try_value_access'] = 'Member access \'.\'';
$string['try_pointer_access'] = 'Member access \'->\'';
$string['expr_array_access'] = 'Subscript \'[]\'';
$string['expr_rightshift'] = 'Right shift \'>>\'';
$string['expr_leftshift'] = 'Left shift \'<<\'';
$string['expr_lesser_or_equal'] = 'Lesser or equal \'<=\'';
$string['expr_greater_or_equal'] = 'Greater or equal \'>=\'';
$string['expr_lesser'] = 'Lesser \'<\'';
$string['expr_greater'] = 'Greater \'>\'';
$string['expr_plus_assign'] = 'Compound assignment \'+=\'';
$string['expr_minus_assign'] = 'Compound assignment \'-=\'';
$string['expr_multiply_assign'] = 'Compound assignment \'*=\'';
$string['expr_division_assign'] = 'Compound assignment \'/=\'';
$string['expr_leftshift_assign'] = 'Compound assignment \'<<=\'';
$string['expr_rightshift_assign'] = 'Compound assignment \'>>=\'';

// Question editing form.
$string['form_answerinstruct'] = 'Fill out answers as C++ expressions. You must enter at least one answer with 100% grade.';
$string['form_declarations'] = 'Declarations of variables';
$string['form_gradinganalyzer'] = 'Grading engine';
$string['form_gradeborder'] = 'Hint grade border';
$string['form_penalty'] = 'Penalty for {$a} hint';
$string['form_analyzersheader'] = 'Hinting';
$string['form_answer'] = 'C++ expression';
$string['cppexpr_answer'] = 'Expression {no}';
$string['usedeclaration'] = 'Use declaration';

// Error messages.
$string['form_erroracceptfail'] = '{$a->analyzer} analyzer don\'t support: {$a->errormsg}.';
$string['form_errorhintgradeborder'] = 'Hint grade border must be a number from 0 to 1.';
$string['form_errorpenalty'] = 'Hint grade border must be a number from 0 to 1.';
$string['form_toomanyerrors'] = '.......{$a} more errors';

// Notice messages.
$string['notice_not_support'] = 'This question is automatically graded by analyzer. Using other operators are recommended because wrong marking may happen by using the following operators:';
$string['notice_support'] = 'This question is automatically graded by analyzer. Using other operators are recommended in case of wrong marking because analyzer only supports the following operators:';

$string['stmt_list'] = 'statements';
$string['enter_other'] = 'Your answer can\'t be graded because you\' using not recommended operator.';

// Array2d analyzers.
$string['form_array2dmode'] = 'Filling 2D array mode';
$string['array2d_hint'] = 'Filling 2D array hint';
$string['array2d_grader'] = 'Filling 2D array grading';
$string['array2d_hint_description'] = 'filled matrixes';
$string['array2d_hint_description_for_mode1'] = "Filled matrixes with different sizes for the response.";
$string['array2d_hint_description_for_mode3'] = "Filled matrixes with different sizes for both answer and response. Equivalent cells are highlighted in green, unequivalent - in red. Incorrect values are crossed out, expected are at the right of them.";

// Normalization and comparison cpp expression analyzers
$string['form_compexprmode'] = 'Comparing C/C++ expressions mode';
$string['compexpr'] = 'Normalization and comparison cppexpression';
$string['compexpr_hint'] = 'Comparing C/C++ expressions hint';
$string['compexpr_grader'] = 'Comparing C/C++ expressions grading';

// Help tool tip
$string['declarationstip'] = 'Declarations\'s rules';
$string['declarationstip_help'] = '<p>Support fundamental data types, which implemented directly by the C language. Each declaration must comment by format.</p>
<p>For numerical integer types:</p> <p>// from < start_value > to < end_value >.</p>
<p>For numerical float types:</p> <p>// from < start_value > to < end_value > step < step_value >.</p>
<p>For other data types simply write // because not supported now.</p>';

$string['syntax_error'] = 'Syntax error';
$string['unknown_expr'] = 'unknown expression';
$string['declaration_error'] = 'Declaration error';
$string['prohibit_use_error'] = 'Using prohibit error';
$string['unknown_type_error'] = 'support only basic C/C++ data types';
$string['redecl_error'] = 'variable $prevdeclaration previosly declared.';
$string['wrong_format_cmt_error'] = 'Detail variable\'s decreption in comment wrong format';
$string['missing_decl_error'] = 'missing semicolon in the end';
$string['missing_cmt_error'] = 'missing detail variable\'s decreption in comment';
$string['decl_syntax_mes_error'] = 'Please check syntax of declaration';
$string['notice'] = 'Notice';