<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the editing form for the cppexpression question type.
 *
 * @package     cppexpression
 * @author      Khrzhanovskaya Olga, Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/question/type/shortanswer/edit_shortanswer_form.php');
require_once($CFG->dirroot . '/question/type/cppexpression/questiontype.php');

use \qtype_cppexpression\core\tree_converter;

/**
 * Cppexpression question editing form definition.
 */
class qtype_cppexpression_edit_form extends qtype_shortanswer_edit_form {

    // Init hints options.
    private $stackanalyzeroptions = array();
    private $algebraanalyzeroptions = array();

    private $hintanalyzers = array();

    protected function definition_inner($mform) {

        global $CFG;
        global $PAGE;

        $qtype = new qtype_cppexpression;

        $graderanalyzernames = $qtype->get_grading_analyzers_names();
        $hintanalyzernames = $qtype->get_hint_analyzers_names();

        foreach ($hintanalyzernames as $name => $description) {
            $classname = "\\qtype_cppexpression\\{$name}\\{$name}_hint";
            // Options.
            $options = $classname::hint_modes();
            $options[0] = get_string('cppexpr_hintno', 'qtype_cppexpression'); // Adding standard 'No' option.

            $this->hintanalyzers[] = array('name' => $name,
                                    'description' => $description,
                                    'modefieldname' => $name . 'mode',
                                    'options' => $options,
                                    'penaltyfieldname' => $name . 'penalty',
                                    'penaltyfieldnameinteractive' => $name . 'interactive');
        }

        $mform->addElement('textarea', 'declarations',
            get_string('form_declarations', 'qtype_cppexpression'), 'wrap="virtual" rows="5" cols="80"');
        $mform->setType('declarations', PARAM_TEXT);
        $mform->addHelpButton('declarations', 'declarationstip', 'qtype_cppexpression');

        // Grading analyzer selector.
        $mform->addElement('select', 'gradinganalyzer',
            get_string('form_gradinganalyzer', 'qtype_cppexpression'), $graderanalyzernames);
        $mform->addElement('text', 'hintgradeborder',
            get_string('form_gradeborder', 'qtype_cppexpression'));
        $mform->setType('hintgradeborder', PARAM_FLOAT);
        $mform->setDefault('hintgradeborder', '0.9');

        // Dissable declaration if not need.
        foreach ($graderanalyzernames as $name => $description) {
            $classname = "\\qtype_cppexpression\\{$name}\\{$name}_grader";
            if (!$classname::is_need_declaration() == \qtype_cppexpression\decl_type::DECL_NO_COMMENT) {
                $mform->disabledIf('declarations', 'gradinganalyzer', 'eq', $name);
            }
        }

        // Hints.
        $mform->addElement('header', 'multitriesheader',
            get_string('form_analyzersheader', 'qtype_cppexpression'));
        $mform->setExpanded('multitriesheader', 0);

        foreach ($this->hintanalyzers as $hintanalyzer) {
            $mform->addElement('select', $hintanalyzer['modefieldname'],
                get_string('form_'.$hintanalyzer['modefieldname'], 'qtype_cppexpression'),
                $hintanalyzer['options']);
            $mform->addElement('text', $hintanalyzer['penaltyfieldname'],
                get_string('form_penalty', 'qtype_cppexpression', $description));
            $mform->setType($hintanalyzer['penaltyfieldname'], PARAM_FLOAT);
            $mform->setDefault($hintanalyzer['penaltyfieldname'], '0.333333');
        }

        // Prohibitive operators.
        $mform->addElement('header', 'prohibitiveoperators',
            get_string('prohibitive_operators_header', 'qtype_cppexpression'));
        $mform->setExpanded('prohibitiveoperators', 0);

        $operators = array(
            'Arithmetic operators'=> array(
                'expr_plus', 'expr_minus', 'expr_multiply', 'expr_division', 'expr_modulosign',
                'expr_unary_minus', 'expr_postfix_increment', 'expr_postfix_decrement',
                'expr_prefix_increment', 'expr_prefix_decrement',
                'expr_assign'
            ),
            'Comparison operators'=> array(
                'expr_equal', 'expr_notequal',
                'expr_lesser_or_equal',
                'expr_greater_or_equal',
                'expr_lesser', 'expr_greater'
            ),
            'Logical operators'=> array(
                'expr_logical_not', 'expr_logical_or', 'expr_logical_and'
            ),
            'Bitwise operators'=> array(
                'expr_rightshift',
                'expr_leftshift'
            ),
            'Compound assignment operators'=> array(
                'expr_plus_assign', 'expr_minus_assign',
                'expr_multiply_assign', 'expr_division_assign',
                'expr_leftshift_assign', 'expr_rightshift_assign'
            ),
            'Member and pointer operators'=> array(
                'expr_dereference', 'expr_take_adress',
                'try_value_access', 'try_pointer_access',
                'expr_array_access'
            ),
            'Other operators'=> array(
                'expr_list', 'expr_function_call'
            )
        );
        $ops = property_exists($this->question, 'options') ? intval($this->question->options->prohibitoperators) : 0;
        foreach ($operators as $gname => $list) {
            $arr = array();
            $arr[] =& $mform->createElement('html', '<div class="row">');
            foreach ($list as $op) {
                $arr[] =& $mform->createElement('html', '<div class="col-sm-4">');
                $arr[] =& $mform->createElement('checkbox', 'proh_'.$op, '', get_string($op, 'qtype_cppexpression'));
                $mform->setDefault('proh_'.$op, ($ops >> tree_converter::$class_names[$op][1]) & 1);
                $arr[] =& $mform->createElement('html', '</div>');
            }
            $arr[] =& $mform->createElement('html', '</div>');
            $mform->addGroup($arr, 'carr', $gname, array(''), false);
        }

        //--------------------------------
        $mform->addElement('static', 'answersinstruct',
                get_string('correctanswers', 'qtype_shortanswer'),
                get_string('form_answerinstruct', 'qtype_cppexpression'));
        $mform->closeHeaderBefore('answersinstruct');

        $this->add_per_answer_fields($mform,
            get_string('cppexpr_answer', 'qtype_cppexpression'), question_bank::fraction_options(), 2, 1);
        $this->add_interactive_settings();
    }

    protected function get_hint_fields($withclearwrong = false, $withshownumpartscorrect = false) {
        $mform = $this->_form;
        list($repeated, $repeatedoptions) = parent::get_hint_fields($withclearwrong, $withshownumpartscorrect);

        // $langselect = $mform->getElement('langid');
        // $langs = $langselect->getSelected();
        // $langobj = block_formal_langs::lang_object($langs[0]);
        foreach ($this->hintanalyzers as $hintanalyzer) {
            $repeated[] = $mform->createElement('select', $hintanalyzer['interactive'],
                get_string('form_'.$hintanalyzer['modefieldname'], 'qtype_cppexpression'),
                $hintanalyzer['options']);
        }
        return array($repeated, $repeatedoptions);
    }

    // TODO - check and make sure hint fields are filled correctly.
    protected function data_preprocessing_hints($question, $withclearwrong = false,
                                                $withshownumpartscorrect = false) {
        if (empty($question->hints)) {
            return $question;
        }
        $question = parent::data_preprocessing_hints($question, $withclearwrong, $withshownumpartscorrect);

        $qtype = new qtype_cppexpression;
        $hintanalyzernames = $qtype->get_hint_analyzers_names();

        $hintsoptions = array();
        /*
        foreach ($question->hints as $hint) {
            $options = explode('\n',$hint->options);
            foreach($options as $option) {
                list($hintkey,$hintanalyzermode) = explode('_',$option);
                foreach($hintanalyzernames as $hintanalyzername) {
                    if($hintkey == $hintanalyzername){
                        $hintsoptions[$hintkey][] = $hintanalyzermode;
                    }
                }
            }
        }
        */

        foreach ($hintanalyzernames as $hintanalyzername) {
            foreach ($question->hints as $hint) {
                $options = explode('\n', $hint->options);
                if (strpos($hint->options, $hintanalyzername) === false) {
                    $hintsoptions[$hintanalyzername][] = 0;
                } else {
                    foreach ($options as $option) {
                        list($hintkey, $hintanalyzermode) = explode('_', $option);
                        if ($hintkey == $hintanalyzername) {
                            $hintsoptions[$hintanalyzername][] = $hintanalyzermode;
                        }

                    }
                }
            }
        }

        /*
        echo '<pre>';
        print_r($hintsoptions);
        echo '<pre>';
        */
        foreach ($hintsoptions as $key => $value) {
            $fieldname = $key . 'interactive';
            $question->$fieldname = $value;
        }

        return $question;
    }

    protected function get_more_choices_string() {
        return get_string('addmoreexpressionblank', 'qtype_cppexpression');
    }

    protected function data_preprocessing($question) {
        $question = parent::data_preprocessing($question);
        $question = $this->data_preprocessing_hints($question);
        return $question;
    }

    private function add_error(&$errors, $name, $message) {
        if (key_exists($name, $errors)) {
            $errors[$name] .= '<br>' . $message;
        } else {
            $errors[$name] = $message;
        }
    }

    // TODO - cleanup this function.
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        // Collect prohibit operators.
        $prohibits = 0;
        foreach (array_slice(array_keys(tree_converter::$class_names), 1) as $key) {
            if (array_key_exists('proh_'.$key, $data)) {
                $prohibits |= (1 << tree_converter::$class_names[$key][1]);
            }
        }
        $data['prohibitoperators'] = $prohibits;

        // Is hintgradeborder numeric in range [0..1] ???
        if (!array_key_exists('hintgradeborder', $data)) {
            $data[''] = 0;
        } else if (!is_numeric($data['hintgradeborder']) ||
            $data['hintgradeborder'] > 1 || $data['hintgradeborder'] < 0
        ) {
            $errors['hintgradeborder'] = get_string('form_errorhintgradeborder', 'qtype_cppexpression');
        }

        $declaration = null;
        $declaration = $this->validation_declarations($data, $files, $errors);
        $this->validation_cppexpression($data, $files, $errors, $declaration);
        return $errors;
    }

    protected function validation_declarations($data, $files, &$errors) {
        $name = $data['gradinganalyzer'];
        $classname = "\\qtype_cppexpression\\{$name}\\{$name}_grader";
        $type = $classname::is_need_declaration();
        $decl = null;
        if ($type != \qtype_cppexpression\decl_type::NO_DECLARATION) {
            $decl = new qtype_cppexpression\core\declaration($data['declarations'], $classname::is_need_declaration());
            $tmp = $decl->accept_declaration();
            foreach ($tmp as $error) {
                $this->add_error($errors, 'declarations', $error->error_message());
            }
        }
        return $decl;
    }

    protected function validation_cppexpression($data, $files, &$errors, $declaration) {
        // TODO: add checking codedescription (when cpp-parser will exist)
        global $CFG;

        $qtype = new qtype_cppexpression;
        $hintanalyzernames = $qtype->get_hint_analyzers_names();
        $graderanalyzernames = $qtype->get_grading_analyzers_names();

        foreach ($hintanalyzernames as $name => $description) {
            $fieldname = $name . 'penalty';
            if (!array_key_exists($fieldname, $data)) {
                $data[$fieldname] = 0;
            } else if (!is_numeric($data[$fieldname]) ||
                $data[$fieldname] > 1 || $data[$fieldname] < 0) {
                $errors[$fieldname] = get_string('form_errorpenalty', 'qtype_cppexpression');
            }
        }

        $answercount = 0;
        $answers = $data['answer'];

        $maxerrors = 5;
        if (isset($CFG->qtype_cppexpression_maxerrorsshown)) {
            $maxerrors = $CFG->qtype_cppexpression_maxerrorsshown;
        }

        // Get type of grander.
        $name = $data['gradinganalyzer'];
        $classname = "\\qtype_cppexpression\\{$name}\\{$name}_grader";
        $type = $classname::is_need_declaration();
        // Fill accepting errors array.
        foreach ($answers as $key => $answer) {
            $trimmedanswer = trim($answer);
            // $elementname = "answer[$key]";
            $elementname = "answeroptions[$key]";
            $acceptingerrors = array();
            if ($trimmedanswer !== '') {
                $answercount++;

                // Hints accepting check.
                foreach ($hintanalyzernames as $name => $description) {
                    $fieldname = $name . 'mode';
                    $isusedinteractive = $this->is_used_analyser_interactive($data, $fieldname);

                    if ($data[$fieldname] != 0 || $isusedinteractive) { // When mode is 0 hint is disabled.
                        $classname = "\\qtype_cppexpression\\{$name}\\{$name}_hint";
                        $accept = $classname::accept_expression($answer, $data['prohibitoperators'], $declaration);
                        $acceptingerrors = array_merge($acceptingerrors, $accept);
                    }
                }

                // Grading accepting check.
                $accept = $classname::accept_expression($answer, $data['prohibitoperators'], $declaration);
                $acceptingerrors = array_merge($acceptingerrors, $accept);

                // Now get error messages from errors (no more than limit).
                if (!empty($acceptingerrors)) {
                    $i = 0;
                    // $errors[$elementname] = '';
                    foreach ($acceptingerrors as $error) {
                        if ($i < $maxerrors) {
                            // $errors[$elementname] .= $error->error_message();
                            $this->add_error($errors, $elementname, $error->error_message());
                        }
                        $i++;
                    }
                    if ($i > $maxerrors) {
                        $errors[$elementname] .= get_string('form_toomanyerrors', 'qtype_cppexpression', $i - $maxerrors);
                    }
                }
            }
        }

        return $errors;

    }

    /**
     * Detect: can analyser be used in interactive mode.
     * @param $data data on the edit_form
     * @param $analysername name of analyser: to get field of using mode for interactive
     * @return bool* using
     */
    private function is_used_analyser_interactive ($data, $analysername) {
        $i = 0;
        $fieldname = $analysername . 'interactive';

        foreach ($data['hint'] as $hint) {
            if ($hint['text'] != '') {
                $fielddata = $data[$fieldname];
                if ($fielddata[$i] != 0) {
                    return true;
                }
            }
            $i++;
        }
        return false;
    }

    public function qtype() {
        return 'cppexpression';
    }

}
