<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cpp expression question renderer class.
 *
 * @package     cppexpression
 * @author      Khrzhanovskaya Olga, Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

use qtype_cppexpression\core\tree_converter;

/**
 * Generates the output for cpp expression questions.
 *
 * @copyright  2014 Khrzhanovskaya Olga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_cppexpression_renderer extends qtype_renderer {

    public function formulation_and_controls(question_attempt $qa,
                                             question_display_options $options) {
        $question = $qa->get_question();
        $currentanswer = $qa->get_last_qt_var('answer');

        $inputname = $qa->get_qt_field_name('answer');
        $inputattributes = array(
            'type' => 'text',
            'name' => $inputname,
            'value' => $currentanswer,
            'id' => $inputname,
            'size' => 80,
        );

        if ($options->readonly) {
            $inputattributes['readonly'] = 'readonly';
        }

        $feedbackimg = '';
        if ($options->correctness) {
            $answer = $question->get_matching_answer(array('answer' => $currentanswer));
            if ($answer) {
                $fraction = $answer->fraction;
            } else {
                $fraction = 0;
            }
            $inputattributes['class'] = $this->feedback_class($fraction);
            $feedbackimg = $this->feedback_image($fraction);
        }

        $questiontext = $question->format_questiontext($qa);
        $placeholder = false;
        if (preg_match('/_____+/', $questiontext, $matches)) {
            $placeholder = $matches[0];
            $inputattributes['size'] = round(strlen($placeholder) * 1.1);
        }
        $input = html_writer::empty_tag('input', $inputattributes) . $feedbackimg;

        if ($placeholder) {
            $inputinplace = html_writer::tag('label', get_string('answer'),
                array('for' => $inputattributes['id'], 'class' => 'accesshide'));
            $inputinplace .= $input;
            $questiontext = substr_replace($questiontext, $inputinplace,
                strpos($questiontext, $placeholder), strlen($placeholder));
        }

        $result = html_writer::tag('div', $questiontext, array('class' => 'qtext'));

        if (!$placeholder) {
            $result .= html_writer::start_tag('div', array('class' => 'ablock'));
            $result .= html_writer::tag('label', get_string('answer', 'qtype_shortanswer',
                html_writer::tag('span', $input, array('class' => 'answer'))),
                array('for' => $inputattributes['id']));
            $result .= html_writer::end_tag('div');
        }

        if ($qa->get_state() == question_state::$todo &&
            $question->is_complete_response(array('answer' => $currentanswer))) {
            $result .= html_writer::nonempty_tag('div',
                             $question->get_validation_error(array('answer' => $currentanswer)),
                             array('class' => 'validationerror'));
        }

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                $question->get_validation_error(array('answer' => $currentanswer)),
                array('class' => 'validationerror'));
        }

        // Create notice about prohibit operators.
        $notice = '';
        if ($question->prohibitoperators != 0) {
            $notice = get_string('prohibit_notice', 'qtype_cppexpression').'</br>';
            $notice .= html_writer::start_div("row");
            foreach (array_slice(tree_converter::$class_names, 1) as $key=>$value) {
                if (($question->prohibitoperators >> $value[1]) & 1 == 1) {
                    $notice .= html_writer::start_div("col-sm-4");
                    $notice .= get_string($key, 'qtype_cppexpression');
                    $notice .= html_writer::end_div();
                }
            }
            $notice .= html_writer::end_div();
        }
        // Get supporting notice.
        $classname = "\\qtype_cppexpression\\{$question->gradinganalyzer}\\{$question->gradinganalyzer}_grader";
        $notice .= $classname::get_supporting_notice();
        // Show notice to interface.
        if (!empty($notice)) {
            $result .= html_writer::start_tag('div', array('class' => 'qnotice'));
            $result .= html_writer::tag('div', get_string('notice', 'qtype_cppexpression').': ', array('class' => 'qnotice', 'style' => 'color:red'));
            $result .= html_writer::tag('div', $notice, array());
            $result .= html_writer::end_tag('div');
        }

        return $result;
    }


    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        $answer = $question->get_correct_response();
        if (!$answer) {
            return '';
        }
        return get_string('correctansweris', 'qtype_shortanswer', $answer['answer']);
    }

    public function feedback(question_attempt $qa, question_display_options $options) {

        $feedback = '';

        $question = $qa->get_question();
        $behaviour = $qa->get_behaviour();
        $currentanswer = $qa->get_last_qt_var('answer');
        if (!$currentanswer) {
            $currentanswer = '';
        }
        $br = html_writer::empty_tag('br');

        if (is_a($behaviour, 'qtype_poasquestion\behaviour_with_hints')) {

            // For interactivehints check existing variants of options in question_attempt_step data.
            if (get_class($behaviour) == 'qbehaviour_interactivehints') {
                $hints = $this->get_qa_hints($qa);

                foreach ($hints as $hintkey) {
                    $feedback .= $this->render_hint($question, $hintkey, $qa, $options, $currentanswer);
                }
            } else { // Adaptivehints?
                $hints = $question->available_specific_hints($currentanswer);
                $hints = $behaviour->adjust_hints($hints);

                foreach ($hints as $hintkey) {
                    if ($qa->get_last_step()->has_behaviour_var('_render_'.$hintkey)) {
                        $feedback .= $this->render_hint($question, $hintkey, $qa, $options, $currentanswer);
                    }
                }
            }
        }
        $output = parent::feedback($qa, $options);
        return $feedback.$output;
    }

    /**
     * Generate feedback of analyser. Feedback contains information about errors
     * if analyzer cann't process answer.
     *
     * @param $question question.
     * @param $hintkey available hints as hintey.
     * @param $qa the question attempt to display.
     * @param $options controls what should and should not be displayed.
     * @param $answer last student response.
     * @return feedback as HTML fragment.
     */
    private function render_hint($question, $hintkey, $qa, $options, $answer) {
        $feedback = '';
        $br = html_writer::empty_tag('br');

        $hintobj = $question->hint_object($hintkey);
        $hint = $hintobj->render_hint($this, $qa, $options, array('answer' => $answer));
        if (gettype($hint) == 'string') { // Moodle hint?
            $feedback .= $hint . $br;
        } else {                        // Analyser hint?
            if (count($hint['errors'])) {
                foreach ($hint['errors'] as $error) {
                    $feedback .= $error->analyzer . ' error: ' . $error->errormsg . $br;
                }
            } else {
                $feedback .= $hint['hint']->hint . $br;
            }
        }
        return $feedback;
    }

    /**
     * Gereate array of hintkeys of available hints parsing fields of question_attempt object.
     * It is used for interactivehints behaviour.
     *
     * @param question_attempt $qa the question attempt to display.
     * @return array of hintkeys.
     */
    private function get_qa_hints(question_attempt $qa) {
        // TODO : Check correct working.
        $qtype = new qtype_cppexpression;
        $hintanalyzernames = $qtype->get_hint_analyzers_names();

        // Generate all analyzer's names.
        $arr = array();
        foreach ($hintanalyzernames as $hintanalyzername => $description) {
            $analyzerclassname = "qtype_cppexpression\\$hintanalyzername\\${hintanalyzername}_hint";
            $analyzer = new $analyzerclassname('', '');
            $curarr = array( $hintanalyzername => count($analyzer->hint_modes()) );
            $arr = array_merge($arr, $curarr);
        }

        $result = array();
        foreach ($arr as $name => $count) {
            $curres = array();
            // Add in array <exist option + current> .
            foreach ($result as $curval) {
                for ($i = 1; $i <= $count; $i++) {
                    $optionsstr = $curval . '\n' . $name . '_' . $i;
                    if ($qa->get_last_step()->has_qt_var($optionsstr)) {
                        $hintsstr = str_replace('-_render_', '', $optionsstr);
                        return explode('\n', $hintsstr);
                    }
                    $curres[] = $optionsstr;
                }
            }
            // Add in array pure current option.
            for ($i = 1; $i <= $count; $i++) {
                $optionsstr = '-_render_' . $name . '_' . $i;
                if ($qa->get_last_step()->has_qt_var($optionsstr)) {
                    $hintsstr = str_replace('-_render_', '', $optionsstr);
                    return explode('\n', $hintsstr);
                }
                $result[] = $optionsstr;
            }
            $result = array_merge($result, $curres);
        }
        return $result;
    }
}
